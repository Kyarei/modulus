MobEntity#getEquipmentForSlot() is called inside initEquipment() to determine
which item should be equipped; returns Item but we need our method to return an
ItemStack (DONE)

ZombieEntity#initEquipment() has a chance of giving zombies iron swords or
shovels; we'll need to change these to give them our custom items (DONE)

WitherSkeletonEntity#initEquipment() needs to be injected to give custom swords
instead of the regular one (DONE)

TODO:

AbstractSkeletonEntity#initEquipment() needs to be injected once we add generic
bows

maybe inject MobEntity#updateEnchantments() to have a rare chance of gear
enchanted at an unusually high level
