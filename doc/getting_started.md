## Getting Started with Suludom

### Crafting a tool table

Craft a wooden pickaxe with a crafting table, then use it to get at least
15 blocks of cobblestone and 2 pieces of coal (alternatively, smelt wood for
charcoal). Use 8 blocks to make a furnace then smelt the rest into stone, and
again into smooth stone. Now you can craft a tool table:

```
SCS  S = smooth stone or polished blackstone
SLS  C = crafting table
SSS  L = log
```

(Make another crafting table, as you'll still need the regular one.)

Place down the tool table. When you open it, you'll see an interface similar
to the crafting table. You can make tools there, using similar recipes to
vanilla Minecraft. Here are some things to know about:

* You can mix and match materials. However, mixing materials from different
  categories won't work well.
* Once you acquire other items that work like tool rods, you'll be able to
  use them in place of sticks.
* You can hold Ctrl while hovering over a piece of equipment to see how it was
  constructed.
* Mining levels are named by Suludom as such: *flimsy* for wood, *modest* for
  stone, *sturdy* for iron and *reinforced* for diamond. *Vibrant* is a tier
  higher than diamond and currently unused.
* In order to make stone tools, you'll have to use regular stone, not
  cobblestone. Alternatively, use the other stone variants.
* Mirroring or shifting tool table recipes does not work.

#### Crafting armor

Armor requires an undamaged piece of the corresponding leather armor to craft.
The tool table recipes are shown below:

```
Helmet Chestplate Leggings Boots
 MMM      MLM       MMM     MLM
 MLM      MMM       MLM     M M
          MMM       M M

M = material
L = leather armor
```

### Crafting an augment table

Once you acquire 7 iron ingots, you can make an augment table:

```
SCS  S = iron ingot
SLS  C = crafting table
SSS  L = log
```

The augment table has two input slots: the left one accepts the equipment you
want to upgrade, and the right one accepts the material used to upgrade your
equipment. To confirm your augmentation, just take the item from the output
slot.

Notes:

* A tool can hold only one augment, so choose wisely!
* Different augments require different quantities of their respective items.

### Crafting a scrapping table

A scrapping table, crafted as shown below, can recover materials from unwanted
tools.

```
SCS  S = obsidian
SLS  C = crafting table
SSS  L = block of gold
```

Just put your tool in the input slot and press the button.

### Crafting a blood furnace

Blood furnaces are used to craft some advanced items.

```
SCS  S = blood lapis block
SLS  C = furnace
SSS  L = nether bricks
```

There are two input slots and one fuel slot. The fuel slot takes eyes of ender.
Current recipes:

* emerald + lapis lazuli → diamond (takes 2 eyes)

### Enchanting

For the most part, Suludom equipment can be enchanted like vanilla equipment.
However, Suludom adds many new features to the enchanting system.

#### Enchantment affinities

Certain materials lend themselves toward certain enchantments. That is,
depending on which materials you use to craft your items, you will be more or
less likely to get a given enchantment on it. These affinities can be viewed
by pressing Shift while viewing a gear's tooltip.

Some materials can unlock treasure enchantments for the enchanting table.

#### Breaking the 30-level barrier

Suludom adds two blocks that in a structure raise the 30-level limit for
enchanting.

##### Orthogonal pylons

Orthogonal pylons are crafted as such:

```
MIM  M = magma cream   D = block of diamond
GDG  I = eye of ender  Q = block of quartz
QQQ  G = glowstone
```

An orthogonal pylon is placed on top of four quartz pillars, four blocks away
from an enchanting table. See the top and side views below:

```
Top            Side
(base level)
                P
                Q
'''             Q
'Q'--T          Q
'''            'Q'--T

Legend:
Q = quartz pillar (oriented vertically)
T = enchanting table
P = orthogonal pylon
' = must not have a solid block in this space
- = can be any block; used for illustration purposes
```

An active orthogonal pylon will emit particles toward the enchanting table and
increase the level limit from bookshelves by 5. A total of four orthogonal
pylons can be placed, allowing enchantments to reach level 50 (with sufficient
bookshelves).

##### Diagonal pylons

Diagonal pylons are crafted as such:

```
MIM  M = end rod       D = nether star
GDG  I = end crystal   Q = purpur block
QQQ  G = end stone bricks
```

A diagonal pylon is placed on top of four vertical purpur pillars, four blocks
away *diagonally* from an enchanting table. It has the same obstruction
requirements as the orthogonal pylon.

An active diagonal pylon will emit particles toward the enchanting table and
increase the level limit (after factoring orthogonal pylons) by 25%. Up to
four diagonal pylons can be placed, doubling the level limit. With four
orthogonal pylons and four diagonal pylons active, an enchanting table can
provide level 100 enchantments.

##### Overenchanting and blood lapis

Sufficient enchanting power can allow an enchantment to break its natural
limits; this mechanic is known as *overenchanting*. As a result, one can obtain
tools with, for instance, Efficiency VII or Fortune IV. However, note that
anvils are blissfully ignorant about such tools, so getting an overenchanted
book is somewhat pointless.

Overenchanting requires a new materials instead of lapis. To find *blood
lapis*, go to the nether and look for red streaks in the netherrack. This blood
lapis ore can be mined with a *reinforced* pickaxe, but beware! Make sure you
have less than two hearts of health before you claim your goods, or you'll get
a surprising blast.

#### New enchantments

##### Weapons

* **Tidal Edge** increases damage dealt against mobs susceptible to water
  damage: blazes, magma cubes and endermen.
* **Endless** increases damage dealt against ender mobs.
* **Defuser** increases damage dealt against explosive mobs.
* **Pork Chopper** increases damage dealt against pig mobs.
* **Curse of Mercy** increases damage dealt, but prevents the weapon from
  dealing a fatal blow. Conflicts with both Fire Aspect and Looting.
* **Corrupted Edge** inflicts status effects on hits but reduces attack damage.

##### New enchantables

Blast furnaces and smokers can be enchanted with the following enchantments:

* **Efficiency** increases the rate of smelting (but does not affect the number
  of items smelted per fuel).
* **Frugal** increases the number of items smelted per fuel.
* **Fortune** (Blast Furnace) increases the yield of certain items.

Scrapping tables can be enchanted with the following enchantments:

* **Fortune** increases the yield of materials.
* **Runic Loopback** allows lapis lazuli and blood lapis to be recovered from
  enchanted items.

*(Note: the enchanted books for Frugal and Runic Loopback are currently in
the Combat section.)*

##### Compatibility changes

* Damage enchantments no longer conflict with each other unless one of them
  is Sharpness.

### Configuration

In the `config/suludom.json5` file.

### For server owners

If you're holding a piece of Suludom gear, then `/surecalculate` will
recalculate its stats based on its construction. Useful when a material's
properties have been modified.
