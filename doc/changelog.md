## 0.1.1

* Update to 1.16.1
* Paper rods now give a bonus to augment capacity as intended
* Add new material: Dense Iron Ingot. Can be used as-is, but will also be used for crafting future materials.
* Add the Crystal Matrix (will be used for crafting future materials)
* Nerf Fortune for blast furnaces (old formula was `1 + randint(1 + level)`; new formula is `(5 + randint(5 + 2 * level)) / 5`.
  For comparison, a Fortune IV blast furnace gave an average of 3 items per eligible ore; now it gives about
  1.84 items per smelt.
* Add an Easter egg

## 0.1.0

Initial 1.16 port.
