## How to add new \<thing\> to Suludom

Suludom aims to be data-driven and expose custom features through data packs.

### What you can add

* New materials
* Recipes for tools

### Materials

Materials are defined in the `data/<namespace>/materials/` directory. To add
a material of `<type>` from an item `<namespace>:<item_name>`, create the
file `data/<namespace>/materials/<type>/<item_name>.json`.

#### Tool materials

```json
{
  "miningLevel": 2,
  "durability": 250,
  "miningSpeed": 6.0,
  "damage": 6.0,
  "attackSpeedBonus": 0.0,
  "enchantability": 14,
  "toolHeadColor": 14211288,
  "materialCategory": "metal",
  "enchantmentAffinities": {
    "minecraft:sharpness": 50,
    "minecraft:sweeping": 4,
    "minecraft:efficiency": 50,
    "minecraft:unbreaking": 32
  }
}
```

* `miningLevel`: the mining level of a tool made out of this material, where
  0 is wood, 1 is stone, 2 is iron and 3 is diamond.
* `durability`: how many uses a tool made out of this material has until it
  breaks.
* `miningSpeed`: the mining speed. Wood has 2.0, stone has 4.0, iron has 6.0,
  diamond has 8.0 and gold has 12.0.
* `damage`: the amount of damage dealt by a sword of this material.
* `attackSpeedBonus`: the bonus or penalty that this material gives to attack
  speed.
* `enchantability`: the enchantability of a tool of this material. Wood has 15,
  stone has 5, iron has 14, diamond has 10 and gold has 22.
* `toolHeadColor`: the color of the tool head, given in 0xRRGGBB format.
* `materialCategory`: one of `wood`, `stone`, `metal`, `gem` or `cloth`.
  Determines which materials this one synergizes well with.
* `enchantmentAffinities`: see the relevant section.

#### Tool rod materials

```json
{
  "durabilityBonus": 0,
  "miningSpeedBonus": 0,
  "damageBonus": 0,
  "attackSpeedBonus": 0,
  "enchantabilityBonus": 0,
  "rodColor": 7690273
}
```

* `durabilityBonus`: the bonus to the tool's durability.
* `miningSpeedBonus`: the bonus to the tool's mining speed.
* `damageBonus`: the bonus to the tool's damage.
* `attackSpeedBonus`: the bonus to the tool's attack speed.
* `enchantabilityBonus`: the bonus to the tool's enchantability.
* `rodColor`: the color of the rod, given in 0xRRGGBB format.

Negative values can be given to bonuses to make them penalties.

#### Armor materials

```json
{
  "durabilityFactor": 15,
  "armor": 15,
  "armorToughness": 0,
  "enchantability": 9,
  "armorColor": 14211288,
  "materialCategory": "metal",
  "enchantmentAffinities": {
    "minecraft:protection": 20,
    "minecraft:unbreaking": 32
  }
}
```

* `durabilityFactor`: multiplied by [13, 15, 16, 11] to get the true
  durabilities.
* `armor`: the amount of protection from a full set of armor. These are
  multiplied by [3/20, 8/20, 6/20, 3/20] to get the amount of protection for
  each piece.
* `armorToughness`: the amount of armor toughness given from each piece.
* `enchantability`: the enchantability of a piece of armor of this material.
* `armorColor`: the color of the armor, given in 0xRRGGBB format.
* `materialCategory`: one of `wood`, `stone`, `metal`, `gem` or `cloth`.
  Determines which materials this one synergizes well with.
* `enchantmentAffinities`: see the relevant section.

#### Augment materials

Augments are treated as materials of their own.

```json
{
  "durabilityBonus": 80,
  "miningSpeedBonus": 0.5,
  "meleeDamageBonus": 1.0,
  "rangedDamageBonus": 1.5,
  "attackSpeedBonus": 0,
  "enchantabilityBonus": 0,
  "armorBonus": 2.5,
  "armorToughnessBonus": 0,
  "enchantmentAffinities": {},
  "itemCount": 1
}
```

* `durabilityBonus`: the bonus to durability.
* `miningSpeedBonus`: the bonus to mining speed.
* `meleeDamageBonus`: the bonus to melee damage.
* `rangedDamageBonus`: the bonus to ranged damage.
* `attackSpeedBonus`: the bonus to attack speed.
* `enchantabilityBonus`: the bonus to enchantability.
* `armorBonus`: the bonus to protection, given as the value for a full set of
  armor.
* `armorToughnessBonus`: the bonus to armor toughness per piece.
* `enchantmentAffinities`: see the relevant section.
* `itemCount`: the number of this item needed for the augment.

#### Enchantment affinity

Tools have affinities for or against certain enchantments, depending on the
materials they were made from. The values for these affinities are added to
or subtracted from the weights given in [this page][enchantment weights],
multiplied by 16, to give the actual weights.

In JSON, these values are represented as an object with the enchantment IDs
as the keys and the bonuses as the values.

[enchantment weights]: https://minecraft.gamepedia.com/Mechanics/Enchanting#Step_three_.E2.80.93_Select_a_set_of_enchantments_from_the_list

### Tool table recipes

Unfortunately, you can't add new tool types through data packs just yet, but
you can add recipes for them:

```json
{
  "type": "suludom:tool_table_recipe",
  "components": [
    {"type": "tool_material"},
    {"type": "tool_material"},
    {"type": "tool_material"},
    {"type": "empty"},
    {"type": "rod_material"},
    {"type": "empty"},
    {"type": "empty"},
    {"type": "rod_material"},
    {"type": "empty"}
  ],
  "outputItem": "suludom:generic_pickaxe"
}
```

* `type`: it is `suludom:tool_table_recipe`.
* `components`: an array of nine elements describing the items needed to craft
  the item in question. For `type`:
  * `tool_material`, `rod_material`, `armor_material`: any {tool, rod, armor}
    material
  * `empty`: empty
  * `exact`: exactly the item in the `item` field
* `outputItem`: the ID of the item you get.

### What you can't add yet

* Material types can be added only through code. In addition, you have to make
  sure that their ordinals don't conflict. Basically, don't even try.
* Material categories are hardcoded.
