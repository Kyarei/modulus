package flirora.suludom.loot;

import flirora.suludom.SuludomMod;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonSerializer;
import net.minecraft.util.registry.Registry;

public class SuludomLootFunctionTypes {
  public static final LootFunctionType RECALCULATE_COMPOSITE =
      register("recalculate_composite",
          new RecalculateCompositeLootFunction.Serializer());

  private static LootFunctionType register(String id,
      JsonSerializer<? extends LootFunction> jsonSerializer) {
    return Registry.register(Registry.LOOT_FUNCTION_TYPE,
        new Identifier(SuludomMod.MOD_ID, id),
        new LootFunctionType(jsonSerializer));
  }
}
