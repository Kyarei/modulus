package flirora.suludom.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import flirora.suludom.item.tool.GenericGear;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctionType;
import net.minecraft.util.JsonSerializer;

public class RecalculateCompositeLootFunction implements LootFunction {
  public static RecalculateCompositeLootFunction INSTANCE =
      new RecalculateCompositeLootFunction();

  private RecalculateCompositeLootFunction() {
  }

  @Override public ItemStack apply(ItemStack stack, LootContext ctx) {
    if (stack.getItem() instanceof GenericGear) {
      ((GenericGear) stack.getItem()).recalculateComposite(stack);
    }
    return stack;
  }

  @Override public LootFunctionType getType() {
    return SuludomLootFunctionTypes.RECALCULATE_COMPOSITE;
  }

  public static class Serializer
      implements JsonSerializer<RecalculateCompositeLootFunction> {
    @Override
    public void toJson(JsonObject json, RecalculateCompositeLootFunction object,
        JsonSerializationContext context) {

    }

    @Override public RecalculateCompositeLootFunction fromJson(JsonObject json,
        JsonDeserializationContext context) {
      return RecalculateCompositeLootFunction.INSTANCE;
    }
  }
}
