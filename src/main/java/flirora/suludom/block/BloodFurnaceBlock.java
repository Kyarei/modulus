package flirora.suludom.block;

import java.util.List;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.item.tool.TooltipUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class BloodFurnaceBlock extends Block implements BlockEntityProvider {

  public static final Identifier ID =
      new Identifier("suludom", "blood_furnace");

  public BloodFurnaceBlock(Settings settings) {
    super(settings);
    setDefaultState(
        getStateManager().getDefaultState().with(Properties.LIT, false));
  }

  @Override
  public BlockEntity createBlockEntity(BlockView world) {
    return new BloodFurnaceBlockEntity();
  }

  @Override
  protected void appendProperties(Builder<Block, BlockState> builder) {
    builder.add(Properties.LIT);
  }

  @Override
  public ActionResult onUse(BlockState state, World world, BlockPos pos,
      PlayerEntity player, Hand hand, BlockHitResult hit) {
    if (!world.isClient) {
      BlockEntity be = world.getBlockEntity(pos);
      if (be != null && be instanceof BloodFurnaceBlockEntity) {
        ContainerProviderRegistry.INSTANCE.openContainer(ID, player, (buf) -> {
          buf.writeBlockPos(pos);
        });
      }
    }
    return ActionResult.SUCCESS;
  }

  @Override
  public void onStateReplaced(BlockState state, World world, BlockPos pos,
      BlockState newState, boolean moved) {
    if (state.getBlock() != newState.getBlock()) {
      BlockEntity blockEntity = world.getBlockEntity(pos);
      if (blockEntity instanceof Inventory) {
        ItemScatterer.spawn(world, pos, (Inventory) blockEntity);
        world.updateComparators(pos, this);
      }

      super.onStateReplaced(state, world, pos, newState, moved);
    }
  }

  @Override
  @Environment(EnvType.CLIENT)
  public void buildTooltip(ItemStack stack, @Nullable BlockView view,
      List<Text> tooltip, TooltipContext options) {
    TooltipUtils.addDescriptionTooltips(tooltip,
        "block.suludom.blood_furnace.tooltip_1",
        "block.suludom.blood_furnace.tooltip_2");
  }

}
