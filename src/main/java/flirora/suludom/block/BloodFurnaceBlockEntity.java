package flirora.suludom.block;

import flirora.suludom.SuludomMod;
import flirora.suludom.block.gui.BloodFurnaceController;
import flirora.suludom.recipe.BloodSmeltingRecipe;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeFinder;
import net.minecraft.recipe.RecipeInputProvider;
import net.minecraft.recipe.RecipeUnlocker;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;

import java.util.Optional;

public class BloodFurnaceBlockEntity extends LockableContainerBlockEntity
    implements SidedInventory, RecipeUnlocker, RecipeInputProvider, Tickable,
    BlockEntityClientSerializable, NamedScreenHandlerFactory {
  private static final int[] TOP_SLOTS = new int[] { 0, 1 };
  private static final int[] BOTTOM_SLOTS = new int[] { 3, 4 };
  private static final int[] SIDE_SLOTS = new int[] { 2 };

  public static final int ENDER_EYE_BURN_TIME = 400;
  private int burnTime;
  private int cookTime;
  private int totalCookTime;
  private DefaultedList<ItemStack> inventory;
  private BloodSmeltingRecipe currentRecipe;

  public BloodFurnaceBlockEntity() {
    super(SuludomMod.BLOOD_FURNACE_BLOCK_ENTITY);
    inventory = DefaultedList.ofSize(5, ItemStack.EMPTY);
  }

  public boolean isBurning() {
    return burnTime > 0;
  }

  @Override
  public void fromTag(BlockState state, CompoundTag tag) {
    super.fromTag(state, tag);
    burnTime = tag.getInt("BurnTime");
    cookTime = tag.getInt("CookTime");
    totalCookTime = tag.getInt("TotalCookTime");
    Inventories.fromTag(tag, inventory);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    tag.putInt("BurnTime", burnTime);
    tag.putInt("CookTime", cookTime);
    tag.putInt("TotalCookTime", totalCookTime);
    Inventories.toTag(tag, inventory);
    return super.toTag(tag);
  }

  @Override
  public int size() {
    return inventory.size();
  }

  @Override
  public boolean isEmpty() {
    return inventory.isEmpty();
  }

  @Override
  public ItemStack getStack(int slot) {
    return inventory.get(slot);
  }

  @Override
  public ItemStack removeStack(int slot, int amount) {
    return Inventories.splitStack(inventory, slot, amount);
  }

  @Override
  public ItemStack removeStack(int slot) {
    return Inventories.removeStack(inventory, slot);
  }

  @Override
  public void setStack(int slot, ItemStack input) {
    ItemStack existing = (ItemStack) this.inventory.get(slot);
    boolean shouldMerge =
        !input.isEmpty() && input.isItemEqualIgnoreDamage(existing)
            && ItemStack.areTagsEqual(input, existing);
    this.inventory.set(slot, input);
    if (input.getCount() > this.getMaxCountPerStack()) {
      input.setCount(this.getMaxCountPerStack());
    }
    if (slot < 2 && !shouldMerge) {
      // startSmelting();
    }
    markDirty();
  }

  @Override
  public boolean canPlayerUse(PlayerEntity player) {
    if (this.world.getBlockEntity(this.pos) != this) {
      return false;
    } else {
      return player.squaredDistanceTo((double) this.pos.getX() + 0.5D,
          (double) this.pos.getY() + 0.5D,
          (double) this.pos.getZ() + 0.5D) <= 64.0D;
    }
  }

  @Override
  public void clear() {
    inventory.clear();
  }

  private boolean canAcceptRecipeOutput() {
    if (currentRecipe == null)
      return false;
    ItemStack recipeOutput = currentRecipe.getOutput();
    ItemStack secondaryOutput = currentRecipe.getSecondaryOutput();
    ItemStack outputSlot = inventory.get(3);
    ItemStack secondaryOutputSlot = inventory.get(4);
    if (!secondaryOutput.isEmpty() && !secondaryOutputSlot
        .isEmpty() && !(secondaryOutput.getItem() == secondaryOutputSlot
        .getItem() && secondaryOutput.getCount() + secondaryOutputSlot
        .getCount() <= secondaryOutputSlot.getMaxCount())) {
      return false;
    }
    if (outputSlot.isEmpty())
      return true;
    return recipeOutput.getItem() == outputSlot.getItem() && recipeOutput
        .getCount() + outputSlot.getCount() <= outputSlot.getMaxCount();
  }

  private void startSmelting() {
    if (currentRecipe == null) {
      // We don't have the recipe cached.
      Optional<BloodSmeltingRecipe> match = world.getRecipeManager()
          .getFirstMatch(BloodSmeltingRecipe.Type.INSTANCE, this, world);
      if (!match.isPresent()) {
        // Recipe was taken away
        cookTime = 0;
      } else {
        currentRecipe = match.get();
        totalCookTime = currentRecipe.getCookTime();
      }
    } else {
      if (!currentRecipe.matches(this, world)) {
        // Recipe has been invalidated
        currentRecipe = null;
        cookTime = 0;
      }
    }
    if (!isBurning()) {
      // Can we start a recipe?
      ItemStack fuel = inventory.get(2);
      if (fuel.getItem() == Items.ENDER_EYE && fuel.getCount() >= 1
          && currentRecipe != null && canAcceptRecipeOutput()) {
        // Start recipe
        burnTime = ENDER_EYE_BURN_TIME;
        removeStack(2, 1);
      }
    }
  }

  @Override
  public void tick() {
    boolean burning = isBurning();
    if (burning)
      --burnTime;
    if (!world.isClient) {
      startSmelting();
      if (currentRecipe != null) {
        if (burnTime > 0) {
          --burnTime;
          ++cookTime;
        } else {
          cookTime -= 2;
          if (cookTime < 0)
            cookTime = 0;
        }
        if (cookTime >= totalCookTime && canAcceptRecipeOutput()) {
          removeStack(0, 1);
          removeStack(1, 1);
          ItemStack output = inventory.get(3);
          ItemStack secondaryOutputSlot = inventory.get(4);
          if (output.isEmpty()) {
            inventory.set(3, currentRecipe.getOutput().copy());
          } else {
            output.increment(currentRecipe.getOutput().getCount());
          }
          if (world.random.nextDouble() < currentRecipe
              .getSecondaryOutputChance()) {
            if (secondaryOutputSlot.isEmpty()) {
              inventory.set(4, currentRecipe.getSecondaryOutput().copy());
            } else {
              secondaryOutputSlot
                  .increment(currentRecipe.getSecondaryOutput().getCount());
            }
          }
          cookTime = 0;
        }
      }
      sync();
    }
    if (burning != isBurning()) {
      world.setBlockState(pos, SuludomMod.BLOOD_FURNACE.getDefaultState()
          .with(Properties.LIT, isBurning()), 3);
    }
    markDirty();
  }

  @Override
  public void provideRecipeInputs(RecipeFinder finder) {
    for (ItemStack stack : inventory) {
      finder.addItem(stack);
    }
  }

  @Override
  public void setLastRecipe(Recipe<?> recipe) {
    // TODO Auto-generated method stub
  }

  @Override
  public Recipe<?> getLastRecipe() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int[] getAvailableSlots(Direction side) {
    if (side == Direction.DOWN) {
      return BOTTOM_SLOTS;
    } else {
      return side == Direction.UP ? TOP_SLOTS : SIDE_SLOTS;
    }
  }

  @Override
  public boolean canInsert(int slot, ItemStack stack, Direction dir) {
    return this.isValid(slot, stack);
  }

  @Override
  public boolean canExtract(int slot, ItemStack stack, Direction dir) {
    return true;
  }

  @Override
  protected Text getContainerName() {
    return new TranslatableText("block.suludom.blood_furnace");
  }

  @Override
  protected ScreenHandler createScreenHandler(int i,
      PlayerInventory playerInventory) {
    // TODO Auto-generated method stub
    return null;
  }

  public float getSmeltingProgress() {
    if (totalCookTime == 0)
      return 0.0f;
    return ((float) cookTime) / totalCookTime;
  }

  public float getHeatLeft() {
    return ((float) burnTime) / ENDER_EYE_BURN_TIME;
  }

  @Override
  public void fromClientTag(CompoundTag tag) {
    burnTime = tag.getInt("BurnTime");
    cookTime = tag.getInt("CookTime");
    totalCookTime = tag.getInt("TotalCookTime");
  }

  @Override public CompoundTag toClientTag(CompoundTag tag) {
    tag.putInt("BurnTime", burnTime);
    tag.putInt("CookTime", cookTime);
    tag.putInt("TotalCookTime", totalCookTime);
    return tag;
  }

  @Override public Text getDisplayName() {
    return new TranslatableText("block.suludom.blood_furnace");
  }

  @Override public ScreenHandler createMenu(int syncId, PlayerInventory inv,
      PlayerEntity player) {
    return new BloodFurnaceController(syncId, inv,
        ScreenHandlerContext.create(world, pos));
  }

}
