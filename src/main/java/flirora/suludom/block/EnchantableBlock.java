package flirora.suludom.block;

import java.util.function.Function;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public interface EnchantableBlock {
  default int getEnchantability() {
    return 1;
  }

  public static boolean isOne(Item item) {
    return item instanceof BlockItem
        && ((BlockItem) item).getBlock() instanceof EnchantableBlock;
  }

  public static <T> T getPropertyOrDefault(Item item,
      Function<EnchantableBlock, T> f, T d) {
    if (item instanceof BlockItem
        && ((BlockItem) item).getBlock() instanceof EnchantableBlock) {
      return f.apply((EnchantableBlock) ((BlockItem) item).getBlock());
    }
    return d;
  }
}
