package flirora.suludom.block;

import blue.endless.jankson.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion.DestructionType;

public class BloodLapisOreBlock extends Block {
  public final static Identifier ID =
      new Identifier("suludom", "blood_lapis_ore");

  public BloodLapisOreBlock(Settings settings) {
    super(settings);
  }

  @Override
  public void afterBreak(World world, PlayerEntity player, BlockPos pos,
      BlockState state, @Nullable BlockEntity blockEntity, ItemStack stack) {
    if (player.getHealth() < 4.0f) {
      super.afterBreak(world, player, pos, state, blockEntity, stack);
    } else {
      world.createExplosion(null, pos.getX() + 0.5, pos.getY() + 0.5,
          pos.getZ() + 0.5, 3, true, DestructionType.DESTROY);
    }
  }

}
