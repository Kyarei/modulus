package flirora.suludom.block;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;

public class OutputSlot implements ImplementedInventory {
  private final DefaultedList<ItemStack> items = DefaultedList.ofSize(1,
      ItemStack.EMPTY);
  private final Inventory inputInventory;

  public OutputSlot(Inventory inputInventory) {
    this.inputInventory = inputInventory;
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  @Override
  public int[] getAvailableSlots(Direction side) {
    return new int[] {};
  }

  public ItemStack removeStack(int slot, int amount) {
    return Inventories.removeStack(items, 0);
  }

  public ItemStack removeStack(int slot) {
    return Inventories.removeStack(items, 0);
  }

  public boolean isValid(int slot, ItemStack stack) {
    return false;
  }

  public Inventory getInputInventory() {
    return inputInventory;
  }

  public void setOutput(ItemStack s) {
    items.set(0, s);
  }

  public boolean canPlayerUse(PlayerEntity player) {
    return true;
  }

}
