package flirora.suludom.block;

import java.util.List;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.item.tool.TooltipUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;

public class OrthogonalPylonBlock extends Block {
  public static final Identifier ID =
      new Identifier("suludom", "orthogonal_pylon");
  protected static final VoxelShape SHAPE =
      Block.createCuboidShape(0.0, 0.0, 0.0, 16.0, 12.0, 16.0);

  public OrthogonalPylonBlock(Settings settings) {
    super(settings);
  }

  @Override
  public VoxelShape getOutlineShape(BlockState state, BlockView view,
      BlockPos pos, ShapeContext ePos) {
    return SHAPE;
  }

  @Override
  @Environment(EnvType.CLIENT)
  public void buildTooltip(ItemStack stack, @Nullable BlockView view,
      List<Text> tooltip, TooltipContext options) {
    TooltipUtils.addDescriptionTooltips(tooltip,
        "block.suludom.orthogonal_pylon.tooltip_1",
        "block.suludom.orthogonal_pylon.tooltip_2",
        "block.suludom.orthogonal_pylon.tooltip_3",
        "block.suludom.orthogonal_pylon.tooltip_4",
        "block.suludom.orthogonal_pylon.tooltip_5",
        "block.suludom.orthogonal_pylon.tooltip_6",
        "block.suludom.orthogonal_pylon.tooltip_7",
        "block.suludom.orthogonal_pylon.tooltip_8",
        "block.suludom.orthogonal_pylon.tooltip_9");
  }

}
