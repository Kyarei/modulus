package flirora.suludom.block;

import flirora.suludom.SuludomMod;
import flirora.suludom.block.gui.ToolTableController;
import flirora.suludom.item.tool.GenericArmor;
import flirora.suludom.item.tool.RecipeResult;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.recipe.ToolTableRecipe;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;

import java.util.Optional;

public class ToolTableBlockEntity extends BlockEntity
    implements ImplementedInventory, NamedScreenHandlerFactory {
  private final DefaultedList<ItemStack> items =
      DefaultedList.ofSize(9, ItemStack.EMPTY);
  private final OutputSlot outputSlot = new OutputSlot(this);
  private boolean dirty;

  public ToolTableBlockEntity() {
    super(SuludomMod.TOOL_TABLE_BLOCK_ENTITY);
  }

  public OutputSlot getOutputSlot() {
    return outputSlot;
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  @Override
  public void fromTag(BlockState state, CompoundTag tag) {
    super.fromTag(state, tag);
    Inventories.fromTag(tag, items);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    return super.toTag(tag);
  }

  @Override
  public void markDirty() {
    dirty = true;
  }

  public boolean isDirty() {
    return dirty;
  }

  public void clearDirty() {
    dirty = false;
  }

  public void recalculateOutput() {
    Optional<ToolTableRecipe> match = world.getRecipeManager()
        .getFirstMatch(ToolTableRecipe.Type.INSTANCE, this, world);
    if (match.isPresent()) {
      ToolTableRecipe theMatch = match.get();
      ItemStack stack = theMatch.getOutput().copy();
      RecipeResult result = theMatch.tryRecipe(this);
      EquipmentSlot slot = stack.getItem() instanceof GenericArmor ?
          ((GenericArmor) stack.getItem()).getSlotType() :
          null;
      CompositeMaterial material = result.makeCompositeMaterial(slot);
      stack.getOrCreateTag()
          .put("RecipeResult", result.toTag(new CompoundTag()));
      stack.getTag()
          .put("CompositeMaterial", material.toTag(new CompoundTag()));
      outputSlot.setOutput(stack);
    } else {
      outputSlot.setOutput(ItemStack.EMPTY);
    }
  }

  @Override public Text getDisplayName() {
    return new TranslatableText("block.suludom.tool_table");
  }

  @Override public ScreenHandler createMenu(int syncId, PlayerInventory inv,
      PlayerEntity player) {
    return new ToolTableController(syncId, inv,
        ScreenHandlerContext.create(world, pos));
  }
}
