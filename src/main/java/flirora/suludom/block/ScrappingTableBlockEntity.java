package flirora.suludom.block;

import flirora.suludom.SuludomMod;
import flirora.suludom.block.gui.ScrappingTableController;
import flirora.suludom.enchanting.EnchantingUtils;
import flirora.suludom.enchanting.SuludomEnchantments;
import flirora.suludom.item.tool.GenericGear;
import flirora.suludom.item.tool.RecipeResult;
import flirora.suludom.material.augment.AugmentMaterial;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;

import java.util.Map;
import java.util.Map.Entry;

public class ScrappingTableBlockEntity extends BlockEntity
    implements ImplementedInventory, EnchantableBlockEntity,
    NamedScreenHandlerFactory {
  private final double SCRAPPING_CHANCE = 0.25;

  // Slot 0 is input, 1 – 15 are outputs
  private final DefaultedList<ItemStack> items =
      DefaultedList.ofSize(16, ItemStack.EMPTY);
  private Object2IntMap<Enchantment> enchantments = null;

  public ScrappingTableBlockEntity() {
    super(SuludomMod.SCRAPPING_TABLE_BLOCK_ENTITY);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  @Override
  public void fromTag(BlockState state, CompoundTag tag) {
    super.fromTag(state, tag);
    Inventories.fromTag(tag, items);
    enchantments =
        EnchantingUtils.enchantmentsFromTag(tag.getList("Enchantments", 10));
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    tag.put("Enchantments", EnchantingUtils.enchantmentsToTag(enchantments));
    return super.toTag(tag);
  }

  private int getFortuneLevel() {
    return this.getLevelOf(Enchantments.FORTUNE);
  }

  private int getRunicLoopbackLevel() {
    return this.getLevelOf(SuludomEnchantments.RUNIC_LOOPBACK);
  }

  private static double scrappingChanceMultiplierFromFortune(int level) {
    /*
     * No Fortune: x1
     * 
     * Fortune I: x1.25
     * 
     * Fortune II: x1.4
     * 
     * Fortune III: x1.5
     */
    return 2 - 3 / (level + 3);
  }

  private void giveItem(Item item, int recoveredCount) {
    if (recoveredCount == 0)
      return;
    boolean found = false;
    for (int i = 1; i < 16; ++i) {
      ItemStack output = items.get(i);
      if (output.getItem() == item
          && output.getCount() < output.getMaxCount()) {
        int increment =
            Math.min(recoveredCount, output.getMaxCount() - output.getCount());
        output.increment(increment);
        recoveredCount -= increment;
      }
      if (output.isEmpty()) {
        int increment =
            Math.min(recoveredCount, output.getMaxCount() - output.getCount());
        items.set(i, new ItemStack(item, increment));
        recoveredCount -= increment;
      }
      if (recoveredCount <= 0) {
        found = true;
        break;
      }
    }
    if (!found) {
      DefaultedList<ItemStack> list = DefaultedList.of();
      list.add(new ItemStack(item, recoveredCount));
      // Could not find any empty slot; scatter into the world
      ItemScatterer.spawn(world, pos, list);
    }
  }

  public boolean scrap() {
    ItemStack input = items.get(0);
    Item inputItem = input.getItem();
    if (!(inputItem instanceof GenericGear))
      return false;
    GenericGear inputGear = (GenericGear) inputItem;
    RecipeResult recipeResult = GenericGear.getRecipeResultFrom(input);
    double fractionDurabilityLeft =
        1 - input.getDamage() / inputGear.getStackMaxDamage(input);
    double scrappingChance = SCRAPPING_CHANCE * fractionDurabilityLeft
        * scrappingChanceMultiplierFromFortune(getFortuneLevel());
    recipeResult.forEachMaterial((material) -> {
      Item item = Registry.ITEM.get(material.getId());
      int totalCount = (material instanceof AugmentMaterial)
          ? ((AugmentMaterial) material).getItemCount()
          : 1;
      int recoveredCount = 0;
      for (int i = 0; i < totalCount; ++i) {
        if (world.random.nextDouble() < scrappingChance)
          ++recoveredCount;
      }
      giveItem(item, recoveredCount);
    });
    int runicLoopbackLevel = getRunicLoopbackLevel();
    if (runicLoopbackLevel > 0) {
      int eligibleLapis = 0;
      int eligibleBloodLapis = 0;
      Map<Enchantment, Integer> toolEnchantments = EnchantmentHelper.get(input);
      for (Entry<Enchantment, Integer> entry : toolEnchantments.entrySet()) {
        Enchantment enchantment = entry.getKey();
        int level = entry.getValue();
        int minPower = enchantment.getMinPower(level);
        int maxPower = enchantment.getMaxPower(level);
        int power = world.random.nextInt(maxPower - minPower) + minPower;
        eligibleLapis += power;
        int overenchantment = level - enchantment.getMaxLevel();
        if (overenchantment > 0)
          eligibleBloodLapis += overenchantment;
      }
      int recoveredLapis = 0;
      int recoveredBloodLapis = 0;
      double lapisRecoveryChance = 0.005 + 0.001 * runicLoopbackLevel;
      double bloodLapisRecoveryChance = 0.2 + 0.05 * runicLoopbackLevel;
      for (int i = 0; i < eligibleLapis; ++i) {
        if (world.random.nextDouble() < lapisRecoveryChance)
          ++recoveredLapis;
      }
      for (int i = 0; i < eligibleBloodLapis; ++i) {
        if (world.random.nextDouble() < bloodLapisRecoveryChance)
          ++recoveredBloodLapis;
      }
      giveItem(Items.LAPIS_LAZULI, recoveredLapis);
      giveItem(SuludomMod.BLOOD_LAPIS, recoveredBloodLapis);
    }
    input.decrement(1);
    if (!world.isClient) {
      world.playSound(null, pos, SoundEvents.BLOCK_GRINDSTONE_USE,
          SoundCategory.BLOCKS, 1.0f, 1.0f);
    }
    return true;
  }

  @Override public Object2IntMap<Enchantment> getEnchantments() {
    return enchantments;
  }

  @Override
  public void setEnchantments(Object2IntMap<Enchantment> enchantments) {
    this.enchantments = enchantments;
  }

  @Override public Text getDisplayName() {
    return new TranslatableText("block.suludom.scrapping_table");
  }

  @Override public ScreenHandler createMenu(int syncId, PlayerInventory inv,
      PlayerEntity player) {
    return new ScrappingTableController(syncId, inv,
        ScreenHandlerContext.create(world, pos));
  }

}
