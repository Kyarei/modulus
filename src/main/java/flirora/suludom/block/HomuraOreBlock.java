package flirora.suludom.block;

import blue.endless.jankson.annotation.Nullable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class HomuraOreBlock extends Block {
  public final static Identifier ID = new Identifier("suludom", "homura_ore");

  public HomuraOreBlock(Settings settings) {
    super(settings);
  }

  @Override
  public void afterBreak(World world, PlayerEntity player, BlockPos pos,
      BlockState state, @Nullable BlockEntity blockEntity, ItemStack stack) {
    super.afterBreak(world, player, pos, state, blockEntity, stack);
    player.setOnFireFor(Integer.MAX_VALUE);
  }

}
