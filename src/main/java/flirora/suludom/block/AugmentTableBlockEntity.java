package flirora.suludom.block;

import flirora.suludom.SuludomMod;
import flirora.suludom.SuludomRegistry;
import flirora.suludom.block.gui.AugmentTableController;
import flirora.suludom.item.tool.GenericGear;
import flirora.suludom.item.tool.RecipeResult;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.augment.AugmentMaterial;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;

import java.util.function.Consumer;

public class AugmentTableBlockEntity extends BlockEntity
    implements ImplementedInventory, NamedScreenHandlerFactory {
  private final DefaultedList<ItemStack> items =
      DefaultedList.ofSize(2, ItemStack.EMPTY);
  private final OutputSlot outputSlot = new OutputSlot(this);
  private boolean dirty;

  public AugmentTableBlockEntity() {
    super(SuludomMod.AUGMENT_TABLE_BLOCK_ENTITY);
  }

  @Override
  public DefaultedList<ItemStack> getItems() {
    return items;
  }

  public OutputSlot getOutputSlot() {
    return outputSlot;
  }

  @Override
  public void fromTag(BlockState state, CompoundTag tag) {
    super.fromTag(state, tag);
    Inventories.fromTag(tag, items);
  }

  @Override
  public CompoundTag toTag(CompoundTag tag) {
    Inventories.toTag(tag, items);
    return super.toTag(tag);
  }

  @Override
  public void markDirty() {
    dirty = true;
  }

  public boolean isDirty() {
    return dirty;
  }

  public void clearDirty() {
    dirty = false;
  }

  public void recalculateOutput(Consumer<Text> statusTextCallback) {
    statusTextCallback.accept(new LiteralText(""));
    ItemStack toolStack = items.get(0);
    ItemStack augmentStack = items.get(1);
    if (!(toolStack.getItem() instanceof GenericGear)) {
      outputSlot.setStack(0, ItemStack.EMPTY);
      return;
    }
    GenericGear tool = (GenericGear) toolStack.getItem();
    CompositeMaterial composite =
        GenericGear.getCompositeMaterialFrom(toolStack);
    RecipeResult result = GenericGear.getRecipeResultFrom(toolStack);
    if (result.getAugmentMaterials().size() >= composite.getAugmentCapacity()) {
      outputSlot.setStack(0, ItemStack.EMPTY);
      statusTextCallback
          .accept(new TranslatableText("container.augment_table.full"));
      return;
    }
    AugmentMaterial augment = SuludomRegistry.getAugmentMaterialRegistry()
        .get(Registry.ITEM.getId(augmentStack.getItem()));
    if (augment == null || augment.getItemCount() > augmentStack.getCount()) {
      outputSlot.setStack(0, ItemStack.EMPTY);
      return;
    }
    if (result.getAugmentMaterials().contains(augment)) {
      outputSlot.setStack(0, ItemStack.EMPTY);
      statusTextCallback
          .accept(new TranslatableText("container.augment_table.already_used"));
      return;
    }
    ItemStack output = toolStack.copy();
    result.addAugmentMaterial(augment);
    output.getOrCreateTag()
        .put("RecipeResult", result.toTag(new CompoundTag()));
    tool.recalculateComposite(output);
    outputSlot.setStack(0, output);
  }

  @Override public Text getDisplayName() {
    return new TranslatableText("block.suludom.augment_table");
  }

  @Override public ScreenHandler createMenu(int syncId, PlayerInventory inv,
      PlayerEntity player) {
    return new AugmentTableController(syncId, inv,
        ScreenHandlerContext.create(world, pos));
  }

}
