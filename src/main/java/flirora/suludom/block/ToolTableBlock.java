package flirora.suludom.block;

import java.util.List;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.item.tool.TooltipUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class ToolTableBlock extends Block implements BlockEntityProvider {
  public static final Identifier ID = new Identifier("suludom", "tool_table");

  public ToolTableBlock(Settings settings) {
    super(settings);
  }

  @Override
  public BlockEntity createBlockEntity(BlockView view) {
    return new ToolTableBlockEntity();
  }

  @Override
  public ActionResult onUse(BlockState state, World world, BlockPos pos,
      PlayerEntity player, Hand hand, BlockHitResult hit) {
    if (!world.isClient) {
      BlockEntity be = world.getBlockEntity(pos);
      if (be != null && be instanceof ToolTableBlockEntity) {
        ContainerProviderRegistry.INSTANCE.openContainer(ID, player, (buf) -> {
          buf.writeBlockPos(pos);
        });
      }
    }
    return ActionResult.SUCCESS;
  }

  @Override
  public void onStateReplaced(BlockState state, World world, BlockPos pos,
      BlockState newState, boolean moved) {
    if (state.getBlock() != newState.getBlock()) {
      BlockEntity blockEntity = world.getBlockEntity(pos);
      if (blockEntity instanceof Inventory) {
        ItemScatterer.spawn(world, pos, (Inventory) blockEntity);
        world.updateComparators(pos, this);
      }

      super.onStateReplaced(state, world, pos, newState, moved);
    }
  }

  @Override
  @Environment(EnvType.CLIENT)
  public void buildTooltip(ItemStack stack, @Nullable BlockView view,
      List<Text> tooltip, TooltipContext options) {
    TooltipUtils.addDescriptionTooltips(tooltip,
        "block.suludom.tool_table.tooltip_1",
        "block.suludom.tool_table.tooltip_2");
  }

}
