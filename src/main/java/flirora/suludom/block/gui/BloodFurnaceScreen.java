package flirora.suludom.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class BloodFurnaceScreen
    extends CottonInventoryScreen<BloodFurnaceController> {

  public BloodFurnaceScreen(BloodFurnaceController container,
      PlayerEntity player) {
    super(container, player);
  }

}
