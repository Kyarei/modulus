package flirora.suludom.block.gui;

import flirora.suludom.SuludomRegistry;
import flirora.suludom.block.AugmentTableBlockEntity;
import flirora.suludom.block.OutputSlot;
import flirora.suludom.material.augment.AugmentMaterial;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

public class AugmentTableController extends SyncedGuiDescription {
  private OutputSlot outputSlot;
  private BlockPos blockPos;
  private WLabel status;

  public AugmentTableController(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(SuludomScreenHandlerTypes.AUGMENT_TABLE, syncId, playerInventory,
        getBlockInventory(context), getBlockPropertyDelegate(context));

    blockPos = context.run((world, position) -> position, BlockPos.ORIGIN);

    outputSlot = ((AugmentTableBlockEntity) blockInventory).getOutputSlot();

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel.add(
        new WLabel(new TranslatableText("block.suludom.augment_table"),
            WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot toolInputSlot = WItemSlot.of(blockInventory, 0, 1, 1);
    rootPanel.add(toolInputSlot, 2, 1);

    WItemSlot augmentInputSlot = WItemSlot.of(blockInventory, 1, 1, 1);
    rootPanel.add(augmentInputSlot, 4, 1);

    WLabel status = new WLabel("");
    status.setColor(0xfc6f6f, 0xfc6f6f);
    status.setHorizontalAlignment(HorizontalAlignment.RIGHT);
    rootPanel.add(status, 7, 2);
    status.setLocation(status.getX(), status.getY() + 6);

    WItemSlot outputSlot = new WItemSlot(this.outputSlot, 0, 1, 1, true) {
      @Override public void tick() {
        AugmentTableBlockEntity bi = (AugmentTableBlockEntity) blockInventory;
        if (bi.isDirty()) {
          bi.recalculateOutput(text -> status.setText(text));
          bi.clearDirty();
        }
      }
    };
    rootPanel.add(outputSlot, 6, 1);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 3);

    rootPanel.validate(this);

    this.status = status;
  }

  @Override
  public ItemStack onSlotClick(int slotNumber, int button,
      SlotActionType action, PlayerEntity player) {
    AugmentTableBlockEntity bi = (AugmentTableBlockEntity) blockInventory;
    bi.recalculateOutput(text -> status.setText(text));
    bi.clearDirty();
    if (action == SlotActionType.QUICK_MOVE && slotNumber == 2) {
      // Nasty hack to nudge onSlotClick to move from the output slot
      this.blockInventory = outputSlot;
    }
    ItemStack extracted = super.onSlotClick(slotNumber, button, action, player);
    this.blockInventory = bi;
    if (action != SlotActionType.CLONE && slotNumber == 2 && extracted != ItemStack.EMPTY) {
      ItemStack augmentStack = blockInventory.getStack(1);
      AugmentMaterial augment = SuludomRegistry.getAugmentMaterialRegistry()
          .get(Registry.ITEM.getId(augmentStack.getItem()));
      augmentStack.decrement(augment.getItemCount());
      blockInventory.getStack(0).decrement(1);
      if (!world.isClient) {
        world.playSound(null, blockPos, SoundEvents.BLOCK_ANVIL_USE,
            SoundCategory.BLOCKS, 1, 1);
      }
    }

    return extracted;
  }
}
