package flirora.suludom.block.gui;

import flirora.suludom.block.BloodFurnaceBlockEntity;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public class BloodFurnaceController extends SyncedGuiDescription {
  private static final Identifier FULL_BLOOD_HEAT =
      new Identifier("suludom", "textures/gui/blood_heat_full.png");
  private static final Identifier FULL_SMELTING_PROGRESS =
      new Identifier("suludom",
          "textures/gui/blood_smelting_progress_full.png");

  private void repositionSprite(WWidget sprite, int width) {
    sprite.setSize(width, 16);
  }

  public BloodFurnaceController(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(SuludomScreenHandlerTypes.BLOOD_FURNACE, syncId, playerInventory,
        getBlockInventory(context), getBlockPropertyDelegate(context));

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel.add(
        new WLabel(new TranslatableText("block.suludom.blood_furnace"),
            WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot inputSlot1 = WItemSlot.of(blockInventory, 0, 1, 1);
    rootPanel.add(inputSlot1, 1, 1);
    inputSlot1.setLocation(inputSlot1.getX() + 7, inputSlot1.getY());

    WItemSlot inputSlot2 = WItemSlot.of(blockInventory, 1, 1, 1);
    rootPanel.add(inputSlot2, 3, 1);
    inputSlot2.setLocation(inputSlot2.getX() - 7, inputSlot2.getY());

    WItemSlot fuelSlot = WItemSlot.of(blockInventory, 2, 1, 1);
    rootPanel.add(fuelSlot, 2, 3);

    WItemSlot outputSlot = WItemSlot.outputOf(blockInventory, 3);
    rootPanel.add(outputSlot, 6, 2);
    outputSlot.setLocation(outputSlot.getX() + 3, outputSlot.getY());

    WItemSlot secondaryOutputSlot = WItemSlot.of(blockInventory, 4);
    rootPanel.add(secondaryOutputSlot, 7, 2);
    secondaryOutputSlot.setLocation(secondaryOutputSlot.getX() + 11,
        secondaryOutputSlot.getY());

    WSprite bloodHeatEmpty = new WSprite(
        new Identifier("suludom", "textures/gui/blood_heat_empty.png"));
    rootPanel.add(bloodHeatEmpty, 2, 2);
    repositionSprite(bloodHeatEmpty, 16);

    WWidget bloodHeatFull = new WWidget() {
      @Override @Environment(EnvType.CLIENT)
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        float heatLeft =
            ((BloodFurnaceBlockEntity) blockInventory).getHeatLeft();
        int yOffset = 16 - (int) (16 * heatLeft);
        ScreenDrawing
            .texturedRect(x, y + yOffset, getWidth(), getHeight() - yOffset,
                FULL_BLOOD_HEAT, 0, yOffset / 16.0f, 1, 1, 0xFF_FFFF);
      }
    };
    rootPanel.add(bloodHeatFull, 2, 2);
    repositionSprite(bloodHeatFull, 16);

    WSprite smeltingProgressEmpty = new WSprite(new Identifier("suludom",
        "textures/gui/blood_smelting_progress_empty.png"));
    rootPanel.add(smeltingProgressEmpty, 4, 2);
    repositionSprite(smeltingProgressEmpty, 32);

    WWidget smeltingProgressFull = new WWidget() {
      @Override @Environment(EnvType.CLIENT)
      public void paint(MatrixStack matrices, int x, int y, int mouseX,
          int mouseY) {
        float progress =
            ((BloodFurnaceBlockEntity) blockInventory).getSmeltingProgress();
        int width = (int) (32 * progress);
        ScreenDrawing
            .texturedRect(x, y, width, getHeight(), FULL_SMELTING_PROGRESS, 0,
                0, width / 32.0f, 1, 0xFF_FFFF);
      }
    };
    rootPanel.add(smeltingProgressFull, 4, 2);
    repositionSprite(smeltingProgressFull, 32);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 4);

    rootPanel.validate(this);
  }

}
