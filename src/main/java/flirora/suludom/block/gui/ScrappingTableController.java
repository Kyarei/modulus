package flirora.suludom.block.gui;

import flirora.suludom.SuludomMod;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.BlockPos;

public class ScrappingTableController extends SyncedGuiDescription {
  private BlockPos blockPos;

  public ScrappingTableController(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(SuludomScreenHandlerTypes.SCRAPPING_TABLE, syncId, playerInventory,
        getBlockInventory(context), getBlockPropertyDelegate(context));

    blockPos = context.run((world, position) -> position, BlockPos.ORIGIN);

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel.add(
        new WLabel(new TranslatableText("block.suludom.scrapping_table"),
            WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot toolInputSlot = WItemSlot.of(blockInventory, 0, 1, 1);
    rootPanel.add(toolInputSlot, 1, 1);

    WItemSlot outputSlot = WItemSlot.of(blockInventory, 1, 5, 3);
    rootPanel.add(outputSlot, 3, 1);

    WButton button = new WButton(new TranslatableText("gui.modulus.scrap"))
        .setOnClick(() -> {
          if (world.isClient) {
            PacketByteBuf passedData = new PacketByteBuf(Unpooled.buffer());
            passedData.writeBlockPos(blockPos);
            ClientSidePacketRegistry.INSTANCE
                .sendToServer(SuludomMod.SCRAP_GEAR_PACKET_ID, passedData);
          }
        });
    rootPanel.add(button, 0, 2);
    button.setSize(48, 20);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 4);

    rootPanel.validate(this);
  }

}
