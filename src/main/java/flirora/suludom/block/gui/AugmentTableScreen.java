package flirora.suludom.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class AugmentTableScreen
    extends CottonInventoryScreen<AugmentTableController> {

  public AugmentTableScreen(AugmentTableController augmentTableController,
      PlayerEntity player) {
    super(augmentTableController, player);
  }

}
