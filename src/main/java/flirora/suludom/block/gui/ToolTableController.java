package flirora.suludom.block.gui;

import flirora.suludom.block.OutputSlot;
import flirora.suludom.block.ToolTableBlockEntity;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.TranslatableText;

public class ToolTableController extends SyncedGuiDescription {
  private OutputSlot outputSlot;

  public ToolTableController(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext context) {
    super(SuludomScreenHandlerTypes.TOOL_TABLE, syncId, playerInventory,
        getBlockInventory(context), getBlockPropertyDelegate(context));

    this.outputSlot = ((ToolTableBlockEntity) blockInventory).getOutputSlot();

    WGridPanel rootPanel = (WGridPanel) getRootPanel();

    rootPanel.add(new WLabel(new TranslatableText("block.suludom.tool_table"),
        WLabel.DEFAULT_TEXT_COLOR), 0, 0);

    WItemSlot inputSlot = WItemSlot.of(blockInventory, 0, 3, 3);
    rootPanel.add(inputSlot, 2, 1);

    WItemSlot outputSlot = new WItemSlot(this.outputSlot, 0, 1, 1, true) {
      @Override public void tick() {
        ToolTableBlockEntity bi = (ToolTableBlockEntity) blockInventory;
        if (bi.isDirty()) {
          bi.recalculateOutput();
          bi.clearDirty();
        }
      }
    };
    rootPanel.add(outputSlot, 6, 2);

    rootPanel.add(this.createPlayerInventoryPanel(), 0, 4);

    rootPanel.validate(this);
  }

  @Override
  public ItemStack onSlotClick(int slotNumber, int button,
      SlotActionType action, PlayerEntity player) {
    ToolTableBlockEntity bi = (ToolTableBlockEntity) blockInventory;
    bi.recalculateOutput();
    bi.clearDirty();
    if (action == SlotActionType.QUICK_MOVE && slotNumber == 9) {
      // Nasty hack to nudge onSlotClick to move from the output slot
      this.blockInventory = outputSlot;
    }
    ItemStack extracted = super.onSlotClick(slotNumber, button, action, player);
    this.blockInventory = bi;
    if (action != SlotActionType.CLONE && slotNumber == 9 && extracted != ItemStack.EMPTY) {
      for (int i = 0; i < 9; ++i) {
        blockInventory.getStack(i).split(1);
      }
    }

    return extracted;
  }
}
