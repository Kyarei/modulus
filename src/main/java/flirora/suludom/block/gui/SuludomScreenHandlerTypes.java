package flirora.suludom.block.gui;

import flirora.suludom.SuludomMod;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;

public class SuludomScreenHandlerTypes {
  public static ScreenHandlerType<AugmentTableController> AUGMENT_TABLE;
  public static ScreenHandlerType<BloodFurnaceController> BLOOD_FURNACE;
  public static ScreenHandlerType<ScrappingTableController> SCRAPPING_TABLE;
  public static ScreenHandlerType<ToolTableController> TOOL_TABLE;

  public static void load() {
    AUGMENT_TABLE = ScreenHandlerRegistry
        .registerSimple(new Identifier(SuludomMod.MOD_ID, "augment_table"),
            (int syncId, PlayerInventory inventory) -> {
              return new AugmentTableController(syncId, inventory,
                  ScreenHandlerContext.EMPTY);

            });
    BLOOD_FURNACE = ScreenHandlerRegistry
        .registerSimple(new Identifier(SuludomMod.MOD_ID, "blood_furnace"),
            (int syncId, PlayerInventory inventory) -> {
              return new BloodFurnaceController(syncId, inventory,
                  ScreenHandlerContext.EMPTY);

            });
    SCRAPPING_TABLE = ScreenHandlerRegistry
        .registerSimple(new Identifier(SuludomMod.MOD_ID, "scrapping_table"),
            (int syncId, PlayerInventory inventory) -> {
              return new ScrappingTableController(syncId, inventory,
                  ScreenHandlerContext.EMPTY);

            });
    TOOL_TABLE = ScreenHandlerRegistry
        .registerSimple(new Identifier(SuludomMod.MOD_ID, "tool_table"),
            (int syncId, PlayerInventory inventory) -> {
              return new ToolTableController(syncId, inventory,
                  ScreenHandlerContext.EMPTY);

            });
  }
}
