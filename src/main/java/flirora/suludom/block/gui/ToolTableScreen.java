package flirora.suludom.block.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class ToolTableScreen
    extends CottonInventoryScreen<ToolTableController> {

  public ToolTableScreen(ToolTableController container, PlayerEntity player) {
    super(container, player);
  }

}
