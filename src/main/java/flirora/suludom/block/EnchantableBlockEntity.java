package flirora.suludom.block;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.enchantment.Enchantment;

public interface EnchantableBlockEntity {
  Object2IntMap<Enchantment> getEnchantments();

  void setEnchantments(Object2IntMap<Enchantment> enchantments);

  default int getLevelOf(Enchantment enchantment) {
    Object2IntMap<Enchantment> enchantments = getEnchantments();
    if (enchantments == null)
      return 0;
    return enchantments.getOrDefault(enchantment, 0);
  }
}
