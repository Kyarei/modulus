package flirora.suludom;

import flirora.suludom.material.MaterialType;
import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.augment.AugmentMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;
import net.minecraft.util.registry.Registry;

public class SuludomRegistry {
  public static Registry<ToolMaterial> getToolMaterialRegistry() {
    return SuludomMod.getMaterialManager()
        .getRegistryFor(MaterialType.TOOL_MATERIAL);
  }

  public static Registry<RodMaterial> getRodMaterialRegistry() {
    return SuludomMod.getMaterialManager()
        .getRegistryFor(MaterialType.ROD_MATERIAL);
  }

  public static Registry<ArmorMaterial> getArmorMaterialRegistry() {
    return SuludomMod.getMaterialManager()
        .getRegistryFor(MaterialType.ARMOR_MATERIAL);
  }

  public static Registry<AugmentMaterial> getAugmentMaterialRegistry() {
    return SuludomMod.getMaterialManager()
        .getRegistryFor(MaterialType.AUGMENT_MATERIAL);
  }
}
