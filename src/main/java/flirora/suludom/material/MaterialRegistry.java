package flirora.suludom.material;

import com.mojang.serialization.Lifecycle;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.tag.ItemTags;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.MutableRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.util.registry.SimpleRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class MaterialRegistry<M extends Material> extends MutableRegistry<M> {
  private MutableRegistry<M> materialsByItem;
  private Map<Tag<Item>, M> materialsByTag;
  private Map<Item, M> cache;

  public MaterialRegistry(RegistryKey<Registry<M>> registryKey,
      Lifecycle lifecycle) {
    super(registryKey, lifecycle);
    this.materialsByItem = new SimpleRegistry<>(registryKey, lifecycle);
    this.materialsByTag = new HashMap<>();
    this.cache = new HashMap<>();
  }

  @Override public Identifier getId(M entry) {
    return materialsByItem.getId(entry);
  }

  @Override public Optional<RegistryKey<M>> getKey(M value) {
    return Optional.empty();
  }

  @Override public int getRawId(M entry) {
    return materialsByItem.getRawId(entry);
  }

  @Override public M get(RegistryKey<M> key) {
    return null;
  }

  @Override public M get(Identifier id) {
    return materialsByItem.get(id);
  }

  @Override public Optional<M> getOrEmpty(Identifier id) {
    return materialsByItem.getOrEmpty(id);
  }

  @Override public Set<Identifier> getIds() {
    return materialsByItem.getIds();
  }

  @Environment(EnvType.CLIENT) @Override
  public boolean containsId(Identifier id) {
    return materialsByItem.containsId(id);
  }

  @Override public boolean isLoaded(RegistryKey<M> registryKey) {
    return false;
  }

  @Override public boolean containsId(int id) {
    return false;
  }

  @Override public M get(int index) {
    return materialsByItem.get(index);
  }

  @NotNull @Override public Iterator<M> iterator() {
    return materialsByItem.iterator();
  }

  public M getByTag(Tag<Item> tag) {
    return materialsByTag.get(tag);
  }

  private M computeFromItem(Item item) {
    Identifier id = Registry.ITEM.getId(item);
    M m1 = materialsByItem.get(id);
    if (m1 != null)
      return m1;
    for (Map.Entry<Identifier, Tag<Item>> entry : ItemTags.getContainer()
        .getEntries().entrySet()) {
      M m2 = materialsByTag.get(entry.getValue());
      if (m2 != null)
        return m2;
    }
    return null;
  }

  public M getFromItem(Item item) {
    if (cache.containsKey(item))
      return cache.get(item);
    M material = computeFromItem(item);
    cache.put(item, material);
    return material;
  }

  @Override public <V extends M> V set(int rawId, RegistryKey<M> key, V entry) {
    cache.clear();
    M oldMaterial = materialsByItem.get(key);
    if (oldMaterial != null) {
      Tag<Item> tag = oldMaterial.getTag();
      if (tag != null)
        materialsByTag.remove(tag);
    }
    materialsByTag.put(entry.getTag(), entry);
    return materialsByItem.set(rawId, key, entry);
  }

  @Override public <V extends M> V add(RegistryKey<M> key, V entry) {
    cache.clear();
    M oldMaterial = materialsByItem.get(key);
    if (oldMaterial != null) {
      Tag<Item> tag = oldMaterial.getTag();
      if (tag != null)
        materialsByTag.remove(tag);
    }
    materialsByTag.put(entry.getTag(), entry);
    return materialsByItem.add(key, entry);
  }

  @Override public void markLoaded(RegistryKey<M> registryKey) {
    materialsByItem.markLoaded(registryKey);
  }
}
