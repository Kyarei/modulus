package flirora.suludom.material;

import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;

public enum MaterialCategory {
  WOOD("wood"), // wooden planks
  STONE("stone"), // stony blocks
  METAL("metal"), // metals
  GEM("gem"), // gemstones such as diamonds
  CLOTH("cloth"), // cloth (for armor)
  FOOD("food"), // food
  ;

  public static MaterialCategory fromString(String s) {
    switch (s) {
    case "wood":
      return WOOD;
    case "stone":
      return STONE;
    case "metal":
      return METAL;
    case "gem":
      return GEM;
    case "cloth":
      return CLOTH;
    case "food":
      return FOOD;
    }
    return null;
  }

  private String id;

  private MaterialCategory(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public Text getText() {
    return new TranslatableText("tooltip.suludom.category." + id);
  }
}
