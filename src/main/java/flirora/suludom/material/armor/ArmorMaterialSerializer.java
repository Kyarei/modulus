package flirora.suludom.material.armor;

import blue.endless.jankson.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import flirora.suludom.material.MaterialCategory;
import flirora.suludom.material.MaterialSerializer;
import flirora.suludom.material.SerializerUtils;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class ArmorMaterialSerializer
    implements MaterialSerializer<ArmorMaterial> {

  private static class JsonFormat {
    public int durabilityFactor;
    public float armor;
    public float armorToughness;
    public float knockbackResistance;
    public int enchantability;
    public int armorColor;
    public String materialCategory;
    public JsonObject enchantmentAffinities;
    @Nullable public String tag = null;
  }

  @Override public ArmorMaterial read(Identifier id, JsonObject json) {
    JsonFormat materialJson = new Gson().fromJson(json, JsonFormat.class);
    MaterialCategory cat =
        MaterialCategory.fromString(materialJson.materialCategory);
    if (cat == null)
      throw new JsonSyntaxException(
          "Unknown category " + materialJson.materialCategory);
    Tag.Identified<Item> tag = SerializerUtils.tagFromString(materialJson.tag);
    return new ArmorMaterial(id, materialJson.durabilityFactor,
        materialJson.armor, materialJson.armorToughness,
        materialJson.knockbackResistance, materialJson.enchantability,
        materialJson.armorColor, cat,
        EnchantmentAffinities.fromJson(materialJson.enchantmentAffinities),
        tag);
  }

  @Override public ArmorMaterial read(Identifier id, PacketByteBuf data) {
    return new ArmorMaterial(id, data.readInt(), data.readFloat(),
        data.readFloat(), data.readFloat(), data.readInt(), data.readInt(),
        MaterialCategory.values()[data.readByte()],
        EnchantmentAffinities.fromData(data),
        SerializerUtils.readItemTag(data));
  }

  @Override public void write(ArmorMaterial material, PacketByteBuf data) {
    data.writeInt(material.getDurabilityFactor());
    data.writeFloat(material.getArmor());
    data.writeFloat(material.getArmorToughness());
    data.writeFloat(material.getKnockbackResistance());
    data.writeInt(material.getEnchantability());
    data.writeInt(material.getArmorColor());
    data.writeByte((byte) material.getMaterialCategory().ordinal());
    material.getEnchantmentAffinities().write(data);
    SerializerUtils.writeItemTag(data, material.getTag());
  }

}
