package flirora.suludom.material.armor;

import flirora.suludom.material.HasMaterialCategory;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialCategory;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

public class ArmorMaterial implements Material, HasMaterialCategory {
  private final Identifier id;
  private final int durabilityFactor; // multiplied by {13, 15, 16, 11}
  private final float armor; // of full set
  private final float armorToughness; // of each piece
  private final float knockbackResistance;
  private final int enchantability;
  private final int armorColor;
  private final MaterialCategory materialCategory;
  private final EnchantmentAffinities enchantmentAffinities;
  private final Tag.Identified<Item> tag;

  public ArmorMaterial(Identifier id, int durabilityFactor, float armor,
      float armorToughness, float knockbackResistance, int enchantability,
      int armorColor, MaterialCategory materialCategory,
      EnchantmentAffinities enchantmentAffinities, Tag.Identified<Item> tag) {
    super();
    this.id = id;
    this.durabilityFactor = durabilityFactor;
    this.armor = armor;
    this.armorToughness = armorToughness;
    this.knockbackResistance = knockbackResistance;
    this.enchantability = enchantability;
    this.armorColor = armorColor;
    this.materialCategory = materialCategory;
    this.enchantmentAffinities = enchantmentAffinities;
    this.tag = tag;
  }

  @Override public Identifier getId() {
    return id;
  }

  public int getDurabilityFactor() {
    return durabilityFactor;
  }

  public float getArmor() {
    return armor;
  }

  public float getArmorToughness() {
    return armorToughness;
  }

  public int getEnchantability() {
    return enchantability;
  }

  public int getArmorColor() {
    return armorColor;
  }

  @Override
  public MaterialCategory getMaterialCategory() {
    return materialCategory;
  }

  @Override
  public String getType() {
    return "armor_material";
  }

  @Override
  public MaterialType<?> getTypeObject() {
    return MaterialType.ARMOR_MATERIAL;
  }

  @Override
  public EnchantmentAffinities getEnchantmentAffinities() {
    return enchantmentAffinities;
  }

  public float getKnockbackResistance() {
    return knockbackResistance;
  }

  @Override
  public List<Text> getReiInfo() {
    List<Text> result = new ArrayList<>();
    result.add(new TranslatableText("tooltip.suludom.durability_factor",
        durabilityFactor));
    result
        .add(new TranslatableText("tooltip.suludom.armor.full_set_rei", armor));
    result.add(
        new TranslatableText("tooltip.suludom.armorToughness", armorToughness));
    result.add(new TranslatableText("tooltip.suludom.knockbackResistance",
        knockbackResistance));
    result.add(
        new TranslatableText("tooltip.suludom.enchantability", enchantability));
    result.add(materialCategory.getText());
    return result;
  }

  @Override public Tag.Identified<Item> getTag() {
    return tag;
  }

}
