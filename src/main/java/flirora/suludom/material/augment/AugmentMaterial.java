package flirora.suludom.material.augment;

import flirora.suludom.item.tool.TooltipUtils;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

public class AugmentMaterial implements Material {
  private final Identifier id;
  private final int durabilityBonus;
  private final float miningSpeedBonus;
  private final float meleeDamageBonus;
  private final float rangedDamageBonus;
  private final float attackSpeedBonus;
  private final int enchantabilityBonus;
  private final float armorBonus;
  private final float armorToughnessBonus;
  private final float knockbackResistanceBonus;
  private final EnchantmentAffinities enchantmentAffinities;
  private final int itemCount;
  private final int bonusAugments;
  private final Tag.Identified<Item> tag;

  public AugmentMaterial(Identifier id, int durabilityBonus,
      float miningSpeedBonus, float meleeDamageBonus, float rangedDamageBonus,
      float attackSpeedBonus, int enchantabilityBonus, float armorBonus,
      float armorToughnessBonus, float knockbackResistanceBonus,
      EnchantmentAffinities enchantmentAffinities, int itemCount,
      int bonusAugments, Tag.Identified<Item> tag) {
    super();
    this.id = id;
    this.durabilityBonus = durabilityBonus;
    this.miningSpeedBonus = miningSpeedBonus;
    this.meleeDamageBonus = meleeDamageBonus;
    this.rangedDamageBonus = rangedDamageBonus;
    this.attackSpeedBonus = attackSpeedBonus;
    this.enchantabilityBonus = enchantabilityBonus;
    this.armorBonus = armorBonus;
    this.armorToughnessBonus = armorToughnessBonus;
    this.knockbackResistanceBonus = knockbackResistanceBonus;
    this.enchantmentAffinities = enchantmentAffinities;
    this.itemCount = itemCount;
    this.bonusAugments = bonusAugments;
    this.tag = tag;
  }

  @Override
  public Identifier getId() {
    return id;
  }

  @Override
  public String getType() {
    return "augment_material";
  }

  @Override
  public EnchantmentAffinities getEnchantmentAffinities() {
    return enchantmentAffinities;
  }

  @Override
  public MaterialType<?> getTypeObject() {
    return MaterialType.AUGMENT_MATERIAL;
  }

  public int getDurabilityBonus() {
    return durabilityBonus;
  }

  public float getMiningSpeedBonus() {
    return miningSpeedBonus;
  }

  public float getMeleeDamageBonus() {
    return meleeDamageBonus;
  }

  public float getRangedDamageBonus() {
    return rangedDamageBonus;
  }

  public float getAttackSpeedBonus() {
    return attackSpeedBonus;
  }

  public int getEnchantabilityBonus() {
    return enchantabilityBonus;
  }

  public float getArmorBonus() {
    return armorBonus;
  }

  public float getArmorToughnessBonus() {
    return armorToughnessBonus;
  }

  public int getItemCount() {
    return itemCount;
  }

  public float getKnockbackResistanceBonus() {
    return knockbackResistanceBonus;
  }

  public int getBonusAugments() {
    return bonusAugments;
  }

  @Override
  public List<Text> getReiInfo() {
    List<Text> result = new ArrayList<>();
    result.add(TooltipUtils.bonusText("tooltip.suludom.durability_info",
        durabilityBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.mining_speed",
        miningSpeedBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.melee_damage",
        meleeDamageBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.ranged_damage",
        rangedDamageBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.attack_speed",
        attackSpeedBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.enchantability",
        enchantabilityBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.armor.full_set_rei",
        armorBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.armorToughness",
        armorToughnessBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.knockbackResistance",
        knockbackResistanceBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.bonus_augments",
        bonusAugments));
    result.add(new TranslatableText("tooltip.suludom.count_needed", itemCount));
    return result;
  }

  @Override public Tag.Identified<Item> getTag() {
    return tag;
  }

}
