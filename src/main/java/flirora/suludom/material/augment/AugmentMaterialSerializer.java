package flirora.suludom.material.augment;

import blue.endless.jankson.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import flirora.suludom.material.MaterialSerializer;
import flirora.suludom.material.SerializerUtils;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class AugmentMaterialSerializer
    implements MaterialSerializer<AugmentMaterial> {

  private static class JsonFormat {
    public int durabilityBonus;
    public float miningSpeedBonus;
    public float meleeDamageBonus;
    public float rangedDamageBonus;
    public float attackSpeedBonus;
    public int enchantabilityBonus;
    public float armorBonus;
    public float armorToughnessBonus;
    public float knockbackResistanceBonus;
    public JsonObject enchantmentAffinities;
    public int itemCount;
    public int bonusAugments = 0;
    @Nullable public String tag = null;
  }

  @Override public AugmentMaterial read(Identifier id, JsonObject json) {
    JsonFormat materialJson = new Gson().fromJson(json, JsonFormat.class);
    Tag.Identified<Item> tag = SerializerUtils.tagFromString(materialJson.tag);
    return new AugmentMaterial(id, materialJson.durabilityBonus,
        materialJson.miningSpeedBonus, materialJson.meleeDamageBonus,
        materialJson.rangedDamageBonus, materialJson.attackSpeedBonus,
        materialJson.enchantabilityBonus, materialJson.armorBonus,
        materialJson.armorToughnessBonus, materialJson.knockbackResistanceBonus,
        EnchantmentAffinities.fromJson(materialJson.enchantmentAffinities),
        materialJson.itemCount, materialJson.bonusAugments, tag);
  }

  @Override public AugmentMaterial read(Identifier id, PacketByteBuf data) {
    return new AugmentMaterial(id, data.readInt(), data.readFloat(),
        data.readFloat(), data.readFloat(), data.readFloat(), data.readInt(),
        data.readFloat(), data.readFloat(), data.readFloat(),
        EnchantmentAffinities.fromData(data), data.readInt(), data.readInt(),
        SerializerUtils.readItemTag(data));
  }

  @Override public void write(AugmentMaterial material, PacketByteBuf data) {
    data.writeInt(material.getDurabilityBonus());
    data.writeFloat(material.getMiningSpeedBonus());
    data.writeFloat(material.getMeleeDamageBonus());
    data.writeFloat(material.getRangedDamageBonus());
    data.writeFloat(material.getAttackSpeedBonus());
    data.writeInt(material.getEnchantabilityBonus());
    data.writeFloat(material.getArmorBonus());
    data.writeFloat(material.getArmorToughnessBonus());
    data.writeFloat(material.getKnockbackResistanceBonus());
    material.getEnchantmentAffinities().write(data);
    data.writeInt(material.getItemCount());
    data.writeInt(material.getBonusAugments());
    SerializerUtils.writeItemTag(data, material.getTag());
  }

}
