package flirora.suludom.material.enchantment;

import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import blue.endless.jankson.annotation.Nullable;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * A list of enchantment affinities. Materials have affinities for certain
 * enchantments that are passed to items made from them. During enchantment,
 * these affinities are added to the weights of the applicable enchantments,
 * making them more or less likely to be chosen.
 * 
 * All enchantment weights in EnchantmentAffinities are scaled by 16 relative to
 * the vanilla values for additional granularity. A weight of 0 has no effect
 * except to unlock treasure enchantments in the enchanting table.
 * 
 * @author flirora
 */
public class EnchantmentAffinities {
  private final Object2IntOpenHashMap<Enchantment> affinities;

  public static final EnchantmentAffinities EMPTY = new EnchantmentAffinities();

  public EnchantmentAffinities() {
    affinities = new Object2IntOpenHashMap<>();
  }

  public EnchantmentAffinities(Object2IntOpenHashMap<Enchantment> affinities) {
    this.affinities = affinities;
  }

  public Object2IntMap<Enchantment> getAffinities() {
    return affinities;
  }

  public EnchantmentAffinities merge(EnchantmentAffinities other) {
    for (Object2IntMap.Entry<Enchantment> entry : other.affinities
        .object2IntEntrySet()) {
      affinities.addTo(entry.getKey(), entry.getIntValue());
    }
    return this;
  }

  public EnchantmentAffinities divide(int divisor) {
    affinities
        .replaceAll((enchantment, weight) -> (weight + divisor / 2) / divisor);
    return this;
  }

  public EnchantmentAffinities scale(float scale) {
    affinities.replaceAll((enchantment, weight) -> Math.round(scale * weight));
    return this;
  }

  public static EnchantmentAffinities fromJson(@Nullable JsonObject json) {
    if (json == null)
      return EMPTY;
    Object2IntOpenHashMap<Enchantment> affinities =
        new Object2IntOpenHashMap<>();
    for (Entry<String, JsonElement> entry : json.entrySet()) {
      Identifier id = Identifier.tryParse(entry.getKey());
      Enchantment enchantment = Registry.ENCHANTMENT.get(id);
      if (enchantment == null)
        throw new JsonSyntaxException("Enchantment " + id + " doesn't exist!");
      int weight = entry.getValue().getAsInt();
      affinities.put(enchantment, weight);
    }
    return new EnchantmentAffinities(affinities);
  }

  public static EnchantmentAffinities fromTag(@Nullable ListTag tag) {
    if (tag == null)
      return EMPTY;
    Object2IntOpenHashMap<Enchantment> affinities =
        new Object2IntOpenHashMap<>();
    for (int i = 0; i < tag.size(); ++i) {
      CompoundTag entry = tag.getCompound(i);
      Identifier id = Identifier.tryParse(entry.getString("Enchantment"));
      Enchantment enchantment = Registry.ENCHANTMENT.get(id);
      if (enchantment == null)
        throw new JsonSyntaxException("Enchantment " + id + " doesn't exist!");
      int weight = entry.getInt("Weight");
      affinities.put(enchantment, weight);
    }
    return new EnchantmentAffinities(affinities);
  }

  public ListTag toTag() {
    ListTag tag = new ListTag();
    for (Object2IntMap.Entry<Enchantment> entry : affinities
        .object2IntEntrySet()) {
      CompoundTag entryTag = new CompoundTag();
      entryTag.putString("Enchantment",
          Registry.ENCHANTMENT.getId(entry.getKey()).toString());
      entryTag.putInt("Weight", entry.getIntValue());
      tag.add(entryTag);
    }
    return tag;
  }

  public static EnchantmentAffinities fromData(PacketByteBuf data) {
    int count = data.readInt();
    Object2IntOpenHashMap<Enchantment> affinities =
        new Object2IntOpenHashMap<>();
    for (int i = 0; i < count; ++i) {
      affinities.put(Registry.ENCHANTMENT.get(data.readIdentifier()),
          data.readInt());
    }
    return new EnchantmentAffinities(affinities);
  }

  public void write(PacketByteBuf data) {
    data.writeInt(affinities.size());
    for (Object2IntMap.Entry<Enchantment> entry : affinities
        .object2IntEntrySet()) {
      data.writeIdentifier(Registry.ENCHANTMENT.getId(entry.getKey()));
      data.writeInt(entry.getIntValue());
    }
  }

  @Override
  public String toString() {
    return "EnchantmentAffinities [affinities=" + affinities + "]";
  }
}
