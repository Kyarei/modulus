package flirora.suludom.material.enchantment;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.util.collection.WeightedPicker;

public class InfoEnchantmentExtended extends WeightedPicker.Entry {
  public final Enchantment enchantment;
  public final int level;

  public InfoEnchantmentExtended(Enchantment enchantment, int level,
      int affinity) {
    super(Math.max(1, enchantment.getRarity().getWeight() * 16 + affinity));
    this.enchantment = enchantment;
    this.level = level;
  }

  public InfoEnchantmentExtended(EnchantmentLevelEntry info, int affinity) {
    this(info.enchantment, info.level, affinity);
  }

  public EnchantmentLevelEntry toInfo() {
    return new EnchantmentLevelEntry(enchantment, level);
  }

  // This is here just so debugging code can print
  public int getWeight() {
    return weight;
  }

}
