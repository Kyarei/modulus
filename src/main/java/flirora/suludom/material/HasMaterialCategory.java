package flirora.suludom.material;

public interface HasMaterialCategory {
  MaterialCategory getMaterialCategory();
}
