package flirora.suludom.material;

import blue.endless.jankson.annotation.Nullable;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.tag.ItemTags;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SerializerUtils {
  private static final Logger LOGGER = LogManager.getLogger();

  @Nullable
  public static Tag.Identified<Item> tagFromId(@Nullable Identifier id) {
    if (id == null)
      return null;
    Tag<Item> tag = ItemTags.getContainer().get(id);
    if (tag instanceof Tag.Identified)
      return (Tag.Identified<Item>) tag;
    LOGGER.error("Tag '" + id + "' is not a Tag.Identified");
    return null;
  }

  @Nullable
  public static Tag.Identified<Item> tagFromString(@Nullable String id) {
    return id != null ? tagFromId(Identifier.tryParse(id)) : null;
  }

  @Nullable public static Tag.Identified<Item> readItemTag(PacketByteBuf data) {
    boolean present = data.readBoolean();
    if (!present)
      return null;
    return tagFromId(data.readIdentifier());
  }

  public static void writeItemTag(PacketByteBuf data,
      @Nullable Tag.Identified<Item> tag) {
    if (tag == null)
      data.writeBoolean(false);
    else {
      data.writeBoolean(true);
      data.writeIdentifier(tag.getId());
    }
  }
}
