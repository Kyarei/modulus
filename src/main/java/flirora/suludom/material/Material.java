package flirora.suludom.material;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.util.List;

public interface Material {
  Identifier getId();

  String getType();

  EnchantmentAffinities getEnchantmentAffinities();

  MaterialType<?> getTypeObject();

  List<Text> getReiInfo();

  @Nullable Tag<Item> getTag();
}
