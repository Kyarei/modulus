package flirora.suludom.material;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.serialization.Lifecycle;
import flirora.suludom.SuludomMod;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.MutableRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class MaterialManager extends JsonDataLoader
    implements IdentifiableResourceReloadListener {
  private static final Gson GSON =
      (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
  private static final Logger LOGGER = LogManager.getLogger();
  private static final Identifier ID = new Identifier("suludom", "materials");

  private Map<MaterialType<?>, MaterialRegistry<?>> materials =
      ImmutableMap.of();
  private Map<MaterialType<?>, MaterialSerializer<?>> serializers =
      new HashMap<>();
  private Map<String, MaterialType<?>> materialTypes = new HashMap<>();
  private Map<Byte, MaterialType<?>> materialTypesByOrdinal = new HashMap<>();

  private RegistryKey<Registry<Material>> MATERIAL_KEY =
      RegistryKey.ofRegistry(new Identifier(SuludomMod.MOD_ID, "material"));

  public MaterialManager() {
    super(GSON, "materials");
  }

  public void registerMaterialType(MaterialType<?> type,
      MaterialSerializer<?> serializer) {
    serializers.put(type, serializer);
    materialTypes.put(type.toString(), type);
    materialTypesByOrdinal.put(type.getOrdinal(), type);
  }

  @NotNull private MaterialRegistry<Material> createMaterialRegistry() {
    return new MaterialRegistry<>(MATERIAL_KEY, Lifecycle.experimental());
  }

  @SuppressWarnings("unchecked")
  private <M extends Material> void addMaterialByType(
      Map<MaterialType<?>, MaterialRegistry<?>> materials,
      MaterialType<M> materialType, Identifier id, String materialPath,
      JsonObject json) {
    MaterialSerializer<M> materialSerializer =
        (MaterialSerializer<M>) serializers.get(materialType);
    Identifier itemId = new Identifier(id.getNamespace(), materialPath);
    M material = materialSerializer.read(itemId, json);
    MaterialRegistry<M> registry = (MaterialRegistry<M>) materials
        .computeIfAbsent(materialType, (k) -> createMaterialRegistry());
    Registry.register(registry, itemId, material);
  }

  @Override protected void apply(Map<Identifier, JsonElement> loader,
      ResourceManager manager, Profiler profiler) {
    Map<MaterialType<?>, MaterialRegistry<?>> materials = new HashMap<>();
    int count = 0;
    for (Entry<Identifier, JsonElement> materialTypeEntry : loader.entrySet()) {
      Identifier id = materialTypeEntry.getKey();
      try {
        String path = id.getPath();
        if (!path.contains("/")) {
          LOGGER.warn(
              "JSON file for " + id + " does not reside in a subdirectory; ignoring");
          continue;
        }
        String materialTypeName = path.substring(0, path.indexOf('/'));
        String materialPath = path.substring(path.indexOf('/') + 1);
        MaterialType<?> materialType = materialTypes.get(materialTypeName);
        if (materialType == null) {
          LOGGER
              .warn("Material type '" + materialTypeName + "' does not exist");
          continue;
        }
        addMaterialByType(materials, materialType, id, materialPath,
            JsonHelper.asObject(materialTypeEntry.getValue(), "top element"));
        ++count;
      } catch (Exception e) {
        LOGGER.error("Failed to load material: " + id, e);
      }
    }
    for (MaterialType<?> materialType : serializers.keySet()) {
      materials.computeIfAbsent(materialType, (k) -> createMaterialRegistry());
    }
    LOGGER.info("Loaded " + count + " materials");
    this.materials = ImmutableMap.copyOf(materials);
  }

  @SuppressWarnings("unchecked")
  private <M extends Material> void addMaterialByType(
      Map<MaterialType<?>, MaterialRegistry<?>> materials,
      MaterialType<M> materialType, Identifier itemId, PacketByteBuf data) {
    MaterialSerializer<M> materialSerializer =
        (MaterialSerializer<M>) serializers.get(materialType);
    M material = materialSerializer.read(itemId, data);
    MaterialRegistry<M> registry = (MaterialRegistry<M>) materials
        .computeIfAbsent(materialType, (k) -> createMaterialRegistry());
    Registry.register(registry, itemId, material);
  }

  public void loadDataFromNetwork(PacketContext packetContext,
      PacketByteBuf data) {
    Map<MaterialType<?>, MaterialRegistry<?>> materials = new HashMap<>();
    int count = data.readInt();
    for (int i = 0; i < count; ++i) {
      String materialTypeName = data.readString(32767);
      MaterialType<?> materialType = materialTypes.get(materialTypeName);
      if (materialType == null) {
        LOGGER.fatal("Material type '" + materialType + "' does not exist");
        return;
      }
      Identifier itemId = data.readIdentifier();
      addMaterialByType(materials, materialType, itemId, data);
    }
    for (MaterialType<?> materialType : serializers.keySet()) {
      materials.computeIfAbsent(materialType, (k) -> createMaterialRegistry());
    }
    LOGGER.info("Loaded " + count + " materials");
    packetContext.getTaskQueue().execute(() -> {
      this.materials = ImmutableMap.copyOf(materials);
    });
  }

  public PacketByteBuf getDataToSend() {
    PacketByteBuf data = new PacketByteBuf(Unpooled.buffer());
    int totalCount = 0;
    for (Entry<MaterialType<?>, MaterialRegistry<?>> entry : materials
        .entrySet()) {
      totalCount += entry.getValue().getIds().size();
    }
    data.writeInt(totalCount);
    for (Entry<MaterialType<?>, MaterialRegistry<?>> mtEntry : materials
        .entrySet()) {
      for (Identifier mEntry : mtEntry.getValue().getIds()) {
        data.writeString(mtEntry.getKey().toString());
        data.writeIdentifier(mEntry);
        MaterialSerializer<?> serializer = serializers.get(mtEntry.getKey());
        if (serializer == null) {
          LOGGER
              .fatal("Serializer for " + mtEntry.getKey() + " does not exist");
        } else {
          serializer.writeOrThrow(mtEntry.getValue().get(mEntry), data);
        }
      }
    }
    return data;
  }

  public void sendDataTo(Stream<PlayerEntity> players) {
    PacketByteBuf data = getDataToSend();
    players.forEach((player) -> {
      ServerSidePacketRegistry.INSTANCE.sendToPlayer(player,
          SuludomMod.MATERIAL_DATA_PACKET_ID, data);
    });
  }

  public void sendDataTo(PlayerEntity player) {
    List<PlayerEntity> players = new ArrayList<>();
    players.add(player);
    sendDataTo(players.stream());
  }

  @SuppressWarnings("unchecked")
  public <M extends Material> MaterialRegistry<M> getRegistryFor(
      MaterialType<M> mtype) {
    return (MaterialRegistry<M>) materials.get(mtype);
  }

  @Override
  public Identifier getFabricId() {
    return ID;
  }

  public MaterialType<?> getByOrdinal(byte b) {
    return materialTypesByOrdinal.get(b);
  }

  public MaterialType<?> getByName(String typeName) {
    return materialTypes.get(typeName);
  }

  public void forEachMaterialType(
      BiConsumer<MaterialType<?>, MutableRegistry<? extends Material>> callback) {
    materials.forEach(callback);
  }

}
