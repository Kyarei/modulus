package flirora.suludom.material;

import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.augment.AugmentMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;

public interface MaterialType<M extends Material> {
  public static <T extends Material> MaterialType<T> createFromString(
      String name, byte ordinal) {
    return new MaterialType<T>() {
      public String toString() {
        return name;
      }

      public byte getOrdinal() {
        return ordinal;
      }
    };
  }

  byte getOrdinal();

  public static MaterialType<ToolMaterial> TOOL_MATERIAL =
      createFromString("tool_material", (byte) 0);

  public static MaterialType<RodMaterial> ROD_MATERIAL =
      createFromString("rod_material", (byte) 1);

  public static MaterialType<ArmorMaterial> ARMOR_MATERIAL =
      createFromString("armor_material", (byte) 2);

  public static MaterialType<AugmentMaterial> AUGMENT_MATERIAL =
      createFromString("augment_material", (byte) 3);

//  public static MaterialType<BowMaterial> BOW_MATERIAL = createFromString(
//      "bow_material", (byte) 3);
//
//  public static MaterialType<BowStringMaterial> BOW_STRING_MATERIAL = createFromString(
//      "bow_string_material", (byte) 4);

}
