package flirora.suludom.material;

import com.google.gson.JsonObject;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

public interface MaterialSerializer<M extends Material> {
  M read(Identifier id, JsonObject json);

  M read(Identifier id, PacketByteBuf data);

  void write(M material, PacketByteBuf data);

  @SuppressWarnings("unchecked")
  default void writeOrThrow(Material material, PacketByteBuf data) {
    write((M) material, data);
  }
}
