package flirora.suludom.material;

import java.util.List;

import flirora.suludom.item.tool.GenericArmor;
import flirora.suludom.item.tool.GenericTool;
import flirora.suludom.item.tool.TooltipUtils;
import flirora.suludom.material.augment.AugmentMaterial;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import flirora.suludom.material.rod.RodMaterial;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class CompositeMaterial {
  private final int miningLevel;
  // Note: this is the total durability for both tools and armor.
  // For armor, it used to be the durability *multiplier*, but that was
  // changed to better support augments.
  private final int durability;
  private final float miningSpeed;
  private final float meleeDamage;
  private final float rangedDamage;
  // Note: this is the protection from a full set of armor.
  private final float armor;
  // Note: this is the protection from each piece of gear.
  private final float armorToughness;
  private final float knockbackResistance;
  private final int enchantability;
  private final float attackSpeedBonus;
  private final float synergy;
  private final EnchantmentAffinities enchantmentAffinities;
  private final int augmentCapacity;

  public CompositeMaterial(int miningLevel, int durability, float miningSpeed,
      float meleeDamage, float rangedDamage, float armor, float armorToughness,
      float knockbackResistance, int enchantability, float attackSpeedBonus,
      float synergy, EnchantmentAffinities enchantmentAffinities,
      int augmentCapacity) {
    super();
    this.miningLevel = miningLevel;
    this.durability = durability;
    this.miningSpeed = miningSpeed;
    this.meleeDamage = meleeDamage;
    this.rangedDamage = rangedDamage;
    this.armor = armor;
    this.armorToughness = armorToughness;
    this.knockbackResistance = knockbackResistance;
    this.enchantability = enchantability;
    this.attackSpeedBonus = attackSpeedBonus;
    this.synergy = synergy;
    this.enchantmentAffinities = enchantmentAffinities;
    this.augmentCapacity = augmentCapacity;
  }

  public CompositeMaterial() {
    this(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1.0f, new EnchantmentAffinities(), 1);
  }

  public float getAttackDamage() {
    return meleeDamage;
  }

  public float getRangedDamage() {
    return rangedDamage;
  }

  public int getDurability() {
    return durability;
  }

  public float getArmor() {
    return armor;
  }

  public float getArmorToughness() {
    return armorToughness;
  }

  public int getEnchantability() {
    return enchantability;
  }

  public int getMiningLevel() {
    return miningLevel;
  }

  public float getMiningSpeed() {
    return miningSpeed;
  }

  private static final int DURABILITY_PER_SLOT[] = { 13, 15, 16, 11 };
  // scaled to diamond armor
  private static final int PROTECTION_PER_SLOT[] = { 3, 6, 8, 3 };

  public int getProtectionAmount(EquipmentSlot slot) {
    return Math.round(PROTECTION_PER_SLOT[slot.getEntitySlotId()] * armor / 20);
  }

  public float getProtectionAmountFloat(EquipmentSlot slot) {
    return PROTECTION_PER_SLOT[slot.getEntitySlotId()] * armor / 20;
  }

  public SoundEvent getEquipSound() {
    // Right now, there's no way to get the material the composite is made of.
    return SoundEvents.ITEM_ARMOR_EQUIP_GENERIC;
//    switch (materialCategory) {
//    case CLOTH:
//      return SoundEvents.ITEM_ARMOR_EQUIP_LEATHER;
//    case METAL:
//      return SoundEvents.ITEM_ARMOR_EQUIP_IRON;
//    case GEM:
//      return SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND;
//    default:
//      // use a sound that's obviously not putting on armor
//      return SoundEvents.ENTITY_ENDER_DRAGON_DEATH;
//    }
  }

  public String getName() {
    return "composite";
  }

  public float getToughness() {
    return getArmorToughness();
  }

  public float getKnockbackResistance() {
    return knockbackResistance;
  }

  public float getAttackSpeedBonus() {
    return attackSpeedBonus;
  }

  public float getSynergy() {
    return synergy;
  }

  public EnchantmentAffinities getEnchantmentAffinities() {
    return enchantmentAffinities;
  }

  /**
   * Get the total number of augments this equipment can hold, including any
   * that are already on it.
   * 
   * @return The number of augments that can be held.
   */
  public int getAugmentCapacity() {
    return augmentCapacity;
  }

//  // Not needed right now, and not likely to be needed soon.
//  public CompositeMaterial merge(CompositeMaterial other) {
//    return new CompositeMaterial(Math.max(miningLevel, other.miningLevel),
//        durability + other.durability, miningSpeed + other.miningSpeed,
//        attackDamage + other.attackDamage,
//        enchantability + other.enchantability,
//        attackSpeedBonus + other.attackSpeedBonus, synergy + other.synergy - 1);
//  }

  public CompositeMaterial merge(
      flirora.suludom.material.tool.ToolMaterial other) {
    return new CompositeMaterial(Math.max(miningLevel, other.getMiningLevel()),
        durability + other.getDurability(),
        miningSpeed + other.getMiningSpeed(), meleeDamage + other.getDamage(),
        rangedDamage, armor, armorToughness, knockbackResistance,
        enchantability + other.getEnchantability(),
        attackSpeedBonus + other.getAttackSpeedBonus(), synergy,
        enchantmentAffinities.merge(other.getEnchantmentAffinities()),
        augmentCapacity);
  }

  public CompositeMaterial merge(
      flirora.suludom.material.armor.ArmorMaterial other) {
    return new CompositeMaterial(miningLevel,
        durability + other.getDurabilityFactor(), miningSpeed, meleeDamage,
        rangedDamage, armor + other.getArmor(),
        armorToughness + other.getArmorToughness(),
        knockbackResistance + other.getKnockbackResistance(),
        enchantability + other.getEnchantability(), attackSpeedBonus, synergy,
        enchantmentAffinities.merge(other.getEnchantmentAffinities()),
        augmentCapacity);
  }

  public CompositeMaterial merge(AugmentMaterial other) {
    return new CompositeMaterial(miningLevel,
        durability + other.getDurabilityBonus(),
        miningSpeed + other.getMiningSpeedBonus(),
        meleeDamage + other.getMeleeDamageBonus(),
        rangedDamage + other.getRangedDamageBonus(),
        armor + other.getArmorBonus(),
        armorToughness + other.getArmorToughnessBonus(),
        knockbackResistance + other.getKnockbackResistanceBonus(),
        enchantability + other.getEnchantabilityBonus(),
        attackSpeedBonus + other.getAttackSpeedBonus(), synergy,
        enchantmentAffinities.merge(other.getEnchantmentAffinities()),
        augmentCapacity + other.getBonusAugments());
  }

  public CompositeMaterial scale(float factor) {
    return new CompositeMaterial(miningLevel, (int) (durability * factor),
        miningSpeed * factor, meleeDamage * factor, rangedDamage * factor,
        armor * factor, armorToughness * factor, knockbackResistance * factor,
        (int) (enchantability * factor), attackSpeedBonus * factor, synergy,
        enchantmentAffinities.scale(factor), augmentCapacity);
  }

  public CompositeMaterial applySynergy(float factor) {
    return new CompositeMaterial(miningLevel, (int) (durability * factor),
        miningSpeed * factor, meleeDamage * factor, rangedDamage * factor,
        armor * factor, armorToughness * factor, knockbackResistance * factor,
        enchantability, attackSpeedBonus, synergy * factor,
        enchantmentAffinities, augmentCapacity);
  }

  public CompositeMaterial merge(RodMaterial other) {
    return new CompositeMaterial(miningLevel,
        durability + other.getDurabilityBonus(),
        miningSpeed + other.getMiningSpeedBonus(),
        meleeDamage + other.getDamageBonus(), rangedDamage, armor,
        armorToughness, knockbackResistance,
        enchantability + other.getEnchantabilityBonus(),
        attackSpeedBonus + other.getAttackSpeedBonus(), synergy,
        enchantmentAffinities.merge(other.getEnchantmentAffinities()),
        augmentCapacity + other.getBonusAugments());
  }

  public CompositeMaterial scaleDurabilityFor(EquipmentSlot slot) {
    return new CompositeMaterial(miningLevel,
        durability * DURABILITY_PER_SLOT[slot.getEntitySlotId()], miningSpeed,
        meleeDamage, rangedDamage, armor, armorToughness, knockbackResistance,
        enchantability, attackSpeedBonus, synergy, enchantmentAffinities,
        augmentCapacity);
  }

  public CompoundTag toTag(CompoundTag tag) {
    tag.putInt("MiningLevel", miningLevel);
    tag.putInt("Durability", durability);
    tag.putFloat("MiningSpeed", miningSpeed);
    tag.putFloat("AttackDamage", meleeDamage);
    tag.putFloat("RangedDamage", rangedDamage);
    tag.putFloat("Armor", armor);
    tag.putFloat("ArmorToughness", armorToughness);
    tag.putFloat("KnockbackResistance", knockbackResistance);
    tag.putInt("Enchantability", enchantability);
    tag.putFloat("AttackSpeedBonus", attackSpeedBonus);
    tag.putFloat("Synergy", synergy);
    tag.put("Affinities", enchantmentAffinities.toTag());
    tag.putInt("AugmentCapacity", augmentCapacity);
    return tag;
  }

  public static CompositeMaterial fromTag(CompoundTag tag) {
    return new CompositeMaterial(tag.getInt("MiningLevel"),
        tag.getInt("Durability"), tag.getFloat("MiningSpeed"),
        tag.getFloat("AttackDamage"), tag.getFloat("RangedDamage"),
        tag.getFloat("Armor"), tag.getFloat("ArmorToughness"),
        tag.getFloat("KnockbackResistance"), tag.getInt("Enchantability"),
        tag.getFloat("AttackSpeedBonus"), tag.getFloat("Synergy"),
        EnchantmentAffinities.fromTag(tag.getList("Affinities", 10)),
        tag.getInt("AugmentCapacity"));
  }

  @Override
  public String toString() {
    return "CompositeMaterial [miningLevel=" + miningLevel + ", durability="
        + durability + ", miningSpeed=" + miningSpeed + ", meleeDamage="
        + meleeDamage + ", rangedDamage=" + rangedDamage + ", armor=" + armor
        + ", armorToughness=" + armorToughness + ", knockbackResistance="
        + knockbackResistance + ", enchantability=" + enchantability
        + ", attackSpeedBonus=" + attackSpeedBonus + ", synergy=" + synergy
        + ", enchantmentAffinities=" + enchantmentAffinities
        + ", augmentCapacity=" + augmentCapacity + "]";
  }

  @Environment(EnvType.CLIENT)
  private void appendTooltipPostCommon(List<Text> tooltip, ItemStack stack) {
    if (stack.hasEnchantments()) {
      tooltip.add(new TranslatableText("tooltip.suludom.already_enchanted")
          .formatted(Formatting.AQUA));
      return;
    }
    if (Screen.hasShiftDown()) {
      TooltipUtils.formatEnchantmentAffinities(tooltip, enchantmentAffinities,
          stack);
    } else {
      tooltip.add(TooltipUtils.pressShiftText());
    }
  }

  @Environment(EnvType.CLIENT)
  public void appendTooltip(List<Text> tooltip, int currentDurability,
      GenericTool tool, ItemStack stack, int currentAugmentCount) {
    tooltip
        .add(new TranslatableText("tooltip.suludom.mining_level",
            new TranslatableText("suludom.mining_level." + miningLevel)
                .formatted(TooltipUtils.getMiningLevelColor(miningLevel)),
            miningLevel));
    tooltip.add(new TranslatableText("tooltip.suludom.durability",
        currentDurability, durability).formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.mining_speed",
        TooltipUtils.roundTo1Decimal(miningSpeed)).formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.melee_damage",
        TooltipUtils.roundTo1Decimal(tool.getTotalMeleeDamage(this)))
            .formatted(Formatting.GREEN));
    tooltip.add(
        new TranslatableText("tooltip.suludom.enchantability", enchantability)
            .formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.attack_speed",
        TooltipUtils.roundTo1Decimal(tool.getTotalAttackSpeed(this)))
            .formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.synergy")
        .formatted(Formatting.WHITE)
        .append(new TranslatableText("tooltip.suludom.synergy_percent",
            Math.round(100.0f * synergy))
                .formatted(TooltipUtils.getSynergyColor(synergy))));
    tooltip.add(new TranslatableText("tooltip.suludom.augments_used",
        currentAugmentCount, augmentCapacity).formatted(Formatting.GREEN));
    appendTooltipPostCommon(tooltip, stack);
  }

  @Environment(EnvType.CLIENT)
  public void appendTooltip(List<Text> tooltip, int currentDurability,
      GenericArmor armor, ItemStack stack, int currentAugmentCount) {
    EquipmentSlot slot =
        armor instanceof ArmorItem ? ((ArmorItem) armor).getSlotType() : null;
    tooltip.add(new TranslatableText("tooltip.suludom.durability",
        currentDurability, getDurability()).formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.armor",
        getProtectionAmountFloat(slot)).formatted(Formatting.GREEN));
    tooltip
        .add(new TranslatableText("tooltip.suludom.armor.full_set", getArmor())
            .formatted(Formatting.DARK_GREEN).formatted(Formatting.ITALIC));
    tooltip.add(
        new TranslatableText("tooltip.suludom.armorToughness", armorToughness)
            .formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.knockbackResistance",
        knockbackResistance).formatted(Formatting.GREEN));
    tooltip.add(
        new TranslatableText("tooltip.suludom.enchantability", enchantability)
            .formatted(Formatting.GREEN));
    tooltip.add(new TranslatableText("tooltip.suludom.synergy")
        .formatted(Formatting.WHITE)
        .append(new TranslatableText("tooltip.suludom.synergy_percent",
            Math.round(100.0f * synergy))
                .formatted(TooltipUtils.getSynergyColor(synergy))));
    tooltip.add(new TranslatableText("tooltip.suludom.augments_used",
        currentAugmentCount, augmentCapacity).formatted(Formatting.GREEN));
    appendTooltipPostCommon(tooltip, stack);
  }

  public class AsToolMaterial implements ToolMaterial {
    @Override
    public int getDurability() {
      return CompositeMaterial.this.getDurability();
    }

    @Override
    public float getMiningSpeedMultiplier() {
      return CompositeMaterial.this.getMiningSpeed();
    }

    @Override
    public float getAttackDamage() {
      return CompositeMaterial.this.getAttackDamage();
    }

    @Override
    public int getMiningLevel() {
      return CompositeMaterial.this.getMiningLevel();
    }

    @Override
    public int getEnchantability() {
      return CompositeMaterial.this.getEnchantability();
    }

    @Override
    public Ingredient getRepairIngredient() {
      return null;
    }
    //
  }

  public ToolMaterial asToolMaterial() {
    return new AsToolMaterial();
  }

  public class AsArmorMaterial implements ArmorMaterial {

    @Override
    public int getDurability(EquipmentSlot slot) {
      return CompositeMaterial.this.getDurability();
    }

    @Override
    public int getProtectionAmount(EquipmentSlot slot) {
      return CompositeMaterial.this.getProtectionAmount(slot);
    }

    @Override
    public int getEnchantability() {
      return CompositeMaterial.this.getEnchantability();
    }

    @Override
    public SoundEvent getEquipSound() {
      return CompositeMaterial.this.getEquipSound();
    }

    @Override
    public Ingredient getRepairIngredient() {
      return null;
    }

    @Override
    public String getName() {
      return "generic";
    }

    @Override
    public float getToughness() {
      return CompositeMaterial.this.getToughness();
    }

    @Override
    public float getKnockbackResistance() {
      return CompositeMaterial.this.getKnockbackResistance();
    }

  }

  public ArmorMaterial asArmorMaterial() {
    return new AsArmorMaterial();
  }

}
