package flirora.suludom.material.rod;

import blue.endless.jankson.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import flirora.suludom.material.MaterialSerializer;
import flirora.suludom.material.SerializerUtils;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class RodMaterialSerializer implements MaterialSerializer<RodMaterial> {

  private static class JsonFormat {
    public int durabilityBonus;
    public float miningSpeedBonus;
    public float damageBonus;
    public float attackSpeedBonus;
    public int enchantabilityBonus;
    public int rodColor;
    public JsonObject enchantmentAffinities;
    public int bonusAugments = 0;
    @Nullable public String tag = null;
  }

  @Override public RodMaterial read(Identifier id, JsonObject json) {
    JsonFormat materialJson = new Gson().fromJson(json, JsonFormat.class);
    Tag.Identified<Item> tag = SerializerUtils.tagFromString(materialJson.tag);
    return new RodMaterial(id, materialJson.durabilityBonus,
        materialJson.miningSpeedBonus, materialJson.damageBonus,
        materialJson.attackSpeedBonus, materialJson.enchantabilityBonus,
        materialJson.rodColor,
        EnchantmentAffinities.fromJson(materialJson.enchantmentAffinities),
        materialJson.bonusAugments, tag);
  }

  @Override public RodMaterial read(Identifier id, PacketByteBuf data) {
    return new RodMaterial(id, data.readInt(), data.readFloat(),
        data.readFloat(), data.readFloat(), data.readInt(), data.readInt(),
        EnchantmentAffinities.fromData(data), data.readInt(),
        SerializerUtils.readItemTag(data));
  }

  @Override public void write(RodMaterial material, PacketByteBuf data) {
    data.writeInt(material.getDurabilityBonus());
    data.writeFloat(material.getMiningSpeedBonus());
    data.writeFloat(material.getDamageBonus());
    data.writeFloat(material.getAttackSpeedBonus());
    data.writeInt(material.getEnchantabilityBonus());
    data.writeInt(material.getRodColor());
    material.getEnchantmentAffinities().write(data);
    data.writeInt(material.getBonusAugments());
    SerializerUtils.writeItemTag(data, material.getTag());
  }

}
