package flirora.suludom.material.rod;

import flirora.suludom.item.tool.TooltipUtils;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

public class RodMaterial implements Material {
  private final Identifier id;
  private final int durabilityBonus;
  private final float miningSpeedBonus;
  private final float damageBonus;
  private final float attackSpeedBonus;
  private final int enchantabilityBonus;
  private final int rodColor;
  private final EnchantmentAffinities enchantmentAffinities;
  private final int bonusAugments;
  private final Tag.Identified<Item> tag;

  public RodMaterial(Identifier id, int durabilityBonus, float miningSpeedBonus,
      float damageBonus, float attackSpeedBonus, int enchantabilityBonus,
      int rodColor, EnchantmentAffinities enchantmentAffinities,
      int bonusAugments, Tag.Identified<Item> tag) {
    this.id = id;
    this.durabilityBonus = durabilityBonus;
    this.miningSpeedBonus = miningSpeedBonus;
    this.damageBonus = damageBonus;
    this.attackSpeedBonus = attackSpeedBonus;
    this.enchantabilityBonus = enchantabilityBonus;
    this.rodColor = rodColor;
    this.enchantmentAffinities = enchantmentAffinities;
    this.bonusAugments = bonusAugments;
    this.tag = tag;
  }

  public int getDurabilityBonus() {
    return durabilityBonus;
  }

  public float getMiningSpeedBonus() {
    return miningSpeedBonus;
  }

  public float getDamageBonus() {
    return damageBonus;
  }

  public float getAttackSpeedBonus() {
    return attackSpeedBonus;
  }

  public int getEnchantabilityBonus() {
    return enchantabilityBonus;
  }

  public int getRodColor() {
    return rodColor;
  }

  @Override
  public Identifier getId() {
    return id;
  }

  @Override
  public String getType() {
    return "rod_material";
  }

  @Override
  public MaterialType<?> getTypeObject() {
    return MaterialType.ROD_MATERIAL;
  }

  @Override
  public EnchantmentAffinities getEnchantmentAffinities() {
    return enchantmentAffinities;
  }

  public int getBonusAugments() {
    return bonusAugments;
  }

  @Override
  public List<Text> getReiInfo() {
    List<Text> result = new ArrayList<>();
    result.add(TooltipUtils.bonusText("tooltip.suludom.durability_info",
        durabilityBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.mining_speed",
        miningSpeedBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.attack_speed",
        attackSpeedBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.enchantability",
        enchantabilityBonus));
    result.add(TooltipUtils.bonusText("tooltip.suludom.bonus_augments",
        bonusAugments));
    return result;
  }

  @Override public Tag.Identified<Item> getTag() {
    return tag;
  }
}
