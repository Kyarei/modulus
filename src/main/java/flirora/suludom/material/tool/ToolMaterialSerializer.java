package flirora.suludom.material.tool;

import blue.endless.jankson.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import flirora.suludom.material.MaterialCategory;
import flirora.suludom.material.MaterialSerializer;
import flirora.suludom.material.SerializerUtils;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

public class ToolMaterialSerializer
    implements MaterialSerializer<ToolMaterial> {

  private static class JsonFormat {
    public int miningLevel;
    public int durability;
    public float miningSpeed;
    public float damage;
    public float attackSpeedBonus;
    public int enchantability;
    public int toolHeadColor;
    public String materialCategory;
    public JsonObject enchantmentAffinities;
    @Nullable public String tag = null;
  }

  @Override public ToolMaterial read(Identifier id, JsonObject json) {
    JsonFormat materialJson = new Gson().fromJson(json, JsonFormat.class);
    MaterialCategory cat =
        MaterialCategory.fromString(materialJson.materialCategory);
    if (cat == null)
      throw new JsonSyntaxException(
          "Unknown category " + materialJson.materialCategory);
    Tag.Identified<Item> tag = SerializerUtils.tagFromString(materialJson.tag);
    return new ToolMaterial(id, materialJson.miningLevel,
        materialJson.durability, materialJson.miningSpeed, materialJson.damage,
        materialJson.attackSpeedBonus, materialJson.enchantability,
        materialJson.toolHeadColor, cat,
        EnchantmentAffinities.fromJson(materialJson.enchantmentAffinities),
        tag);
  }

  @Override public ToolMaterial read(Identifier id, PacketByteBuf data) {
    return new ToolMaterial(id, data.readInt(), data.readInt(),
        data.readFloat(), data.readFloat(), data.readFloat(), data.readInt(),
        data.readInt(), MaterialCategory.values()[data.readByte()],
        EnchantmentAffinities.fromData(data),
        SerializerUtils.readItemTag(data));
  }

  @Override public void write(ToolMaterial material, PacketByteBuf data) {
    data.writeInt(material.getMiningLevel());
    data.writeInt(material.getDurability());
    data.writeFloat(material.getMiningSpeed());
    data.writeFloat(material.getDamage());
    data.writeFloat(material.getAttackSpeedBonus());
    data.writeInt(material.getEnchantability());
    data.writeInt(material.getToolHeadColor());
    data.writeByte((byte) material.getMaterialCategory().ordinal());
    material.getEnchantmentAffinities().write(data);
    SerializerUtils.writeItemTag(data, material.getTag());
  }

}
