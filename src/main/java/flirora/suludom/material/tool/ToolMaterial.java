package flirora.suludom.material.tool;

import flirora.suludom.material.HasMaterialCategory;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialCategory;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import net.minecraft.item.Item;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes a material for a material-and-sticks tool (sword, pickaxe, axe,
 * shovel or hoe).
 * 
 * @author flirora
 *
 */
public class ToolMaterial implements Material, HasMaterialCategory {
  private final Identifier id;
  private final int miningLevel;
  private final int durability;
  private final float miningSpeed;
  private final float damage;
  private final float attackSpeedBonus;
  private final int enchantability;
  private final int toolHeadColor;
  private final MaterialCategory materialCategory;
  private final EnchantmentAffinities enchantmentAffinities;
  private final Tag.Identified<Item> tag;

  public ToolMaterial(Identifier id, int miningLevel, int durability,
      float miningSpeed, float damage, float attackSpeedBonus,
      int enchantability, int toolHeadColor, MaterialCategory materialCategory,
      EnchantmentAffinities enchantmentAffinities, Tag.Identified<Item> tag) {
    this.id = id;
    this.miningLevel = miningLevel;
    this.durability = durability;
    this.miningSpeed = miningSpeed;
    this.damage = damage;
    this.attackSpeedBonus = attackSpeedBonus;
    this.enchantability = enchantability;
    this.toolHeadColor = toolHeadColor;
    this.materialCategory = materialCategory;
    this.enchantmentAffinities = enchantmentAffinities;
    this.tag = tag;
  }

  public int getMiningLevel() {
    return this.miningLevel;
  }

  public int getDurability() {
    return this.durability;
  }

  public float getMiningSpeed() {
    return this.miningSpeed;
  }

  public float getDamage() {
    return this.damage;
  }

  public float getAttackSpeedBonus() {
    return this.attackSpeedBonus;
  }

  public int getEnchantability() {
    return this.enchantability;
  }

  public int getToolHeadColor() {
    return this.toolHeadColor;
  }

  @Override
  public MaterialCategory getMaterialCategory() {
    return this.materialCategory;
  }

  @Override
  public Identifier getId() {
    return id;
  }

  @Override
  public String getType() {
    return "tool_material";
  }

  @Override
  public MaterialType<?> getTypeObject() {
    return MaterialType.TOOL_MATERIAL;
  }

  @Override
  public EnchantmentAffinities getEnchantmentAffinities() {
    return enchantmentAffinities;
  }

  @Override
  public List<Text> getReiInfo() {
    List<Text> result = new ArrayList<>();
    result.add(new TranslatableText("tooltip.suludom.mining_level.info",
        new TranslatableText("suludom.mining_level." + miningLevel),
        miningLevel));
    result.add(
        new TranslatableText("tooltip.suludom.durability_info", durability));
    result
        .add(new TranslatableText("tooltip.suludom.mining_speed", miningSpeed));
    result.add(new TranslatableText("tooltip.suludom.melee_damage", damage));
    result.add(
        new TranslatableText("tooltip.suludom.attack_speed", attackSpeedBonus));
    result.add(
        new TranslatableText("tooltip.suludom.enchantability", enchantability));
    result.add(materialCategory.getText());
    return result;
  }

  @Override public Tag.Identified<Item> getTag() {
    return tag;
  }
}
