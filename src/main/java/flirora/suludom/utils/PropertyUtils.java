package flirora.suludom.utils;

import net.minecraft.screen.Property;

public class PropertyUtils {
  public static Property create(final boolean[] is, final int key) {
    return new Property() {
      public int get() {
        return is[key] ? 1 : 0;
      }

      public void set(int value) {
        is[key] = value != 0;
      }
    };
  }
}
