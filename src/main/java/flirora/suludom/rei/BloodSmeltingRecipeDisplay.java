package flirora.suludom.rei;

import com.google.common.collect.ImmutableList;
import flirora.suludom.recipe.BloodSmeltingRecipe;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.TransferRecipeDisplay;
import me.shedaniel.rei.server.ContainerInfo;
import me.shedaniel.rei.utils.CollectionUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.recipe.Ingredient;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

public class BloodSmeltingRecipeDisplay implements TransferRecipeDisplay {

  private BloodSmeltingRecipe display;
  private List<List<EntryStack>> input;
  private List<EntryStack> output;

  @Environment(EnvType.CLIENT)
  private List<EntryStack> makeEntryStacks(Ingredient i) {
    List<EntryStack> result =
        CollectionUtils.map(i.getMatchingStacksClient(), EntryStack::create);
    Collections.shuffle(result);
    return result;
  }

  @Environment(EnvType.CLIENT)
  public BloodSmeltingRecipeDisplay(BloodSmeltingRecipe recipe) {
    this.display = recipe;
    this.input = ImmutableList.of(makeEntryStacks(recipe.getInput1()),
        makeEntryStacks(recipe.getInput2()));
    DecimalFormat df = new DecimalFormat("###.##");
    EntryStack secondaryEntryStack =
        EntryStack.create(recipe.getSecondaryOutput())
            .setting(EntryStack.Settings.TOOLTIP_APPEND_EXTRA,
                (es) -> Collections.singletonList(
                    new TranslatableText("rei.suludom.secondaryOutput",
                        df.format(100 * recipe.getSecondaryOutputChance()))));
    this.output = ImmutableList
        .of(EntryStack.create(recipe.getOutput()), secondaryEntryStack);
  }

  @Override public List<List<EntryStack>> getInputEntries() {
    return input;
  }

  @Override public List<EntryStack> getOutputEntries() {
    return output;
  }

  @Override public Identifier getRecipeCategory() {
    return SuludomREIPlugin.BLOOD_SMELTING;
  }

  @Override public int getWidth() {
    return 2;
  }

  @Override
  public int getHeight() {
    return 1;
  }

  @Override
  public List<List<EntryStack>> getOrganisedInputEntries(
      ContainerInfo<ScreenHandler> containerInfo, ScreenHandler container) {
    return input;
  }

  public double getCookingTime() {
    return display.getCookTime();
  }

}
