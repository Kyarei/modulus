package flirora.suludom.rei;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import flirora.suludom.recipe.ToolTableRecipe;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.plugin.crafting.DefaultCraftingDisplay;
import me.shedaniel.rei.utils.CollectionUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.recipe.Recipe;
import net.minecraft.util.Identifier;

public class ToolTableRecipeDisplay implements DefaultCraftingDisplay {

  private ToolTableRecipe display;
  private List<List<EntryStack>> input;
  private List<EntryStack> output;

  @Environment(EnvType.CLIENT)
  public ToolTableRecipeDisplay(ToolTableRecipe recipe) {
    this.display = recipe;
    this.input = Stream.of(recipe.getComponents()).map(i -> {
      List<EntryStack> result =
          CollectionUtils.map(i.getMatchingStacksClient(), EntryStack::create);
      Collections.shuffle(result);
      return result;
    }).collect(Collectors.toList());
    this.output =
        Collections.singletonList(EntryStack.create(recipe.getOutput()));
  }

  @Override
  public List<List<EntryStack>> getInputEntries() {
    return input;
  }

  @Override
  public List<EntryStack> getOutputEntries() {
    return output;
  }

  @Override
  public Identifier getRecipeCategory() {
    return SuludomREIPlugin.TOOL_TABLE;
  }

  @Override
  public int getWidth() {
    return 3;
  }

  @Override
  public int getHeight() {
    return 3;
  }

  @Override
  public Optional<Recipe<?>> getOptionalRecipe() {
    return Optional.of(display);
  }

}
