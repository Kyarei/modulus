package flirora.suludom.rei;

import flirora.suludom.SuludomMod;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.plugin.information.DefaultInformationCategory;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.util.Identifier;

public class MaterialCategory extends DefaultInformationCategory {

  @Override
  public Identifier getIdentifier() {
    return SuludomREIPlugin.MATERIAL;
  }

  @Override
  public EntryStack getLogo() {
    return EntryStack.create(SuludomMod.NEBULITE_INGOT);
  }

  @Environment(EnvType.CLIENT)
  @Override
  public String getCategoryName() {
    return I18n.translate("category.suludom.material");
  }

  public MaterialCategory() {
    // TODO Auto-generated constructor stub
  }

}
