package flirora.suludom.rei;

import flirora.suludom.SuludomMod;
import flirora.suludom.block.BloodFurnaceBlockEntity;
import it.unimi.dsi.fastutil.ints.IntList;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.TransferRecipeCategory;
import me.shedaniel.rei.api.widgets.Widgets;
import me.shedaniel.rei.gui.widget.Widget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

public class BloodSmeltingCategory
    implements TransferRecipeCategory<BloodSmeltingRecipeDisplay> {

  public BloodSmeltingCategory() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public Identifier getIdentifier() {
    return SuludomREIPlugin.BLOOD_SMELTING;
  }

  @Environment(EnvType.CLIENT)
  @Override
  public String getCategoryName() {
    return I18n.translate("category.suludom.blood_smelting");
  }

  @Environment(EnvType.CLIENT)
  @Override
  public List<Widget> setupDisplay(BloodSmeltingRecipeDisplay recipeDisplay,
      Rectangle bounds) {
    Point startPoint = new Point(bounds.getCenterX() - 41, bounds.y + 10);
    double cookingTime = recipeDisplay.getCookingTime();
    DecimalFormat df = new DecimalFormat("###.##");
    List<Widget> widgets = new LinkedList<>();
    Text text = new TranslatableText("category.rei.campfire.time",
        df.format(cookingTime / 20d));
    widgets.add(Widgets
        .createLabel(new Point(bounds.x + bounds.width - 5, bounds.y + 5), text)
        .noShadow().rightAligned().color(0xFF404040, 0xFFBBBBBB));
    widgets.add(Widgets
        .createBurningFire(new Point(startPoint.x - 1, startPoint.y + 20))
        .animationDurationTicks(BloodFurnaceBlockEntity.ENDER_EYE_BURN_TIME));
    widgets.add(
        Widgets.createArrow(new Point(startPoint.x + 28, startPoint.y + 8))
            .animationDurationTicks(cookingTime));
    widgets.add(
        Widgets.createSlot(new Point(startPoint.x - 11, startPoint.y + 1))
            .entries(recipeDisplay.getInputEntries().get(0)).markInput());
    widgets.add(
        Widgets.createSlot(new Point(startPoint.x + 9, startPoint.y + 1))
            .entries(recipeDisplay.getInputEntries().get(1)).markInput());
    widgets.add(
        Widgets.createSlot(new Point(startPoint.x + 61, startPoint.y + 9))
            .entry(recipeDisplay.getOutputEntries().get(0)).markOutput());
    widgets.add(
        Widgets.createSlot(new Point(startPoint.x + 81, startPoint.y + 9))
            .entry(recipeDisplay.getOutputEntries().get(1)).markOutput());
    return widgets;
  }

  @Override public EntryStack getLogo() {
    return EntryStack.create(SuludomMod.BLOOD_FURNACE);
  }

  @Environment(EnvType.CLIENT) @Override
  public void renderRedSlots(MatrixStack matrixStack, List<Widget> list,
      Rectangle bounds, BloodSmeltingRecipeDisplay bloodSmeltingRecipeDisplay,
      IntList redSlots) {
    Point startPoint =
        new Point(bounds.getCenterX() - 41, bounds.getCenterY() - 27);
    matrixStack.push();
    matrixStack.translate(0, 0, 400);
    if (redSlots.contains(0)) {
      DrawableHelper.fill(matrixStack, startPoint.x - 11, startPoint.y + 1,
          startPoint.x - 11 + 16, startPoint.y + 1 + 16, 1090453504);
    }
    if (redSlots.contains(1)) {
      DrawableHelper.fill(matrixStack, startPoint.x + 9, startPoint.y + 1,
          startPoint.x + 9 + 16, startPoint.y + 1 + 16, 1090453504);
    }
    matrixStack.pop();
  }
}
