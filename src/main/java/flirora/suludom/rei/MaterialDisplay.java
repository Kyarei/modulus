package flirora.suludom.rei;

import java.util.List;

import com.google.common.collect.ImmutableList;

import flirora.suludom.material.Material;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.plugin.information.DefaultInformationDisplay;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class MaterialDisplay<M extends Material>
    extends DefaultInformationDisplay {

  private MaterialDisplay(M material, List<EntryStack> entryStacks, Text name) {
    super(entryStacks, name);
    this.lines(material.getReiInfo());
  }

  // I have to do this since I can't put any statements before the super call of
  // a ctor.
  @Environment(EnvType.CLIENT)
  public static <M extends Material> MaterialDisplay<M> of(M material) {
    Identifier id = material.getId();
    Item item = Registry.ITEM.get(id);
    List<EntryStack> entryStacks = ImmutableList.of(EntryStack.create(item));
    Text name = new TranslatableText("rei.suludom.material.title",
        item.getName(), new TranslatableText(
            "modular_component.suludom." + material.getType()));
    return new MaterialDisplay<>(material, entryStacks, name);
  }

}
