package flirora.suludom.rei;

import flirora.suludom.recipe.BloodSmeltingRecipe;
import flirora.suludom.recipe.ToolTableRecipe;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.RecipeHelper;
import me.shedaniel.rei.api.plugins.REIPluginV0;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class SuludomREIPlugin implements REIPluginV0 {

  public static final Identifier PLUGIN =
      new Identifier("suludom", "rei_plugin");
  public static final Identifier TOOL_TABLE =
      new Identifier("suludom", "plugins/tool_table_recipe");
  public static final Identifier BLOOD_SMELTING =
      new Identifier("suludom", "plugins/blood_smelting_recipe");
  public static final Identifier MATERIAL =
      new Identifier("suludom", "plugins/material");

  public SuludomREIPlugin() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public Identifier getPluginIdentifier() {
    return PLUGIN;
  }

  @Override
  public void registerPluginCategories(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerPluginCategories(recipeHelper);
    recipeHelper.registerCategory(new ToolTableCategory());
    recipeHelper.registerCategory(new BloodSmeltingCategory());
    recipeHelper.registerCategory(new MaterialCategory());
  }

  @Override
  public void registerRecipeDisplays(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerRecipeDisplays(recipeHelper);
    recipeHelper.registerRecipes(TOOL_TABLE, ToolTableRecipe.class,
        ToolTableRecipeDisplay::new);
    recipeHelper.registerRecipes(BLOOD_SMELTING, BloodSmeltingRecipe.class,
        BloodSmeltingRecipeDisplay::new);
    recipeHelper.registerLiveRecipeGenerator(new LiveMaterialRecipeGenerator());
  }

  @Override
  public void registerOthers(RecipeHelper recipeHelper) {
    REIPluginV0.super.registerOthers(recipeHelper);
    recipeHelper.registerWorkingStations(TOOL_TABLE, EntryStack
        .create(Registry.ITEM.get(new Identifier("suludom", "tool_table"))));
    recipeHelper.removeAutoCraftButton(TOOL_TABLE);
    recipeHelper.registerWorkingStations(BLOOD_SMELTING, EntryStack
        .create(Registry.ITEM.get(new Identifier("suludom", "blood_furnace"))));
    recipeHelper.registerWorkingStations(MATERIAL, EntryStack.create(
        Registry.ITEM.get(new Identifier("suludom", "nebulite_ingot"))));
    recipeHelper.removeAutoCraftButton(MATERIAL);
  }

}
