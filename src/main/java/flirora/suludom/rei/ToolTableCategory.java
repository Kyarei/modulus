package flirora.suludom.rei;

import flirora.suludom.SuludomMod;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.plugin.crafting.DefaultCraftingCategory;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.util.Identifier;

public class ToolTableCategory extends DefaultCraftingCategory {

  @Override
  public Identifier getIdentifier() {
    return SuludomREIPlugin.TOOL_TABLE;
  }

  @Override
  public EntryStack getLogo() {
    return EntryStack.create(SuludomMod.TOOL_TABLE);
  }

  @Environment(EnvType.CLIENT)
  @Override
  public String getCategoryName() {
    return I18n.translate("category.suludom.tool_crafting");
  }

  public ToolTableCategory() {
    // TODO Auto-generated constructor stub
  }

}
