package flirora.suludom.rei;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import flirora.suludom.SuludomMod;
import flirora.suludom.material.Material;
import me.shedaniel.rei.api.EntryStack;
import me.shedaniel.rei.api.LiveRecipeGenerator;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class LiveMaterialRecipeGenerator
    implements LiveRecipeGenerator<MaterialDisplay<?>> {

  public LiveMaterialRecipeGenerator() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public Identifier getCategoryIdentifier() {
    // TODO Auto-generated method stub
    return SuludomREIPlugin.MATERIAL;
  }

  @Override
  public Optional<List<MaterialDisplay<?>>> getUsageFor(EntryStack entry) {
    Item item = entry.getItem();
    List<MaterialDisplay<?>> usages = new ArrayList<>();
    SuludomMod.getMaterialManager().forEachMaterialType((type, registry) -> {
      Material material = registry.get(Registry.ITEM.getId(item));
      if (material != null) {
        usages.add(MaterialDisplay.of(material));
      }
    });
    return Optional.of(usages);
  }

}
