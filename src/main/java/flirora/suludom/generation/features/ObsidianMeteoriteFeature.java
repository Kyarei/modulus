package flirora.suludom.generation.features;

import com.mojang.serialization.Codec;
import flirora.suludom.SuludomMod;
import it.unimi.dsi.fastutil.PriorityQueue;
import it.unimi.dsi.fastutil.objects.ObjectHeapPriorityQueue;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ServerWorldAccess;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.Feature;

import java.util.*;
import java.util.stream.Collectors;

public class ObsidianMeteoriteFeature extends Feature<DefaultFeatureConfig> {
  public static final ObsidianMeteoriteFeature INSTANCE =
      new ObsidianMeteoriteFeature(DefaultFeatureConfig.CODEC);

  private ObsidianMeteoriteFeature(Codec<DefaultFeatureConfig> codec) {
    super(codec);
  }

  @Override public boolean generate(ServerWorldAccess serverWorldAccess,
      StructureAccessor accessor, ChunkGenerator generator, Random random,
      BlockPos pos, DefaultFeatureConfig config) {
    BlockPos center = pos;
    while (true) {
      if (center.getY() < 90)
        return false;
      Block block = serverWorldAccess.getBlockState(center.down()).getBlock();
      if (isSoil(block) || isStone(block))
        break;
      center = center.down();
    }
    center = center.down(1 + random.nextInt(2) + random.nextInt(3));
    int sizeX = 4 + random.nextInt(4) + random.nextInt(3);
    int sizeY = 3 + random.nextInt(4) + random.nextInt(2);
    int sizeZ = 4 + random.nextInt(4) + random.nextInt(3);
    PriorityQueue<Pair<Float, BlockPos>> queue = new ObjectHeapPriorityQueue<>(
        (a, b) -> Float.compare(a.getLeft(), b.getLeft()));
    Set<BlockPos> visited = new HashSet<>();
    queue.enqueue(new Pair<>(0.0f, center));
    while (!queue.isEmpty()) {
      BlockPos current = queue.dequeue().getRight();
      visited.add(current);
      Block chosen =
          random.nextInt(100) == 0 ? SuludomMod.NEBULITE_ORE : Blocks.OBSIDIAN;
      serverWorldAccess.setBlockState(current, chosen.getDefaultState(), 4);
      if (Math.abs(current.getX() - center.getX()) >= sizeX
          || Math.abs(current.getY() - center.getY()) >= sizeY
          || Math.abs(current.getZ() - center.getZ()) >= sizeZ)
        break;
      BlockPos[] neighbors = { current.north(), current.south(), current.east(),
          current.west(), current.up(), current.down() };
      List<BlockPos> eligible = Arrays.stream(neighbors)
          .filter((p) -> !visited.contains(p)).collect(Collectors.toList());
      Collections.shuffle(eligible, random);
      for (BlockPos candidate : eligible) {
        queue.enqueue(
            new Pair<>((float) (Math.sqrt(center.getSquaredDistance(candidate))
                + random.nextGaussian()), candidate));
      }
    }
    return true;
  }

}
