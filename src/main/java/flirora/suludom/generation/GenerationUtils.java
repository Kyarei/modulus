package flirora.suludom.generation;

import flirora.suludom.SuludomMod;
import flirora.suludom.generation.features.ObsidianMeteoriteFeature;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.decorator.ChanceRangeDecoratorConfig;
import net.minecraft.world.gen.decorator.CountDepthDecoratorConfig;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.FeatureConfig;
import net.minecraft.world.gen.feature.OreFeatureConfig;

public class GenerationUtils {
  public static void addToNetherBiome(Biome biome) {
    biome.addFeature(GenerationStep.Feature.UNDERGROUND_ORES,
        Feature.ORE
            .configure(new OreFeatureConfig(OreFeatureConfig.Target.NETHERRACK,
                SuludomMod.BLOOD_LAPIS_ORE.getDefaultState(), 6))
            .createDecoratedFeature(Decorator.COUNT_RANGE
                .configure(new RangeDecoratorConfig(20, 0, 0, 128))));
    biome.addFeature(GenerationStep.Feature.UNDERGROUND_ORES,
        Feature.ORE
            .configure(new OreFeatureConfig(OreFeatureConfig.Target.NETHERRACK,
                SuludomMod.HOMURA_ORE.getDefaultState(), 4))
            .createDecoratedFeature(Decorator.COUNT_DEPTH_AVERAGE
                .configure(new CountDepthDecoratorConfig(8, 30, 10))));
  }

  public static void addToOverworldLandBiome(Biome biome) {
    biome.addFeature(GenerationStep.Feature.LOCAL_MODIFICATIONS,
        ObsidianMeteoriteFeature.INSTANCE.configure(FeatureConfig.DEFAULT)
            .createDecoratedFeature(Decorator.CHANCE_RANGE.configure(
                new ChanceRangeDecoratorConfig(0.04f, 90, 90, 255))));
  }

  public static void registerFeatures() {
    Registry.register(Registry.FEATURE,
        new Identifier("suludom", "obsidian_meteorite"),
        ObsidianMeteoriteFeature.INSTANCE);
    Registry.BIOME.forEach((biome) -> {
      Category category = biome.getCategory();
      if (category == Biome.Category.NETHER) {
        addToNetherBiome(biome);
      }
      if (category != Biome.Category.NETHER
          && category != Biome.Category.THEEND) {
        if (category != Biome.Category.OCEAN
            && category != Biome.Category.RIVER) {
          addToOverworldLandBiome(biome);
        }
      }
    });
  }
}
