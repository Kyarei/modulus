package flirora.suludom.recipe;

import com.google.gson.JsonObject;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.item.tool.RecipeResult;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ExactItemComponent implements Component {

  private Identifier wanted;

  public ExactItemComponent(Identifier wanted) {
    super();
    this.wanted = wanted;
  }

  @Override
  public boolean tryAddMaterial(RecipeResult result, Item item) {
    Identifier id = Registry.ITEM.getId(item);
    return id.equals(wanted);
  }

  @Override
  @Nullable
  public TranslatableText getText() {
    return new TranslatableText("modular_component.suludom.exact",
        Registry.ITEM.get(wanted).getTranslationKey());
  }

  @Override
  public JsonObject toJson() {
    JsonObject obj = new JsonObject();
    obj.addProperty("type", "exact");
    obj.addProperty("item", wanted.toString());
    return obj;
  }

  @Override
  public void write(PacketByteBuf data) {
    data.writeByte(1);
    data.writeIdentifier(wanted);
  }

  @Override
  public ItemStack[] getMatchingStacksClient() {
    return new ItemStack[] { new ItemStack(Registry.ITEM.get(wanted)) };
  }

}
