package flirora.suludom.recipe;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.block.ToolTableBlockEntity;
import flirora.suludom.item.tool.RecipeResult;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class ToolTableRecipe implements Recipe<ToolTableBlockEntity> {
  private final Identifier id;
  private final Component[] components;
  private final Item output;

  public ToolTableRecipe(Identifier id, Component[] components, Item output) {
    if (components.length != 9) {
      throw new IllegalArgumentException("Recipes need exactly 9 components!");
    }
    this.components = components;
    this.id = id;
    this.output = output;
  }

  public Component[] getComponents() {
    return components;
  }

  public Item getOutputItem() {
    return output;
  }

  @Nullable
  public RecipeResult tryRecipe(ToolTableBlockEntity items) {
    if (items.size() != 9) {
      throw new IllegalArgumentException("Wrong number of items to recipe");
    }
    RecipeResult result = new RecipeResult();
    for (int i = 0; i < 9; ++i) {
      ItemStack item = items.getStack(i);
      Component component = components[i];
      if (item.isDamaged())
        return null;
      boolean succeeded = component.tryAddMaterial(result, item.getItem());
      if (!succeeded)
        return null;
    }
    return result;
  }

  @Override
  public boolean matches(ToolTableBlockEntity inv, World world) {
    if (inv.size() != 9)
      return false;
    return tryRecipe(inv) != null;
  }

  @Override
  public ItemStack craft(ToolTableBlockEntity inv) {
    return ItemStack.EMPTY;
  }

  @Override
  public boolean fits(int width, int height) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public ItemStack getOutput() {
    return new ItemStack(output, 1);
  }

  @Override
  public Identifier getId() {
    return id;
  }

  @Override
  public RecipeSerializer<?> getSerializer() {
    return ToolTableRecipeSerializer.INSTANCE;
  }

  public static class Type implements RecipeType<ToolTableRecipe> {
    private Type() {
    }

    public static final Type INSTANCE = new Type();

    public static final String ID = "tool_table_recipe";
  }

  @Override
  public RecipeType<?> getType() {
    // TODO Auto-generated method stub
    return Type.INSTANCE;
  }

}
