package flirora.suludom.recipe;

import com.google.gson.JsonObject;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.SuludomMod;
import flirora.suludom.item.tool.RecipeResult;
import flirora.suludom.material.MaterialType;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;

public interface Component {
  /**
   * Try to add an item as a material.
   * 
   * This does not check for damaged items (since @ref Item does not store it;
   * it is left to @ref ItemStack).
   * 
   * @param result The @ref RecipeResult to which to add the new material, if
   *               doing so was successful. Will be mutated.
   * @param item   The item to try to add.
   * @return true if succeeded, false if failed (due to wrong item).
   */
  boolean tryAddMaterial(RecipeResult result, Item item);

  @Nullable
  TranslatableText getText();

  JsonObject toJson();

  public void write(PacketByteBuf data);

  @Environment(EnvType.CLIENT)
  public ItemStack[] getMatchingStacksClient();

  public static Component fromJson(JsonObject from) {
    String typeName = from.get("type").getAsString();
    switch (typeName) {
    case "empty":
      return EMPTY;
    case "exact":
      return new ExactItemComponent(
          Identifier.tryParse(from.get("item").getAsString()));
    default: {
      MaterialType<?> type =
          SuludomMod.getMaterialManager().getByName(typeName);
      if (type == null)
        throw new IllegalArgumentException("unknown material type " + typeName);
      MaterialComponent<?> component = MaterialComponent.getByType(type);
      if (component == null)
        throw new IllegalArgumentException("unknown material type" + typeName);
      return component;
    }
    }
  }

  public static Component read(PacketByteBuf data) {
    switch (data.readByte()) {
    case 2: {
      MaterialType<?> type =
          SuludomMod.getMaterialManager().getByOrdinal(data.readByte());
      if (type == null)
        throw new IllegalArgumentException("unknown material type");
      MaterialComponent<?> component = MaterialComponent.getByType(type);
      if (component == null)
        throw new IllegalArgumentException(
            "unknown material type" + type.toString());
      return component;
    }
    case 0:
      return EMPTY;
    case 1:
      return new ExactItemComponent(data.readIdentifier());
    default:
      throw new IllegalArgumentException("unknown recipe type");
    }
  }

  public static Component EMPTY = new EmptyComponent();
}
