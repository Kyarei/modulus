package flirora.suludom.recipe;

import com.google.gson.JsonObject;

public class BloodSmeltingRecipeJsonFormat {
  public class PrimaryOutput {
    public String item;
    public int count = 1;
  }

  public class SecondaryOutput {
    public String item;
    public int count = 1;
    public double chance = 1.0;
  }

  public JsonObject input1;
  public JsonObject input2;
  public int cookTime;
  public PrimaryOutput result;
  public SecondaryOutput secondaryOutput;
}
