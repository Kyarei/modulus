package flirora.suludom.recipe;

import flirora.suludom.block.BloodFurnaceBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public class BloodSmeltingRecipe implements Recipe<BloodFurnaceBlockEntity> {
  private final Identifier id;
  private final Ingredient input1;
  private final Ingredient input2;
  private final int cookTime;
  private final ItemStack output;
  private final ItemStack secondaryOutput;
  private final double secondaryOutputChance;

  public BloodSmeltingRecipe(Identifier id, Ingredient input1,
      Ingredient input2, int cookTime, ItemStack output,
      ItemStack secondaryOutput, double secondaryOutputChance) {
    super();
    this.id = id;
    this.input1 = input1;
    this.input2 = input2;
    this.cookTime = cookTime;
    this.output = output;
    this.secondaryOutput = secondaryOutput;
    this.secondaryOutputChance = secondaryOutputChance;
  }

  @Override
  public boolean matches(BloodFurnaceBlockEntity inv, World world) {
    ItemStack input1 = inv.getStack(0);
    ItemStack input2 = inv.getStack(1);
    return (this.input1.test(input1) && this.input2.test(input2))
        || (this.input1.test(input2) && this.input2.test(input1));
  }

  @Override
  public ItemStack craft(BloodFurnaceBlockEntity inv) {
    return output.copy();
  }

  @Override public boolean fits(int width, int height) {
    return false;
  }

  @Override public ItemStack getOutput() {
    return output;
  }

  public ItemStack getSecondaryOutput() {
    return secondaryOutput;
  }

  public double getSecondaryOutputChance() {
    return secondaryOutputChance;
  }

  @Override public Identifier getId() {
    return id;
  }

  @Override public RecipeSerializer<?> getSerializer() {
    return BloodSmeltingRecipeSerializer.INSTANCE;
  }

  public static class Type implements RecipeType<BloodSmeltingRecipe> {
    private Type() {
    }

    public static final Type INSTANCE = new Type();

    public static final String ID = "blood_smelting_recipe";
  }

  @Override
  public RecipeType<?> getType() {
    return Type.INSTANCE;
  }

  public Ingredient getInput1() {
    return input1;
  }

  public Ingredient getInput2() {
    return input2;
  }

  public int getCookTime() {
    return cookTime;
  }

}
