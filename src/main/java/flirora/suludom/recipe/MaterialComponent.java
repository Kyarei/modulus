package flirora.suludom.recipe;

import blue.endless.jankson.annotation.Nullable;
import com.google.gson.JsonObject;
import flirora.suludom.SuludomMod;
import flirora.suludom.item.tool.RecipeResult;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

public abstract class MaterialComponent<M extends Material>
    implements Component {

  private MaterialType<M> type;
  private final JsonObject json;
  private static final Map<MaterialType<?>, MaterialComponent<?>> componentsByType =
      new HashMap<>();
  private ItemStack[] matchingStacks;

  protected abstract void addMaterial(RecipeResult result, M material);

  public MaterialComponent(MaterialType<M> type) {
    this.type = type;
    json = new JsonObject();
    json.addProperty("type", type.toString());
  }

  @Override
  public boolean tryAddMaterial(RecipeResult result, Item item) {
    M mat =
        SuludomMod.getMaterialManager().getRegistryFor(type).getFromItem(item);
    if (mat == null)
      return false;
    addMaterial(result, mat);
    return true;
  }

  @Override
  public @Nullable TranslatableText getText() {
    return new TranslatableText("modular_component.suludom." + type.toString());
  }

  @Override
  public JsonObject toJson() {
    return json;
  }

  @Override
  public void write(PacketByteBuf data) {
    data.writeByte(2);
    data.writeByte(type.getOrdinal());
  }

  @Override
  public ItemStack[] getMatchingStacksClient() {
    if (matchingStacks != null)
      return matchingStacks;
    Registry<M> materialRegistry =
        SuludomMod.getMaterialManager().getRegistryFor(type);
    return materialRegistry.stream()
        .map(m -> new ItemStack(Registry.ITEM.get(m.getId())))
        .toArray(ItemStack[]::new);
  }

  private static void register(MaterialComponent<?>... components) {
    for (MaterialComponent<?> component : components) {
      componentsByType.put(component.type, component);
    }
  }

  @SuppressWarnings("unchecked")
  public static <T extends Material> MaterialComponent<T> getByType(
      MaterialType<T> type) {
    return (MaterialComponent<T>) componentsByType.get(type);
  }

  public static final MaterialComponent<ToolMaterial> TOOL_MATERIAL =
      new MaterialComponent<ToolMaterial>(MaterialType.TOOL_MATERIAL) {
        @Override protected void addMaterial(RecipeResult result, ToolMaterial material) {
          result.addToolMaterial(material);
        }
      };

  public static final MaterialComponent<RodMaterial> ROD_MATERIAL =
      new MaterialComponent<RodMaterial>(MaterialType.ROD_MATERIAL) {
        @Override protected void addMaterial(RecipeResult result, RodMaterial material) {
          result.addRodMaterial(material);
        }
      };

  public static final MaterialComponent<ArmorMaterial> ARMOR_MATERIAL =
      new MaterialComponent<ArmorMaterial>(MaterialType.ARMOR_MATERIAL) {
        @Override protected void addMaterial(RecipeResult result,
            ArmorMaterial material) {
          result.addArmorMaterial(material);
        }
      };

  static {
    register(TOOL_MATERIAL, ROD_MATERIAL, ARMOR_MATERIAL);
  }

}
