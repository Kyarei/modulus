package flirora.suludom.recipe;

import com.google.gson.JsonObject;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.item.tool.RecipeResult;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.TranslatableText;

public class EmptyComponent implements Component {

  protected EmptyComponent() {
    //
  }

  @Override
  public boolean tryAddMaterial(RecipeResult result, Item item) {
    return item.equals(Items.AIR);
  }

  @Override
  @Nullable
  public TranslatableText getText() {
    return null;
  }

  private static final JsonObject JSON;

  static {
    JSON = new JsonObject();
    JSON.addProperty("type", "empty");
  }

  @Override
  public JsonObject toJson() {
    return JSON;
  }

  @Override
  public void write(PacketByteBuf data) {
    data.writeByte(0);
  }

  private static final ItemStack[] NO_STACKS = {};

  @Override
  public ItemStack[] getMatchingStacksClient() {
    return NO_STACKS;
  }

}
