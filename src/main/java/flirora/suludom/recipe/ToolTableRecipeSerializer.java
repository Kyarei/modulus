package flirora.suludom.recipe;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ToolTableRecipeSerializer
    implements RecipeSerializer<ToolTableRecipe> {

  private ToolTableRecipeSerializer() {
  }

  public static ToolTableRecipeSerializer INSTANCE =
      new ToolTableRecipeSerializer();

  public static final Identifier ID =
      new Identifier("suludom", "tool_table_recipe");

  @Override
  public ToolTableRecipe read(Identifier id, JsonObject json) {
    // TODO Auto-generated method stub
    ToolTableRecipeJsonFormat recipeJson =
        new Gson().fromJson(json, ToolTableRecipeJsonFormat.class);
    if (recipeJson.components == null || recipeJson.outputItem == null) {
      throw new JsonSyntaxException("missing field");
    }
    if (recipeJson.components.size() != 9) {
      throw new JsonSyntaxException("need exactly 9 components");
    }
    Component[] components = new Component[9];
    for (int i = 0; i < 9; ++i) {
      components[i] =
          Component.fromJson(recipeJson.components.get(i).getAsJsonObject());
    }
    Identifier outputItem = Identifier.tryParse(recipeJson.outputItem);
    Item output = Registry.ITEM.getOrEmpty(outputItem).get();
    if (output == null) {
      throw new JsonSyntaxException("unknown output item");
    }
    return new ToolTableRecipe(id, components, output);
  }

  @Override
  public ToolTableRecipe read(Identifier id, PacketByteBuf buf) {
    Component[] components = new Component[9];
    for (int i = 0; i < 9; ++i) {
      components[i] = Component.read(buf);
    }
    Item output = Registry.ITEM.getOrEmpty(buf.readIdentifier()).get();
    if (output == null) {
      throw new JsonSyntaxException("unknown output item");
    }
    return new ToolTableRecipe(id, components, output);
  }

  @Override
  public void write(PacketByteBuf buf, ToolTableRecipe recipe) {
    for (Component c : recipe.getComponents())
      c.write(buf);
    buf.writeIdentifier(Registry.ITEM.getId(recipe.getOutputItem()));
  }

}
