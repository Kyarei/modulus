package flirora.suludom.recipe;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class BloodSmeltingRecipeSerializer
    implements RecipeSerializer<BloodSmeltingRecipe> {

  private BloodSmeltingRecipeSerializer() {
  }

  public static final BloodSmeltingRecipeSerializer INSTANCE =
      new BloodSmeltingRecipeSerializer();

  public static final Identifier ID =
      new Identifier("suludom", "blood_smelting_recipe");

  @Override
  public BloodSmeltingRecipe read(Identifier id, JsonObject json) {
    BloodSmeltingRecipeJsonFormat format =
        new Gson().fromJson(json, BloodSmeltingRecipeJsonFormat.class);
    ItemStack outputStack = new ItemStack(
        Registry.ITEM.get(Identifier.tryParse(format.result.item)),
        Math.max(1, format.result.count));
    ItemStack secondaryOutputStack = ItemStack.EMPTY;
    double secondaryOutputChance = 0.0;
    if (format.secondaryOutput != null) {
      secondaryOutputStack = new ItemStack(
          Registry.ITEM.get(Identifier.tryParse(format.secondaryOutput.item)),
          Math.max(1, format.secondaryOutput.count));
      secondaryOutputChance = format.secondaryOutput.chance;
    }
    return new BloodSmeltingRecipe(id, Ingredient.fromJson(format.input1),
        Ingredient.fromJson(format.input2), format.cookTime, outputStack,
        secondaryOutputStack, secondaryOutputChance);
  }

  @Override
  public BloodSmeltingRecipe read(Identifier id, PacketByteBuf buf) {
    Ingredient input1 = Ingredient.fromPacket(buf);
    Ingredient input2 = Ingredient.fromPacket(buf);
    int cookTime = buf.readInt();
    ItemStack output =
        new ItemStack(Registry.ITEM.get(buf.readIdentifier()), buf.readInt());
    ItemStack secondary =
        new ItemStack(Registry.ITEM.get(buf.readIdentifier()), buf.readInt());
    double secondaryChance = buf.readDouble();
    return new BloodSmeltingRecipe(id, input1, input2, cookTime, output,
        secondary, secondaryChance);
  }

  @Override
  public void write(PacketByteBuf buf, BloodSmeltingRecipe recipe) {
    recipe.getInput1().write(buf);
    recipe.getInput2().write(buf);
    buf.writeInt(recipe.getCookTime());
    ItemStack output = recipe.getOutput();
    buf.writeIdentifier(Registry.ITEM.getId(output.getItem()));
    buf.writeInt(output.getCount());
    ItemStack secondary = recipe.getSecondaryOutput();
    buf.writeIdentifier(Registry.ITEM.getId(secondary.getItem()));
    buf.writeInt(secondary.getCount());
    buf.writeDouble(recipe.getSecondaryOutputChance());
  }

}
