package flirora.suludom.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import flirora.suludom.item.tool.GenericGear;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class RecalculateCommand implements CommandRegistrationCallback {
  @Override public void register(CommandDispatcher<ServerCommandSource> dispatcher,
      boolean dedicated) {
    dispatcher.register(CommandManager.literal("surecalculate")
        .requires(source -> source.hasPermissionLevel(2))
        .executes(ctx -> recalculate(ctx.getSource())));
  }

  private static int recalculate(ServerCommandSource source)
      throws CommandSyntaxException {
    PlayerEntity self = source.getPlayer();
    ItemStack stack = self.getMainHandStack();
    Item item = stack.getItem();
    if (!(item instanceof GenericGear)) {
      self.sendMessage(
          new TranslatableText("command.surecalculate.failure.not_holding.gear")
              .formatted(Formatting.RED), false);
      return 1;
    }
    ((GenericGear) item).recalculateComposite(stack);
    self.sendMessage(new TranslatableText("command.surecalculate.success",
        stack.toHoverableText()), false);
    return Command.SINGLE_SUCCESS;
  }
}
