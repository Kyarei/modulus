package flirora.suludom.item;

import net.minecraft.item.Item;

public class BloodLapisItem extends Item implements EnchantingMaterial {

  public BloodLapisItem(Settings settings) {
    super(settings);
  }

  @Override
  public boolean canOverenchant() {
    return true;
  }

}
