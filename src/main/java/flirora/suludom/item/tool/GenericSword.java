package flirora.suludom.item.tool;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ToolItem;
import net.minecraft.text.Text;
import net.minecraft.world.World;

import java.util.List;
import java.util.UUID;

public class GenericSword extends SwordItem implements GenericTool {
  public static final UUID ATTACK_DAMAGE_MODIFIER_ID =
      Item.ATTACK_DAMAGE_MODIFIER_ID;
  public static final UUID ATTACK_SPEED_MODIFIER_ID =
      Item.ATTACK_SPEED_MODIFIER_ID;

  public GenericSword(Settings settings) {
    super(GenericTool.DUMMY_MATERIAL, 0, 0, settings);
  }

  @Override public float getBaseAttackSpeed() {
    return 1.6f;
  }

  @Override
  public float getDamageBonus() {
    return 0;
  }

  @Override
  public ToolItem getVanillaItemFor(ItemStack stack) {
    return new SwordItem(
        GenericGear.getCompositeMaterialFrom(stack).asToolMaterial(), 0, 0,
        new Item.Settings());
  }

  @Environment(EnvType.CLIENT)
  @Override
  public void appendTooltip(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    appendTooltipImpl(itemStack, world, tooltip, tooltipContext);
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  @Override
  public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
    float speed = super.getMiningSpeedMultiplier(stack, state);
    return (speed == 1.0f) ?
        1.0f :
        GenericGear.getCompositeMaterialFrom(stack).getMiningSpeed();
  }

  @Override public boolean canRepair(ItemStack stack, ItemStack ingredient) {
    return canRepairImpl(stack, ingredient);
  }

}
