package flirora.suludom.item.tool;

import java.util.List;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.text.Text;
import net.minecraft.world.World;

public class GenericAxe extends AxeItem implements GenericTool {

  public GenericAxe(Settings settings) {
    super(GenericTool.DUMMY_MATERIAL, 0, 0, settings);
  }

  protected GenericAxe(ItemStack stack) {
    super(GenericGear.getCompositeMaterialFrom(stack).asToolMaterial(), 0.0f,
        0.0f, new Item.Settings());
  }

  @Override
  public float getBaseAttackSpeed() {
    return 0.9f;
  }

  @Override
  public float getDamageBonus() {
    return 3.0f;
  }

  @Override
  public ToolItem getVanillaItemFor(ItemStack stack) {
    return new GenericAxe(stack);
  }

  @Environment(EnvType.CLIENT)
  @Override
  public void appendTooltip(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    appendTooltipImpl(itemStack, world, tooltip, tooltipContext);
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  @Override
  public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
    float speed = super.getMiningSpeedMultiplier(stack, state);
    return (speed == 1.0f) ? 1.0f
        : GenericGear.getCompositeMaterialFrom(stack).getMiningSpeed();
  }

  @Override
  public boolean canRepair(ItemStack stack, ItemStack ingredient) {
    return canRepairImpl(stack, ingredient);
  }

}
