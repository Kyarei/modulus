package flirora.suludom.item.tool.render;

import flirora.suludom.item.tool.GenericArmor;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public abstract class GenericArmorFeatureRenderer<T extends LivingEntity, M extends BipedEntityModel<T>, A extends BipedEntityModel<T>>
    extends FeatureRenderer<T, M> {
  protected final A modelLeggings;
  protected final A modelBody;
  private static final Identifier ARMOR_TEXTURE_1 =
      new Identifier("suludom", "textures/models/armor/generic_layer_1.png");
  private static final Identifier ARMOR_TEXTURE_2 =
      new Identifier("suludom", "textures/models/armor/generic_layer_2.png");

  protected GenericArmorFeatureRenderer(
      FeatureRendererContext<T, M> featureRendererContext, A modelLeggings,
      A modelBody) {
    super(featureRendererContext);
    this.modelLeggings = modelLeggings;
    this.modelBody = modelBody;
  }

  @Override public void render(MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, int light, T livingEntity,
      float limbAngle, float limbDistance, float tickDelta, float age,
      float yaw, float pitch) {
    this.renderArmor(matrixStack, vertexConsumerProvider, livingEntity,
        limbAngle, limbDistance, tickDelta, age, yaw, pitch,
        EquipmentSlot.CHEST, light);
    this.renderArmor(matrixStack, vertexConsumerProvider, livingEntity,
        limbAngle, limbDistance, tickDelta, age, yaw, pitch, EquipmentSlot.LEGS,
        light);
    this.renderArmor(matrixStack, vertexConsumerProvider, livingEntity,
        limbAngle, limbDistance, tickDelta, age, yaw, pitch, EquipmentSlot.FEET,
        light);
    this.renderArmor(matrixStack, vertexConsumerProvider, livingEntity,
        limbAngle, limbDistance, tickDelta, age, yaw, pitch, EquipmentSlot.HEAD,
        light);
  }

  private void renderArmor(MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, T livingEntity,
      float limbAngle, float limbDistance, float tickDelta, float aage,
      float yaw, float pitch, EquipmentSlot equipmentSlot, int light) {
    ItemStack equippedItem = livingEntity.getEquippedStack(equipmentSlot);
    if (equippedItem.getItem() instanceof GenericArmor) {
      GenericArmor armorItem = (GenericArmor) equippedItem.getItem();
      if (armorItem.getSlotType() == equipmentSlot) {
        A bipedEntityModel = this.getArmor(equipmentSlot);
        this.getContextModel().setAttributes(bipedEntityModel);
        bipedEntityModel.animateModel(livingEntity, limbAngle, limbDistance,
            tickDelta);
        this.setVisible(bipedEntityModel, equipmentSlot);
        bipedEntityModel.setAngles(livingEntity, limbAngle, limbDistance, aage,
            yaw, pitch);
        boolean lowerParts = this.isLegs(equipmentSlot);
        boolean renderGlint = equippedItem.hasGlint();
        int color = armorItem.getColor(equippedItem);
        float r = (float) (color >> 16 & 255) / 255.0F;
        float g = (float) (color >> 8 & 255) / 255.0F;
        float b = (float) (color & 255) / 255.0F;
        this.renderArmorParts(matrixStack, vertexConsumerProvider, light,
            armorItem, renderGlint, bipedEntityModel, lowerParts, r, g, b);
      }
    }
  }

  private void renderArmorParts(MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, int light,
      GenericArmor armorItem, boolean renderGlint, A bipedEntityModel,
      boolean lowerParts, float r, float g, float b) {
    Identifier armorTexture = lowerParts ? ARMOR_TEXTURE_2 : ARMOR_TEXTURE_1;
    VertexConsumer vertexConsumer = ItemRenderer.getArmorVertexConsumer(
        vertexConsumerProvider, RenderLayer.getEntityCutoutNoCull(armorTexture),
        false, renderGlint);
    bipedEntityModel.render(matrixStack, vertexConsumer, light,
        OverlayTexture.DEFAULT_UV, r, g, b, 1.0F);
  }

  public A getArmor(EquipmentSlot equipmentSlot) {
    return this.isLegs(equipmentSlot) ? this.modelLeggings : this.modelBody;
  }

  private boolean isLegs(EquipmentSlot equipmentSlot) {
    return equipmentSlot == EquipmentSlot.LEGS;
  }

  protected abstract void setVisible(A bipedModel, EquipmentSlot equipmentSlot);

  protected abstract void setInvisible(A bipedEntityModel);
}
