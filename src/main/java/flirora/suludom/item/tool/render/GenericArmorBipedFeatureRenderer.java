package flirora.suludom.item.tool.render;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;

@Environment(EnvType.CLIENT)
public class GenericArmorBipedFeatureRenderer<T extends LivingEntity, M extends BipedEntityModel<T>, A extends BipedEntityModel<T>>
    extends GenericArmorFeatureRenderer<T, M, A> {
  public GenericArmorBipedFeatureRenderer(
      FeatureRendererContext<T, M> featureRendererContext, A modelLeggings,
      A modelBody) {
    super(featureRendererContext, modelLeggings, modelBody);
  }

  protected void setVisible(A bipedModel, EquipmentSlot equipmentSlot) {
    this.setInvisible(bipedModel);
    switch (equipmentSlot) {
    case HEAD:
      bipedModel.head.visible = true;
      bipedModel.helmet.visible = true;
      break;
    case CHEST:
      bipedModel.torso.visible = true;
      bipedModel.rightArm.visible = true;
      bipedModel.leftArm.visible = true;
      break;
    case LEGS:
      bipedModel.torso.visible = true;
      bipedModel.rightLeg.visible = true;
      bipedModel.leftLeg.visible = true;
      break;
    case FEET:
      bipedModel.rightLeg.visible = true;
      bipedModel.leftLeg.visible = true;
    default:
      // nothing
    }

  }

  protected void setInvisible(A bipedEntityModel) {
    bipedEntityModel.setVisible(false);
  }
}
