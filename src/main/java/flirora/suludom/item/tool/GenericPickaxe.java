package flirora.suludom.item.tool;

import java.util.List;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolItem;
import net.minecraft.text.Text;
import net.minecraft.world.World;

public class GenericPickaxe extends PickaxeItem implements GenericTool {

  public GenericPickaxe(Settings settings) {
    super(GenericTool.DUMMY_MATERIAL, 0, 0, settings);
  }

  protected GenericPickaxe(ItemStack stack) {
    super(GenericGear.getCompositeMaterialFrom(stack).asToolMaterial(), 0, 0.0f,
        new Item.Settings());
  }

  @Override
  public float getBaseAttackSpeed() {
    return 1.2f;
  }

  @Override
  public float getDamageBonus() {
    return -2.0f;
  }

  @Override
  public ToolItem getVanillaItemFor(ItemStack stack) {
    return new GenericPickaxe(stack);
  }

  @Environment(EnvType.CLIENT)
  @Override
  public void appendTooltip(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    appendTooltipImpl(itemStack, world, tooltip, tooltipContext);
  }

  @Override
  public boolean canRepair(ItemStack stack, ItemStack ingredient) {
    return canRepairImpl(stack, ingredient);
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  @Override
  public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
    float speed = super.getMiningSpeedMultiplier(stack, state);
    return (speed == 1.0f) ? 1.0f
        : GenericGear.getCompositeMaterialFrom(stack).getMiningSpeed();
  }

}
