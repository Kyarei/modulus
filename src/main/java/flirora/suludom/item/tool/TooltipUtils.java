package flirora.suludom.item.tool;

import flirora.suludom.material.Material;
import flirora.suludom.material.enchantment.EnchantmentAffinities;
import it.unimi.dsi.fastutil.objects.Object2IntLinkedOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap.Entry;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TooltipUtils {
  public static String getMaterialName(List<? extends Material> materials) {
    String delimiter = I18n.translate("material.separator");
    Object2IntLinkedOpenHashMap<Material> frequencies =
        new Object2IntLinkedOpenHashMap<>();
    frequencies.defaultReturnValue(0);
    for (Material tmat : materials) {
      frequencies.addTo(tmat, 1);
    }
    ArrayList<Entry<Material>> sorted =
        new ArrayList<>(frequencies.object2IntEntrySet());
    // Sort in descending order
    sorted
        .sort((p1, p2) -> Integer.compare(p2.getIntValue(), p1.getIntValue()));
    return sorted.stream()
        .map((e) -> I18n.translate("material." + e.getKey().getId()))
        .collect(Collectors.joining(delimiter));
  }

  public static Formatting getMiningLevelColor(int level) {
    switch (level) {
    case 0:
      return Formatting.WHITE;
    case 1:
      return Formatting.GREEN;
    case 2:
      return Formatting.YELLOW;
    case 3:
      return Formatting.BLUE;
    case 4:
      return Formatting.GOLD;
    case 5:
      return Formatting.AQUA;
    default:
      return Formatting.LIGHT_PURPLE;
    }
  }

  public static Formatting getSynergyColor(float synergy) {
    return (Math.abs(synergy - 1.0f) < 0.03) ? Formatting.YELLOW
        : synergy > 1.0f ? Formatting.GREEN : Formatting.RED;
  }

  public static Text formatEnchantmentAffinityWeight(int weight) {
    if (weight == 0) {
      return new LiteralText("0").formatted(Formatting.YELLOW);
    }
    Formatting color = weight > 0 ? Formatting.GREEN : Formatting.RED;
    String sign = weight > 0 ? "+" : "-";
    // we want round-to-zero
    String fractional =
        weight % 16 == 0 ? "" : String.format("%s/16", Math.abs(weight % 16));
    String fractionalSpace = weight % 16 == 0 ? "" : " ";
    if (Math.abs(weight) < 16) {
      return new LiteralText(String.format("%s%s", sign, fractional))
          .formatted(color);
    }
    return new LiteralText(String.format("%s%d%s%s", sign, weight / 16,
        fractionalSpace, fractional)).formatted(color);
  }

  public static void formatEnchantmentAffinities(List<Text> tooltip,
      EnchantmentAffinities affinities, ItemStack stack) {
    for (Entry<Enchantment> entry : affinities.getAffinities()
        .object2IntEntrySet()) {
      Enchantment enchantment = entry.getKey();
      int weight = entry.getIntValue();
      if (!enchantment.isAcceptableItem(stack))
        continue;
      String enchantmentKey = enchantment.getTranslationKey();
      if (enchantment.isTreasure()) {
        tooltip.add(
            new TranslatableText("tooltip.suludom.treasure_enchant_unlocked",
                new TranslatableText(enchantmentKey))
                    .formatted(Formatting.LIGHT_PURPLE)
                    .formatted(Formatting.ITALIC));
        if (weight == 0)
          continue;
      }
      tooltip.add(new TranslatableText(enchantmentKey).append(" ")
          .append(formatEnchantmentAffinityWeight(weight)));
    }
  }

  public static Text pressShiftText() {
    return new TranslatableText("tooltip.suludom.shift_for_enchant_info")
        .formatted(Formatting.AQUA).formatted(Formatting.UNDERLINE);
  }

  public static Text pressCtrlText() {
    return new TranslatableText("tooltip.suludom.ctrl_for_recipe_results")
        .formatted(Formatting.AQUA).formatted(Formatting.UNDERLINE);
  }

  @Environment(EnvType.CLIENT)
  public static void addDescriptionTooltips(List<Text> tooltip,
      String... keys) {
    if (Screen.hasShiftDown()) {
      for (String key : keys) {
        tooltip.add(new TranslatableText(key).formatted(Formatting.DARK_AQUA,
            Formatting.ITALIC));
      }
    } else {
      tooltip.add(new TranslatableText("tooltip.suludom.shift_for_item_info")
          .formatted(Formatting.AQUA).formatted(Formatting.UNDERLINE));
    }
  }

  public static final DecimalFormat ONE_DECIMAL = new DecimalFormat("0.0");

  public static String roundTo1Decimal(double x) {
    return ONE_DECIMAL.format(x);
  }

  public static Text bonusText(String key, int quantity) {
    String rhs = quantity >= 0 ? "+" + quantity : "-" + (-quantity);
    return new TranslatableText(key, rhs);
  }

  public static Text bonusText(String key, double quantity) {
    String rhs = quantity >= 0 ? "+" + roundTo1Decimal(quantity)
        : "-" + roundTo1Decimal(-quantity);
    return new TranslatableText(key, rhs);
  }
}
