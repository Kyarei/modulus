package flirora.suludom.item.tool;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.Material;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import java.util.List;

public interface GenericTool extends GenericGear {
  public static final ToolMaterial DUMMY_MATERIAL =
      new CompositeMaterial(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, 1)
          .asToolMaterial();

  public static final double HAND_ATTACK_SPEED =
      EntityAttributes.GENERIC_ATTACK_SPEED.getDefaultValue();
  public static final double HAND_ATTACK_DAMAGE = 1.0;

  float getBaseAttackSpeed();

  float getDamageBonus();

  @Override
  public default List<? extends Material> primaryMaterialList(
      RecipeResult result) {
    return result.getToolMaterials();
  }

  public default boolean canMine(BlockState state, World world, BlockPos pos,
      PlayerEntity miner) {
    return !miner.isCreative();
  }

  public default double getDamageMultiplier() {
    return 1.0f;
  }

  public default double getTotalMeleeDamage(CompositeMaterial composite) {
    return composite
        .getAttackDamage() * getDamageMultiplier() + getDamageBonus();
  }

  public default double getTotalAttackSpeed(CompositeMaterial composite) {
    return getBaseAttackSpeed() + composite.getAttackSpeedBonus();
  }

  @Override
  default Multimap<EntityAttribute, EntityAttributeModifier> getStackModifiers(
      ItemStack stack, EquipmentSlot slot) {
    ImmutableMultimap.Builder<EntityAttribute, EntityAttributeModifier>
        builder = ImmutableMultimap.builder();
    CompositeMaterial composite = GenericGear.getCompositeMaterialFrom(stack);
    if (composite == null)
      return ImmutableMultimap.of();
    if (slot == EquipmentSlot.MAINHAND) {
      EntityAttributeModifier damageModifier =
          new EntityAttributeModifier(GenericSword.ATTACK_DAMAGE_MODIFIER_ID,
              "Tool modifier",
              getTotalMeleeDamage(composite) - HAND_ATTACK_DAMAGE,
              EntityAttributeModifier.Operation.ADDITION);
      EntityAttributeModifier speedModifier =
          new EntityAttributeModifier(GenericSword.ATTACK_SPEED_MODIFIER_ID,
              "Tool modifier",
              getTotalAttackSpeed(composite) - HAND_ATTACK_SPEED,
              EntityAttributeModifier.Operation.ADDITION);
      builder.put(EntityAttributes.GENERIC_ATTACK_DAMAGE, damageModifier);
      builder.put(EntityAttributes.GENERIC_ATTACK_SPEED, speedModifier);
    }
    return builder.build();
  }

  ToolItem getVanillaItemFor(ItemStack stack);

  public default boolean isVanillaBlockEffective(ItemStack stack,
      BlockState state) {
    return getVanillaItemFor(stack).isEffectiveOn(state);
  }

  @Environment(EnvType.CLIENT)
  public default void appendTooltipImpl(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    RecipeResult recipeResult = GenericGear.getRecipeResultFrom(itemStack);
    CompositeMaterial compositeMaterial =
        GenericGear.getCompositeMaterialFrom(itemStack);
    if (recipeResult == null || compositeMaterial == null) {
      tooltip.add(new TranslatableText("tooltip.invalid_modular_tool")
          .formatted(Formatting.RED));
      return;
    }
    if (recipeResult.isSteaky()) {
      tooltip
          .add(new TranslatableText("tooltip.suludom.top_secret_easter_egg_0"));
    }
    compositeMaterial.appendTooltip(tooltip,
        itemStack.getMaxDamage() - itemStack.getDamage(), this, itemStack,
        recipeResult.getAugmentMaterials().size());
    recipeResult.appendTooltip(tooltip);
  }

  public default boolean canRepairImpl(ItemStack stack, ItemStack ingredient) {
    Item repairMaterial = ingredient.getItem();
    RecipeResult result = GenericGear.getRecipeResultFrom(stack);
    return result.getToolMaterials().stream()
        .anyMatch((mat) -> Registry.ITEM.get(mat.getId()) == repairMaterial);
  }

}
