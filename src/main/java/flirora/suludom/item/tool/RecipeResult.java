package flirora.suludom.item.tool;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.SuludomRegistry;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.HasMaterialCategory;
import flirora.suludom.material.Material;
import flirora.suludom.material.MaterialCategory;
import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.augment.AugmentMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class RecipeResult {
  private static final Logger logger = LogManager.getLogger();

  private ArrayList<ToolMaterial> toolMaterials;
  private ArrayList<RodMaterial> rodMaterials;
  private ArrayList<ArmorMaterial> armorMaterials;
  private ArrayList<AugmentMaterial> augmentMaterials;

  public RecipeResult() {
    toolMaterials = new ArrayList<>();
    rodMaterials = new ArrayList<>();
    armorMaterials = new ArrayList<>();
    augmentMaterials = new ArrayList<>();
  }

  public ArrayList<ToolMaterial> getToolMaterials() {
    return toolMaterials;
  }

  public ArrayList<RodMaterial> getRodMaterials() {
    return rodMaterials;
  }

  public ArrayList<ArmorMaterial> getArmorMaterials() {
    return armorMaterials;
  }

  public ArrayList<AugmentMaterial> getAugmentMaterials() {
    return augmentMaterials;
  }

  public void addToolMaterial(ToolMaterial mat) {
    toolMaterials.add(mat);
  }

  public void addRodMaterial(RodMaterial mat) {
    rodMaterials.add(mat);
  }

  public void addArmorMaterial(ArmorMaterial mat) {
    armorMaterials.add(mat);
  }

  public void addAugmentMaterial(AugmentMaterial mat) {
    augmentMaterials.add(mat);
  }

  public void forEachMaterial(Consumer<Material> callback) {
    toolMaterials.forEach(callback);
    rodMaterials.forEach(callback);
    armorMaterials.forEach(callback);
    augmentMaterials.forEach(callback);
  }

  private <M extends Material> void saveMaterials(CompoundTag tag, String key,
      ArrayList<M> materials) {
    ListTag value = new ListTag();
    tag.put(key, value);
    for (M m : materials) {
      String name = m.getId().toString();
      value.add(StringTag.of(name));
    }
  }

  public CompoundTag toTag(CompoundTag tag) {
    saveMaterials(tag, "ToolMaterials", toolMaterials);
    saveMaterials(tag, "RodMaterials", rodMaterials);
    saveMaterials(tag, "ArmorMaterials", armorMaterials);
    saveMaterials(tag, "AugmentMaterials", augmentMaterials);
    return tag;
  }

  private static <M extends Material> void readFromTag(CompoundTag root,
      String key, Registry<M> registry, ArrayList<M> destination) {
    try {
      ListTag tag = root.getList(key, 8);
      if (tag == null)
        return;
      for (Tag materialTag : tag) {
        Identifier id =
            Identifier.tryParse(materialTag.asString());
        if (id == null) {
          logger.warn("Invalid ID: " + materialTag.asString());
          continue;
        }
        M tm = registry.get(id);
        if (tm == null) {
          logger.warn("Unknown material: " + id.toString());
        }
        destination.add(tm);
      }
    } catch (ClassCastException ex) {
      logger.warn("NBT read failed for tool materials", ex);
    }
  }

  public static RecipeResult fromTag(CompoundTag tag) {
    // For fuck's sake.
    RecipeResult res = new RecipeResult();
    if (tag == null) {
      logger.warn("Tag is null, ignoring");
      return res;
    }
    readFromTag(tag, "ToolMaterials", SuludomRegistry.getToolMaterialRegistry(),
        res.toolMaterials);
    readFromTag(tag, "RodMaterials", SuludomRegistry.getRodMaterialRegistry(),
        res.rodMaterials);
    readFromTag(tag, "ArmorMaterials",
        SuludomRegistry.getArmorMaterialRegistry(), res.armorMaterials);
    readFromTag(tag, "AugmentMaterials",
        SuludomRegistry.getAugmentMaterialRegistry(), res.augmentMaterials);
    return res;
  }

  private static <M extends Material & HasMaterialCategory> void updateSynergySets(
      Set<Identifier> uniqueMaterials, Set<MaterialCategory> uniqueCategories,
      ArrayList<M> materials) {
    for (M m : materials) {
      uniqueMaterials.add(m.getId());
      uniqueCategories.add(m.getMaterialCategory());
    }
  }

  private float getSynergy() {
    Set<Identifier> uniqueMaterials = new HashSet<>();
    Set<MaterialCategory> uniqueCategories = new HashSet<>();
    updateSynergySets(uniqueMaterials, uniqueCategories, toolMaterials);
    updateSynergySets(uniqueMaterials, uniqueCategories, armorMaterials);
    // Mixing different materials makes the tool stronger,
    // but mixing different classes is no good
    return 1.0f + 0.05f * (uniqueMaterials.size() - 1)
        - 0.15f * (uniqueCategories.size() - 1);
  }

  // armorSlot is null for tool
  public CompositeMaterial makeCompositeMaterial(
      @Nullable EquipmentSlot armorSlot) {
    // Use maximum mining level; average everything else
    CompositeMaterial summed = new CompositeMaterial();
    int nMaterials = 0;
    for (ToolMaterial tm : toolMaterials) {
      summed = summed.merge(tm);
      ++nMaterials;
    }
    for (ArmorMaterial am : armorMaterials) {
      summed = summed.merge(am);
      ++nMaterials;
    }
    CompositeMaterial summed2 =
        armorSlot != null ? summed.scaleDurabilityFor(armorSlot) : summed;
    CompositeMaterial avg = summed2.scale(1.0f / nMaterials);
    for (RodMaterial rm : rodMaterials) {
      avg = avg.merge(rm);
    }
    avg = avg.applySynergy(getSynergy());
    for (AugmentMaterial am : augmentMaterials) {
      avg = avg.merge(am);
    }
    return avg;
  }

  @Override
  public String toString() {
    return "RecipeResult [toolMaterials=" + toolMaterials + ", rodMaterials="
        + rodMaterials + ", armorMaterials=" + armorMaterials
        + ", augmentMaterials=" + augmentMaterials + "]";
  }

  @Environment(EnvType.CLIENT)
  private <M extends Material> void appendTooltipForMaterials(
      List<Text> tooltip, ArrayList<M> materials, String id) {
    if (!materials.isEmpty()) {
      Object2IntOpenHashMap<Identifier> materialFrequencies =
          new Object2IntOpenHashMap<>();
      materialFrequencies.defaultReturnValue(0);
      for (M material : materials) {
        materialFrequencies.addTo(material.getId(), 1);
      }
      List<Object2IntMap.Entry<Identifier>> sortedFrequencies =
          new ArrayList<>(materialFrequencies.object2IntEntrySet());
      sortedFrequencies
          .sort((a, b) -> Integer.compare(b.getIntValue(), a.getIntValue()));
      tooltip.add(new TranslatableText("modular_component.suludom." + id)
          .formatted(Formatting.LIGHT_PURPLE).formatted(Formatting.BOLD));
      for (Object2IntMap.Entry<Identifier> entry : sortedFrequencies) {
        Identifier materialId = entry.getKey();
        int count = entry.getIntValue();
        tooltip.add(new TranslatableText("tooltip.suludom.bullet",
            Registry.ITEM.get(materialId).getName().copy().append(" x" + count)
                .formatted(Formatting.AQUA)).formatted(Formatting.RED));
      }
    }
  }

  @Environment(EnvType.CLIENT) public void appendTooltip(List<Text> tooltip) {
    if (Screen.hasControlDown()) {
      appendTooltipForMaterials(tooltip, toolMaterials, "tool_material");
      appendTooltipForMaterials(tooltip, rodMaterials, "rod_material");
      appendTooltipForMaterials(tooltip, armorMaterials, "armor_material");
      appendTooltipForMaterials(tooltip, augmentMaterials, "augment_material");
    } else {
      tooltip.add(TooltipUtils.pressCtrlText());
    }
  }

  private static final Identifier STEAK_ID =
      new Identifier("minecraft", "cooked_beef");

  public boolean isSteaky() {
    for (ToolMaterial tm : toolMaterials) {
      if (tm.getId().equals(STEAK_ID))
        return true;
    }
    for (ArmorMaterial am : armorMaterials) {
      if (am.getId().equals(STEAK_ID))
        return true;
    }
    return false;
  }
}
