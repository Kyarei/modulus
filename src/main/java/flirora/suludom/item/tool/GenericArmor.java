package flirora.suludom.item.tool;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import flirora.suludom.SuludomRegistry;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.Material;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.DyeableArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import java.util.List;
import java.util.UUID;

public class GenericArmor extends DyeableArmorItem implements GenericGear {
  public GenericArmor(EquipmentSlot slot, Settings settings) {
    super(DUMMY_MATERIAL, slot, settings);
  }

  public static final UUID[] MODIFIERS =
      new UUID[] { UUID.fromString("b14f017d-ce3c-471e-a66b-c8d7e611db08"),
          UUID.fromString("efb963e7-f319-4508-9a7a-b02f2700f491"),
          UUID.fromString("11e31b4b-db95-4650-bb6a-3acbdbcc3620"),
          UUID.fromString("e82f5c24-c942-4d55-85ae-54c8fe941831") };

  public static final ArmorMaterial DUMMY_MATERIAL =
      new CompositeMaterial(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, 1)
          .asArmorMaterial();

  @Override
  public List<? extends Material> primaryMaterialList(RecipeResult result) {
    return result.getArmorMaterials();
  }

  @Override
  public Multimap<EntityAttribute, EntityAttributeModifier> getStackModifiers(
      ItemStack stack, EquipmentSlot slot) {
    ImmutableMultimap.Builder<EntityAttribute, EntityAttributeModifier>
        builder = ImmutableMultimap.builder();
    CompositeMaterial composite = GenericGear.getCompositeMaterialFrom(stack);
    if (composite == null)
      return ImmutableMultimap.of();

    if (slot == getSlotType()) {
      builder.put(EntityAttributes.GENERIC_ARMOR,
          new EntityAttributeModifier(MODIFIERS[slot.getEntitySlotId()],
              "Armor modifier",
              (double) composite.getProtectionAmountFloat(slot),
              EntityAttributeModifier.Operation.ADDITION));
      builder.put(EntityAttributes.GENERIC_ARMOR_TOUGHNESS,
          new EntityAttributeModifier(MODIFIERS[slot.getEntitySlotId()],
              "Armor toughness", (double) composite.getArmorToughness(),
              EntityAttributeModifier.Operation.ADDITION));
      builder.put(EntityAttributes.GENERIC_KNOCKBACK_RESISTANCE,
          new EntityAttributeModifier(MODIFIERS[slot.getEntitySlotId()],
              "Armor knockback resistance",
              (double) composite.getArmorToughness(),
              EntityAttributeModifier.Operation.ADDITION));
    }
    return builder.build();
  }

  @Override @Environment(EnvType.CLIENT)
  public void appendTooltip(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    RecipeResult recipeResult = GenericGear.getRecipeResultFrom(itemStack);
    CompositeMaterial compositeMaterial =
        GenericGear.getCompositeMaterialFrom(itemStack);
    if (recipeResult == null || compositeMaterial == null) {
      tooltip.add(new TranslatableText("tooltip.invalid_modular_tool")
          .formatted(Formatting.RED));
      return;
    }
    if (recipeResult.isSteaky()) {
      tooltip
          .add(new TranslatableText("tooltip.suludom.top_secret_easter_egg_0"));
    }
    compositeMaterial.appendTooltip(tooltip,
        itemStack.getMaxDamage() - itemStack.getDamage(), this, itemStack,
        recipeResult.getAugmentMaterials().size());
    recipeResult.appendTooltip(tooltip);
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  @Override
  public boolean hasColor(ItemStack stack) {
    return true;
  }

  @Override
  public int getColor(ItemStack stack) {
    CompoundTag recipeResultTag = stack.getSubTag("RecipeResult");
    if (recipeResultTag == null)
      return -1;
    Identifier id = Identifier
        .tryParse(recipeResultTag.getList("ArmorMaterials", 8).getString(0));
    flirora.suludom.material.armor.ArmorMaterial mat =
        SuludomRegistry.getArmorMaterialRegistry().get(id);
    return mat != null ? mat.getArmorColor() : -1;
  }

  @Override
  public void removeColor(ItemStack stack) {
  }

  @Override
  public void setColor(ItemStack stack, int color) {
  }

  @Override
  public boolean canRepair(ItemStack stack, ItemStack ingredient) {
    Item repairMaterial = ingredient.getItem();
    RecipeResult result = GenericGear.getRecipeResultFrom(stack);
    return result.getArmorMaterials().stream()
        .anyMatch((mat) -> Registry.ITEM.get(mat.getId()) == repairMaterial);
  }

}
