package flirora.suludom.item.tool;

import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.Material;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.List;

public interface GenericGear extends PerItemStackAttributes {

  List<? extends Material> primaryMaterialList(RecipeResult result);

  public static CompositeMaterial getCompositeMaterialFrom(
      ItemStack itemStack) {
    CompoundTag tag = itemStack.getTag();
    if (tag == null)
      return null;
    CompoundTag compositeMaterialTag = tag.getCompound("CompositeMaterial");
    if (compositeMaterialTag == null)
      return null;
    return CompositeMaterial.fromTag(compositeMaterialTag);
  }

  public static RecipeResult getRecipeResultFrom(ItemStack itemStack) {
    CompoundTag tag = itemStack.getTag();
    if (tag == null)
      return null;
    CompoundTag recipeResultTag = tag.getCompound("RecipeResult");
    if (recipeResultTag == null)
      return null;
    return RecipeResult.fromTag(recipeResultTag);
  }

  @Override
  public default int getStackMaxDamage(ItemStack stack) {
    CompositeMaterial composite = getCompositeMaterialFrom(stack);
    if (composite == null)
      return 1;
    return composite.getDurability();
  }

  @Override
  public default int getStackEnchantability(ItemStack stack) {
    CompositeMaterial composite = getCompositeMaterialFrom(stack);
    if (composite == null)
      return 0;
    return composite.getEnchantability();
  }

  @Override
  public default int getStackMiningLevel(ItemStack stack) {
    CompositeMaterial composite = getCompositeMaterialFrom(stack);
    if (composite == null)
      return 0;
    return composite.getMiningLevel();
  }

  public default boolean canRepair(ItemStack stack, ItemStack ingredient) {
    RecipeResult recipeResult = getRecipeResultFrom(stack);
    List<? extends Material> mats = primaryMaterialList(recipeResult);
    if (mats.isEmpty())
      return false;
    Item item = Registry.ITEM.get(mats.get(0).getId());
    return ingredient.getItem() == item;
  }

  @Override @Environment(EnvType.CLIENT)
  public default Text getStackName(ItemStack itemStack) {
    Identifier id = Registry.ITEM.getId(itemStack.getItem());
    RecipeResult recipeResult = getRecipeResultFrom(itemStack);
    if (recipeResult == null) {
      return new TranslatableText("modular_item." + id,
          new TranslatableText("tooltip.invalid_material"))
          .formatted(Formatting.RED);
    }
    return new TranslatableText("modular_item." + id,
        TooltipUtils.getMaterialName(primaryMaterialList(recipeResult)));
  }

  public default void recalculateComposite(ItemStack stack) {
    RecipeResult result = getRecipeResultFrom(stack);
    CompositeMaterial composite = result.makeCompositeMaterial(
        (this instanceof GenericArmor) ? ((GenericArmor) this).getSlotType()
            : null);
    stack.getOrCreateTag().put("CompositeMaterial",
        composite.toTag(new CompoundTag()));
  }

  @Override public default boolean isStackFireproof(ItemStack itemStack) {
    return getRecipeResultFrom(itemStack).getAugmentMaterials().stream()
        .anyMatch((mat) -> mat.getId()
            .equals(new Identifier("minecraft", "netherite_ingot")));
  }

  @Override public default float getStackBaseMiningSpeed(ItemStack stack) {
    return GenericGear.getCompositeMaterialFrom(stack).getMiningSpeed();
  }

}
