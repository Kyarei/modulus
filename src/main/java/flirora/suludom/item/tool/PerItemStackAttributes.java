package flirora.suludom.item.tool;

import com.google.common.collect.Multimap;
import net.fabricmc.fabric.api.tool.attribute.v1.DynamicAttributeTool;
import net.minecraft.block.BlockState;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tag.Tag;
import net.minecraft.text.Text;

public interface PerItemStackAttributes extends DynamicAttributeTool {
  // ItemStack is injected to take these into account

  int getStackMaxDamage(ItemStack stack);

  Multimap<EntityAttribute, EntityAttributeModifier> getStackModifiers(
      ItemStack stack, EquipmentSlot slot);

  Text getStackName(ItemStack stack);

  // ToolManager is injected to take this into account

  int getStackMiningLevel(ItemStack stack);

  // EnchantmentHelper is injected to take this into account

  int getStackEnchantability(ItemStack stack);

  boolean isStackFireproof(ItemStack stack);

  float getStackBaseMiningSpeed(ItemStack itemStack);

  @Override
  default int getMiningLevel(ItemStack itemStack, LivingEntity livingEntity) {
    return getStackMiningLevel(itemStack);
  }

  @Override
  default int getMiningLevel(Tag<Item> tag, BlockState state, ItemStack stack,
      LivingEntity user) {
    return getStackMiningLevel(stack);
  }

  @Override default float getMiningSpeedMultiplier(ItemStack itemStack,
      LivingEntity livingEntity) {
    return getStackBaseMiningSpeed(itemStack);
  }

  @Override
  default float getMiningSpeedMultiplier(Tag<Item> tag, BlockState state,
      ItemStack stack, LivingEntity user) {
    return getStackBaseMiningSpeed(stack);
  }

  @Override
  default Multimap<EntityAttribute, EntityAttributeModifier> getDynamicModifiers(
      EquipmentSlot slot, ItemStack stack, LivingEntity user) {
    return getStackModifiers(stack, slot);
  }
}
