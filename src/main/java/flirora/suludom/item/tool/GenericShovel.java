package flirora.suludom.item.tool;

import java.util.List;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.Items;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.ToolItem;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.world.World;

public class GenericShovel extends ShovelItem implements GenericTool {

  public GenericShovel(Settings settings) {
    super(GenericTool.DUMMY_MATERIAL, 0, 0, settings);
  }

  @Override
  public float getBaseAttackSpeed() {
    return 1.0f;
  }

  @Override
  public float getDamageBonus() {
    return -1.5f;
  }

  @Override
  public ActionResult useOnBlock(ItemUsageContext context) {
    return Items.IRON_SHOVEL.useOnBlock(context);
  }

  @Override
  public ToolItem getVanillaItemFor(ItemStack stack) {
    return new ShovelItem(
        GenericGear.getCompositeMaterialFrom(stack).asToolMaterial(), 0, 0,
        new Item.Settings());
  }

  @Environment(EnvType.CLIENT)
  @Override
  public void appendTooltip(ItemStack itemStack, World world,
      List<Text> tooltip, TooltipContext tooltipContext) {
    appendTooltipImpl(itemStack, world, tooltip, tooltipContext);
  }

  @Override
  public boolean isDamageable() {
    return true;
  }

  @Override
  public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
    float speed = super.getMiningSpeedMultiplier(stack, state);
    return (speed == 1.0f) ? 1.0f
        : GenericGear.getCompositeMaterialFrom(stack).getMiningSpeed();
  }

  @Override
  public boolean canRepair(ItemStack stack, ItemStack ingredient) {
    return canRepairImpl(stack, ingredient);
  }

}
