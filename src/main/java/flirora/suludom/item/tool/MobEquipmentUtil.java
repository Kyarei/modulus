package flirora.suludom.item.tool;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.SuludomMod;
import flirora.suludom.SuludomRegistry;
import flirora.suludom.material.CompositeMaterial;
import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;

public class MobEquipmentUtil {

  @Nullable
  private static Item getGenericEquipmentForSlot(EquipmentSlot slot) {
    switch (slot) {
    case HEAD:
      return SuludomMod.GENERIC_HELMET;
    case CHEST:
      return SuludomMod.GENERIC_CHESTPLATE;
    case LEGS:
      return SuludomMod.GENERIC_LEGGINGS;
    case FEET:
      return SuludomMod.GENERIC_BOOTS;
    default:
      return null;
    }
  }

  private static final int[] MATERIAL_COUNTS = { 5, 8, 7, 4 };
  private static final Identifier[] MATERIALS_BY_LEVEL =
      { null, new Identifier("minecraft", "gold_ingot"),
          new Identifier("minecraft", "iron_ingot"),
          new Identifier("minecraft", "iron_ingot"),
          new Identifier("minecraft", "diamond"), };

  private static RecipeResult getResultFor(EquipmentSlot slot, int level) {
    int materialCount = MATERIAL_COUNTS[slot.getEntitySlotId()];
    Identifier materialId =
        MATERIALS_BY_LEVEL[Math.min(MATERIALS_BY_LEVEL.length - 1, level)];
    ArmorMaterial armorMaterial =
        SuludomRegistry.getArmorMaterialRegistry().get(materialId);
    RecipeResult result = new RecipeResult();
    for (int i = 0; i < materialCount; ++i) {
      result.addArmorMaterial(armorMaterial);
    }
    return result;
  }

  /**
   * A version of MobEntity#getEquipmentForSlot that returns the appropriate
   * piece of Suludom armor.
   * 
   * @param slot           the slot to get an item for
   * @param equipmentLevel the equipment level (0 = leather to 4 = diamond)
   * @return an {@link ItemStack} for the equipped item
   */
  @Nullable
  public static ItemStack getEquipmentForSlot(EquipmentSlot slot,
      int equipmentLevel) {
    if (equipmentLevel == 0) {
      switch (slot) {
      case HEAD:
        return new ItemStack(Items.LEATHER_HELMET);
      case CHEST:
        return new ItemStack(Items.LEATHER_CHESTPLATE);
      case LEGS:
        return new ItemStack(Items.LEATHER_LEGGINGS);
      case FEET:
        return new ItemStack(Items.LEATHER_BOOTS);
      default:
        return null;
      }
    }
    Item genericItem = getGenericEquipmentForSlot(slot);
    if (genericItem == null)
      return null;
    ItemStack stack = new ItemStack(genericItem);
    RecipeResult result = getResultFor(slot, equipmentLevel);
    stack.putSubTag("RecipeResult", result.toTag(new CompoundTag()));
    CompositeMaterial composite = result.makeCompositeMaterial(slot);
    stack.putSubTag("CompositeMaterial", composite.toTag(new CompoundTag()));
    return stack;
  }

  public static <X extends Item & GenericGear> ItemStack createGenericTool(
      X genericItem, Identifier materialId, int materialCount, int rodCount) {
    ItemStack stack = new ItemStack(genericItem);
    RecipeResult result = new RecipeResult();
    ToolMaterial toolMaterial =
        SuludomRegistry.getToolMaterialRegistry().get(materialId);
    RodMaterial rodMaterial = SuludomRegistry.getRodMaterialRegistry()
        .get(new Identifier("minecraft", "stick"));
    for (int i = 0; i < materialCount; ++i) {
      result.addToolMaterial(toolMaterial);
    }
    for (int i = 0; i < rodCount; ++i) {
      result.addRodMaterial(rodMaterial);
    }
    stack.putSubTag("RecipeResult", result.toTag(new CompoundTag()));
    CompositeMaterial composite = result.makeCompositeMaterial(null);
    stack.putSubTag("CompositeMaterial", composite.toTag(new CompoundTag()));
    return stack;
  }

}
