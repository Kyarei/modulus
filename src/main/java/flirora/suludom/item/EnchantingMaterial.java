package flirora.suludom.item;

public interface EnchantingMaterial {
  default boolean canOverenchant() {
    return false;
  }
}
