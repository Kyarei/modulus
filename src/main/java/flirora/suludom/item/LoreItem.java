package flirora.suludom.item;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;

public class LoreItem extends Item {
  private Text lore;

  public LoreItem(Settings settings, MutableText lore) {
    super(settings);
    this.lore = lore.formatted(Formatting.AQUA);
  }

  @Environment(EnvType.CLIENT) @Override
  public void appendTooltip(ItemStack stack, World world, List<Text> tooltip,
      TooltipContext context) {
    super.appendTooltip(stack, world, tooltip, context);
    tooltip.add(lore);
  }
}
