package flirora.suludom;

import flirora.suludom.block.AugmentTableBlock;
import flirora.suludom.block.BloodFurnaceBlock;
import flirora.suludom.block.ScrappingTableBlock;
import flirora.suludom.block.ToolTableBlock;
import flirora.suludom.block.gui.AugmentTableController;
import flirora.suludom.block.gui.AugmentTableScreen;
import flirora.suludom.block.gui.BloodFurnaceController;
import flirora.suludom.block.gui.BloodFurnaceScreen;
import flirora.suludom.block.gui.ScrappingTableController;
import flirora.suludom.block.gui.ScrappingTableScreen;
import flirora.suludom.block.gui.ToolTableController;
import flirora.suludom.block.gui.ToolTableScreen;
import flirora.suludom.material.armor.ArmorMaterial;
import flirora.suludom.material.rod.RodMaterial;
import flirora.suludom.material.tool.ToolMaterial;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.client.screen.ScreenProviderRegistry;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class SuludomModClient implements ClientModInitializer {

  private static int getItemLayerColorForTool(ItemStack stack, int layer) {
    CompoundTag tag = stack.getTag();
    if (tag == null)
      return -1;
    CompoundTag recipeResultTag = tag.getCompound("RecipeResult");
    if (recipeResultTag == null)
      return -1;
    if (layer == 1) {
      Identifier id = Identifier
          .tryParse(recipeResultTag.getList("ToolMaterials", 8).getString(0));
      ToolMaterial mat = SuludomRegistry.getToolMaterialRegistry().get(id);
      return mat != null ? mat.getToolHeadColor() : -1;
    } else if (layer == 0) {
      Identifier id = Identifier
          .tryParse(recipeResultTag.getList("RodMaterials", 8).getString(0));
      RodMaterial mat = SuludomRegistry.getRodMaterialRegistry().get(id);
      return mat != null ? mat.getRodColor() : -1;
    }
    return -1;
  }

  private static int getItemLayerColorForArmor(ItemStack stack, int layer) {
    CompoundTag tag = stack.getTag();
    if (tag == null)
      return -1;
    CompoundTag recipeResultTag = tag.getCompound("RecipeResult");
    if (recipeResultTag == null)
      return -1;
    if (layer == 0) {
      Identifier id = Identifier
          .tryParse(recipeResultTag.getList("ArmorMaterials", 8).getString(0));
      ArmorMaterial mat = SuludomRegistry.getArmorMaterialRegistry().get(id);
      return mat != null ? mat.getArmorColor() : -1;
    }
    return -1;
  }

//  @SuppressWarnings("unchecked")
//  private static <L extends LivingEntity, M extends BipedEntityModel<L>, A extends BipedEntityModel<L>> void patchEntityRendering(
//      LivingEntityRenderer<L, M> renderer,
//      Function<Float, A> entityModelCallback) {
//    ((LivingEntityRendererAccessor<L, M>) renderer)
//        .addFeature(new GenericArmorBipedFeatureRenderer<L, M, A>(renderer,
//            entityModelCallback.apply(0.5f), entityModelCallback.apply(1.0f)));
//  }
//
//  private static void patchEntityRendering() {
//    EntityRenderDispatcherAccessor dispatcher =
//        (EntityRenderDispatcherAccessor) MinecraftClient.getInstance()
//            .getEntityRenderManager();
//    if (dispatcher == null)
//      return;
//    System.err.println("hi");
//    patchEntityRendering(dispatcher.getPlayerRenderer(),
//        (off) -> new BipedEntityModel<AbstractClientPlayerEntity>(off));
//  }

  @Override
  public void onInitializeClient() {
    // Register color providers

    ColorProviderRegistry.ITEM.register(
        SuludomModClient::getItemLayerColorForTool, SuludomMod.GENERIC_PICKAXE,
        SuludomMod.GENERIC_AXE, SuludomMod.GENERIC_SHOVEL,
        SuludomMod.GENERIC_HOE, SuludomMod.GENERIC_SWORD);
    ColorProviderRegistry.ITEM.register(
        SuludomModClient::getItemLayerColorForArmor, SuludomMod.GENERIC_HELMET,
        SuludomMod.GENERIC_CHESTPLATE, SuludomMod.GENERIC_LEGGINGS,
        SuludomMod.GENERIC_BOOTS);

    // Register GUIs

    ScreenProviderRegistry.INSTANCE.registerFactory(ToolTableBlock.ID,
        (syncId, identifier, player, buf) -> new ToolTableScreen(
            new ToolTableController(syncId, player.inventory,
                ScreenHandlerContext.create(player.world, buf.readBlockPos())),
            player));
    ScreenProviderRegistry.INSTANCE.registerFactory(AugmentTableBlock.ID,
        (syncId, identifier, player, buf) -> new AugmentTableScreen(
            new AugmentTableController(syncId, player.inventory,
                ScreenHandlerContext.create(player.world, buf.readBlockPos())),
            player));
    ScreenProviderRegistry.INSTANCE.registerFactory(ScrappingTableBlock.ID,
        (syncId, identifier, player, buf) -> new ScrappingTableScreen(
            new ScrappingTableController(syncId, player.inventory,
                ScreenHandlerContext.create(player.world, buf.readBlockPos())),
            player));
    ScreenProviderRegistry.INSTANCE.registerFactory(BloodFurnaceBlock.ID,
        (syncId, identifier, player, buf) -> new BloodFurnaceScreen(
            new BloodFurnaceController(syncId, player.inventory,
                ScreenHandlerContext.create(player.world, buf.readBlockPos())),
            player));

    // Register packets

    ClientSidePacketRegistry.INSTANCE.register(
        SuludomMod.MATERIAL_DATA_PACKET_ID,
        (packetContext, attachedData) -> SuludomMod.getMaterialManager()
            .loadDataFromNetwork(packetContext, attachedData));
  }

}
