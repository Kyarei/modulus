package flirora.suludom;

import io.github.cottonmc.libcd.api.CDSyntaxError;
import io.github.cottonmc.libcd.api.LibCDInitializer;
import io.github.cottonmc.libcd.api.condition.ConditionManager;
import io.github.cottonmc.libcd.api.tweaker.TweakerManager;
import net.minecraft.util.Identifier;

public class SuludomCDContent implements LibCDInitializer {

  public SuludomCDContent() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public void initTweakers(TweakerManager manager) {
    // TODO Auto-generated method stub

  }

  @Override
  public void initConditions(ConditionManager manager) {
    manager.registerCondition(
        new Identifier("suludom", "disable_vanilla_tools"), value -> {
          if (value instanceof Boolean)
            return ((Boolean) value) == SuludomMod.config.disableVanillaTools;
          throw new CDSyntaxError(
              "disable_vanilla_tools must accept a Boolean");
        });
    manager.registerCondition(new Identifier("suludom", "tweak_loot_tables"),
        value -> {
          if (value instanceof Boolean)
            return ((Boolean) value) == SuludomMod.config.tweakLootTables;
          throw new CDSyntaxError("tweak_loot_tables must accept a Boolean");
        });
  }

}
