package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.item.tool.GenericGear;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.Property;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;

@Mixin(AnvilScreenHandler.class)
public abstract class MixinAnvilScreenHandler extends ForgingScreenHandler {

  public MixinAnvilScreenHandler(ScreenHandlerType<?> containerType, int i,
      PlayerInventory playerInventory, ScreenHandlerContext blockContext) {
    super(containerType, i, playerInventory, blockContext);
    // TODO Auto-generated constructor stub
  }

  @Shadow
  @Final
  private Property levelCost;

  @Inject(method = "updateResult", at = @At("HEAD"), cancellable = true)
  private void onUpdateResult(CallbackInfo ci) {
    // Never allow combining two GenericGears. We might figure out conditions
    // where it should be allowed later on.
    ItemStack stack2 = this.input.getStack(1);
    if (stack2.getItem() instanceof GenericGear) {
      this.output.setStack(0, ItemStack.EMPTY);
      this.levelCost.set(0);
      ci.cancel();
    }
  }
}
