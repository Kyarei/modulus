package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.item.tool.GenericArmor;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.feature.HeadFeatureRenderer;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.render.entity.model.ModelWithHead;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;

@Mixin(HeadFeatureRenderer.class)
public abstract class MixinHeadFeatureRenderer<T extends LivingEntity, M extends EntityModel<T> & ModelWithHead>
    extends FeatureRenderer<T, M> {

  public MixinHeadFeatureRenderer(FeatureRendererContext<T, M> context) {
    super(context);
  }

  @Inject(method = "render", at = @At("HEAD"), cancellable = true)
  public void onRender(MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, int i, T livingEntity,
      float f, float g, float h, float j, float k, float l, CallbackInfo ci) {
    ItemStack itemStack = livingEntity.getEquippedStack(EquipmentSlot.HEAD);
    // Prevent HeadFeatureRenderer from rendering GenericArmors
    if (itemStack.getItem() instanceof GenericArmor)
      ci.cancel();
  }

}
