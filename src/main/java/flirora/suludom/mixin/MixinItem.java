package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import flirora.suludom.block.EnchantableBlock;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;

@Mixin(Item.class)
public abstract class MixinItem implements ItemConvertible {
  @Inject(method = "isEnchantable", at = @At("RETURN"), cancellable = true)
  public void onIsEnchantable(ItemStack stack,
      CallbackInfoReturnable<Boolean> cir) {
    if (!cir.getReturnValueZ()) {
      if ((Item) (Object) this instanceof BlockItem) {
        Block block = ((BlockItem) (Object) this).getBlock();
        if (block instanceof EnchantableBlock) {
          cir.setReturnValue(true);
          cir.cancel();
          return;
        }
      }
    }
  }
}
