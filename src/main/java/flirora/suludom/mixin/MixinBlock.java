package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;

import net.minecraft.block.Block;
import net.minecraft.item.ItemConvertible;

@Mixin(Block.class)
public abstract class MixinBlock implements ItemConvertible {
}
