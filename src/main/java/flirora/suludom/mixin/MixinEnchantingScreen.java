package flirora.suludom.mixin;

import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.suludom.enchanting.EnchantingTableContainerTraits;
import flirora.suludom.item.EnchantingMaterial;
import net.minecraft.client.gui.screen.ingame.EnchantmentScreen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.screen.EnchantmentScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

@Mixin(EnchantmentScreen.class)
public abstract class MixinEnchantingScreen
    extends HandledScreen<EnchantmentScreenHandler> {
  public MixinEnchantingScreen(EnchantmentScreenHandler container,
      PlayerInventory playerInventory, Text name) {
    super(container, playerInventory, name);
  }

  private int storedLevel;

  @ModifyConstant(
      method = "render",
      constant = @Constant(stringValue = "container.enchant.lapis.one")
  )
  private String onGetLapisOne(String s) {
    return ((EnchantingTableContainerTraits) handler)
        .isBloodLapisRequired(storedLevel) ? "container.enchant.blood_lapis.one"
            : "container.enchant.lapis.one";
  }

  @ModifyConstant(
      method = "render",
      constant = @Constant(stringValue = "container.enchant.lapis.many")
  )
  private String onGetLapisMany(String s) {
    return ((EnchantingTableContainerTraits) handler).isBloodLapisRequired(
        storedLevel) ? "container.enchant.blood_lapis.many"
            : "container.enchant.lapis.many";
  }

  @Redirect(
      method = { "render", "drawBackground" },
      at = @At(
          value = "FIELD",
          target = "Lnet/minecraft/screen/EnchantmentScreenHandler;enchantmentPower:[I",
          args = "array=get"
      )
  )
  private int onGetIndexEnchantmentPower(int[] enchantmentPower, int level) {
    storedLevel = level;
    return enchantmentPower[level];
  }

  @Redirect(
      method = "render",
      at = @At(
          value = "FIELD",
          target = "net/minecraft/util/Formatting.GRAY:Lnet/minecraft/util/Formatting;",
          opcode = Opcodes.GETSTATIC, ordinal = 0
      )
  )
  private Formatting onGetFormattingGray() {
    if (!((EnchantingTableContainerTraits) handler)
        .isBloodLapisRequired(storedLevel))
      return Formatting.GRAY;
    Item enchantingMaterial = handler.getSlot(1).getStack().getItem();
    if (!(enchantingMaterial instanceof EnchantingMaterial)
        || !((EnchantingMaterial) enchantingMaterial).canOverenchant())
      return Formatting.RED;
    return Formatting.GRAY;
  }

  @Redirect(
      method = "drawBackground",
      at = @At(
          value = "FIELD",
          target = "Lnet/minecraft/client/network/ClientPlayerEntity;experienceLevel:I",
          opcode = Opcodes.GETFIELD
      )
  )
  private int onGetExperienceLevel(ClientPlayerEntity e) {
    // If the appropriate material is not in the slot, then force the level to
    // be shown as 0 so the button is grayed out
    if (!((EnchantingTableContainerTraits) handler)
        .isBloodLapisRequired(storedLevel))
      return e.experienceLevel;
    Item enchantingMaterial = handler.getSlot(1).getStack().getItem();
    if (!(enchantingMaterial instanceof EnchantingMaterial)
        || !((EnchantingMaterial) enchantingMaterial).canOverenchant())
      return 0;
    return e.experienceLevel;
  }
}
