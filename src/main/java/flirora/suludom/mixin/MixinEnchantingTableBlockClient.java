package flirora.suludom.mixin;

import java.util.Random;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.enchanting.PylonUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.EnchantingTableBlock;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Mixin(EnchantingTableBlock.class)
public abstract class MixinEnchantingTableBlockClient extends BlockWithEntity {
  protected MixinEnchantingTableBlockClient(Settings settings) {
    super(settings);
  }

  private final double VELOCITY_FACTOR = 1.0;

  @Environment(EnvType.CLIENT)
  private void spawnOrthogonalPylonParticle(World world, BlockPos pos,
      Random random, int offX, int offZ) {
    if (PylonUtils.hasOrthogonalPylonAt(world, pos, offX, offZ)) {
      double pOffX = offX + random.nextDouble();
      double pOffY = 3.5 + random.nextDouble();
      double pOffZ = offZ + random.nextDouble();

      world.addParticle(ParticleTypes.ENCHANT, pos.getX() + 0.5,
          pos.getY() + 2.0, pos.getZ() + 0.5, VELOCITY_FACTOR * pOffX,
          VELOCITY_FACTOR * pOffY, VELOCITY_FACTOR * pOffZ);
    }
  }

  @Environment(EnvType.CLIENT)
  private void spawnDiagonalPylonParticle(World world, BlockPos pos,
      Random random, int offX, int offZ) {
    if (PylonUtils.hasDiagonalPylonAt(world, pos, offX, offZ)) {
      double pOffX = offX + random.nextDouble();
      double pOffY = 3.5 + random.nextDouble();
      double pOffZ = offZ + random.nextDouble();

      world.addParticle(ParticleTypes.DRAGON_BREATH, pos.getX() + 0.5,
          pos.getY() + 2.0, pos.getZ() + 0.5, VELOCITY_FACTOR * pOffX,
          VELOCITY_FACTOR * pOffY, VELOCITY_FACTOR * pOffZ);
    }
  }

  @Inject(method = "randomDisplayTick", at = @At("HEAD"))
  @Environment(EnvType.CLIENT)
  public void onRandomDisplayTick(BlockState state, World world, BlockPos pos,
      Random random, CallbackInfo ci) {
    spawnOrthogonalPylonParticle(world, pos, random, -4, 0);
    spawnOrthogonalPylonParticle(world, pos, random, 4, 0);
    spawnOrthogonalPylonParticle(world, pos, random, 0, -4);
    spawnOrthogonalPylonParticle(world, pos, random, 0, 4);
    spawnDiagonalPylonParticle(world, pos, random, 4, 4);
    spawnDiagonalPylonParticle(world, pos, random, -4, 4);
    spawnDiagonalPylonParticle(world, pos, random, 4, -4);
    spawnDiagonalPylonParticle(world, pos, random, -4, -4);
  }
}
