package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.suludom.enchanting.EnchantingUtils;
import flirora.suludom.enchanting.SuludomEnchantments;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class MixinPlayerEntity extends LivingEntity {
  protected MixinPlayerEntity(EntityType<? extends LivingEntity> type,
      World world) {
    super(type, world);
  }

  @Shadow
  @Final
  public PlayerInventory inventory =
      new PlayerInventory((PlayerEntity) (Object) this);

  @Redirect(
      method = "attack",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/enchantment/EnchantmentHelper.getAttackDamage(Lnet/minecraft/item/ItemStack;Lnet/minecraft/entity/EntityGroup;)F",
          ordinal = 0
      )
  )
  private float getAttackDamageProxy(ItemStack stack, EntityGroup group,
      Entity target) {
    return EnchantmentHelper.getAttackDamage(stack, group)
        + EnchantingUtils.getAdditionalAttackDamage(stack,
            (PlayerEntity) (Object) this, (LivingEntity) target);
  }

  @Redirect(
      method = "attack",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/entity/Entity.damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"
      )
  )
  private boolean damageProxy(Entity target, DamageSource source,
      float amount) {
    if (target instanceof LivingEntity && EnchantmentHelper.getEquipmentLevel(
        SuludomEnchantments.CURSE_OF_MERCY, (PlayerEntity) (Object) this) > 0) {
      return target.damage(source,
          Math.min(amount, ((LivingEntity) target).getHealth() - 1));
    } else {
      return target.damage(source, amount);
    }
  }
}
