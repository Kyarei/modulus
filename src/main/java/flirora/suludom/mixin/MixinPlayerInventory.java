package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import flirora.suludom.item.tool.PerItemStackAttributes;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Nameable;
import net.minecraft.util.collection.DefaultedList;

@Mixin(PlayerInventory.class)
public abstract class MixinPlayerInventory implements Inventory, Nameable {
  private ItemStack suludomCurrentDamaged;

  @Redirect(
      method = "damageArmor",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/util/collection/DefaultedList.get(I)Ljava/lang/Object;"
      )
  )
  public Object getProxy(DefaultedList<ItemStack> list, int index) {
    ItemStack result = list.get(index);
    this.suludomCurrentDamaged = result;
    return result;
  }

  @Redirect(
      method = "damageArmor",
      at = @At(
          value = "INVOKE", target = "net/minecraft/item/Item.isFireproof()Z"
      )
  )
  public boolean isFireproofProxy(Item item) {
    if (item instanceof PerItemStackAttributes) {
      return ((PerItemStackAttributes) item)
          .isStackFireproof(suludomCurrentDamaged);
    }
    return item.isFireproof();
  }

}
