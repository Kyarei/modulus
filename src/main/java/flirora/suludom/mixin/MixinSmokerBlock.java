package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;

import flirora.suludom.block.EnchantableBlock;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.SmokerBlock;

@Mixin(SmokerBlock.class)
public abstract class MixinSmokerBlock extends AbstractFurnaceBlock
    implements EnchantableBlock {

  protected MixinSmokerBlock(Settings settings) {
    super(settings);
  }

}
