package flirora.suludom.mixin;

import flirora.suludom.SuludomMod;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.MutableWorldProperties;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Supplier;

@Mixin(ServerWorld.class) public abstract class MixinServerWorld extends World {
  protected MixinServerWorld(MutableWorldProperties mutableWorldProperties,
      RegistryKey<World> registryKey, RegistryKey<DimensionType> registryKey2,
      DimensionType dimensionType, Supplier<Profiler> profiler, boolean bl,
      boolean bl2, long l) {
    super(mutableWorldProperties, registryKey, registryKey2, dimensionType,
        profiler, bl, bl2, l);
  }

  @Inject(method = "onPlayerConnected", at = @At("TAIL"))
  public void onOnPlayerConnected(ServerPlayerEntity player, CallbackInfo ci) {
    SuludomMod.getMaterialManager().sendDataTo(player);
  }

}
