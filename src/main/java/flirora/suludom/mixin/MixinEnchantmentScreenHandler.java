package flirora.suludom.mixin;

import flirora.suludom.enchanting.EnchantingTableContainerTraits;
import flirora.suludom.enchanting.EnchantingUtils;
import flirora.suludom.enchanting.PylonUtils;
import flirora.suludom.item.EnchantingMaterial;
import flirora.suludom.utils.PropertyUtils;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.*;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;
import java.util.Random;

@Mixin(EnchantmentScreenHandler.class)
public abstract class MixinEnchantmentScreenHandler extends ScreenHandler
    implements EnchantingTableContainerTraits {
  protected MixinEnchantmentScreenHandler(ScreenHandlerType<?> type,
      int syncId) {
    super(type, syncId);
    inventory = null;
    context = null;
    random = null;
    seed = null;
    enchantmentPower = enchantmentId = enchantmentLevel = null;
  }

  @Mutable @Shadow
  @Final
  private final Inventory inventory;
  @Mutable @Shadow
  @Final
  private final ScreenHandlerContext context;
  @Mutable @Shadow
  @Final
  private final Random random;
  @Mutable @Shadow
  @Final
  private final Property seed;
  @Mutable @Shadow
  @Final
  public final int[] enchantmentPower;
  @Mutable @Shadow
  @Final
  public final int[] enchantmentId;
  @Mutable @Shadow
  @Final
  public final int[] enchantmentLevel;
  // no shadow; this is a new field
  @Mutable @Final
  public boolean[] bloodLapisRequired;

  @Inject(
      method = "<init>(ILnet/minecraft/entity/player/PlayerInventory;Lnet/minecraft/screen/ScreenHandlerContext;)V",
      at = @At("TAIL")
  )
  public void onConstruct(int syncId, PlayerInventory playerInventory,
      ScreenHandlerContext blockContext, CallbackInfo ci) {
    bloodLapisRequired = new boolean[] { false, false, false };
    EnchantmentScreenHandler self = (EnchantmentScreenHandler) (Object) this;
    self.slots.set(1, EnchantingUtils.createLapisOrBloodLapisSlot(this.inventory));
    self.slots.get(1).id = 1;
    this.addProperty(PropertyUtils.create(this.bloodLapisRequired, 0));
    this.addProperty(PropertyUtils.create(this.bloodLapisRequired, 1));
    this.addProperty(PropertyUtils.create(this.bloodLapisRequired, 2));
  }

  @Shadow
  private List<EnchantmentLevelEntry> generateEnchantments(ItemStack stack,
      int slot, int level) {
    return null;
  }

  // private static boolean isOverenchanted

  private static boolean hasBookshelfAt(World world, BlockPos blockPos,
      int offX, int offY, int offZ) {
    return world.getBlockState(blockPos.add(offX, offY, offZ))
        .getBlock() == Blocks.BOOKSHELF;
  }

  @Inject(method = "onContentChanged", at = @At("HEAD"), cancellable = true)
  public void onOnContentChanged(Inventory inventory, CallbackInfo ci) {
    if (inventory == this.inventory) {
      ItemStack itemStack = inventory.getStack(0);
      if (!itemStack.isEmpty() && itemStack.isEnchantable()) {
        this.context.run((world, blockPos) -> {
          int nBookshelves = 0;

          for (int offZ = -1; offZ <= 1; ++offZ) {
            for (int offX = -1; offX <= 1; ++offX) {
              if ((offZ != 0 || offX != 0)
                  && world.isAir(blockPos.add(offX, 0, offZ))
                  && world.isAir(blockPos.add(offX, 1, offZ))) {
                if (hasBookshelfAt(world, blockPos, offX * 2, 0, offZ * 2))
                  ++nBookshelves;
                if (hasBookshelfAt(world, blockPos, offX * 2, 1, offZ * 2))
                  ++nBookshelves;
                if (offX != 0 && offZ != 0) {
                  if (hasBookshelfAt(world, blockPos, offX * 2, 0, offZ))
                    ++nBookshelves;
                  if (hasBookshelfAt(world, blockPos, offX * 2, 1, offZ))
                    ++nBookshelves;
                  if (hasBookshelfAt(world, blockPos, offX, 0, offZ * 2))
                    ++nBookshelves;
                  if (hasBookshelfAt(world, blockPos, offX, 1, offZ * 2))
                    ++nBookshelves;
                }
              }
            }
          }

          int nOrthogonalPylons =
              (PylonUtils.hasOrthogonalPylonAt(world, blockPos, -4, 0) ? 1 : 0)
                  + (PylonUtils.hasOrthogonalPylonAt(world, blockPos, 4, 0) ? 1
                      : 0)
                  + (PylonUtils.hasOrthogonalPylonAt(world, blockPos, 0, -4) ? 1
                      : 0)
                  + (PylonUtils.hasOrthogonalPylonAt(world, blockPos, 0, 4) ? 1
                      : 0);

          int enchantmentPower = 2 * nBookshelves;
          int powerLimit = 30 * (6 + nOrthogonalPylons) / 6;

          if (enchantmentPower > powerLimit)
            enchantmentPower = powerLimit;

          int nDiagonalPylons =
              (PylonUtils.hasDiagonalPylonAt(world, blockPos, -4, -4) ? 1 : 0)
                  + (PylonUtils.hasDiagonalPylonAt(world, blockPos, -4, 4)
                      ? 1
                      : 0)
                  + (PylonUtils.hasDiagonalPylonAt(world, blockPos, 4, -4)
                      ? 1
                      : 0)
                  + (PylonUtils.hasDiagonalPylonAt(world, blockPos, 4, 4) ? 1
                      : 0);

          enchantmentPower = enchantmentPower * (4 + nDiagonalPylons) / 4;

          this.random.setSeed(seed.get());

          for (int slot = 0; slot < 3; ++slot) {
            this.enchantmentPower[slot] =
                EnchantmentHelper.calculateRequiredExperienceLevel(this.random,
                    slot, enchantmentPower, itemStack);
            this.enchantmentId[slot] = -1;
            this.enchantmentLevel[slot] = -1;
            if (this.enchantmentPower[slot] < slot + 1) {
              this.enchantmentPower[slot] = 0;
            }
          }

          for (int slot = 0; slot < 3; ++slot) {
            if (this.enchantmentPower[slot] > 0) {
              List<EnchantmentLevelEntry> list = this.generateEnchantments(
                  itemStack, slot, this.enchantmentPower[slot]);
              if (list != null && !list.isEmpty()) {
                EnchantmentLevelEntry infoEnchantment =
                    (EnchantmentLevelEntry) list
                        .get(this.random.nextInt(list.size()));
                this.enchantmentId[slot] =
                    Registry.ENCHANTMENT.getRawId(infoEnchantment.enchantment);
                this.enchantmentLevel[slot] = infoEnchantment.level;
                this.bloodLapisRequired[slot] =
                    EnchantingUtils.isOverenchanted(list);
              }
            }
          }

          this.sendContentUpdates();
        });
      } else {
        for (int slot = 0; slot < 3; ++slot) {
          this.enchantmentPower[slot] = 0;
          this.enchantmentId[slot] = -1;
          this.enchantmentLevel[slot] = -1;
          this.bloodLapisRequired[slot] = false;
        }
      }
    }

    ci.cancel();
  }

  @Inject(method = "transferSlot", at = @At("HEAD"), cancellable = true)
  public void onTransferSlot(PlayerEntity player, int invSlot,
      CallbackInfoReturnable<ItemStack> cir) {
    if (invSlot == 1)
      return; // don't touch this case
    Slot slot = slots.get(invSlot);
    if (slot != null && slot.hasStack()) {
      ItemStack from = slot.getStack();
      ItemStack old = from.copy();
      if (from.getItem() instanceof EnchantingMaterial) {
        if (!this.insertItem(from, 1, 2, true)) {
          cir.setReturnValue(ItemStack.EMPTY);
        } else {
          slot.markDirty();
          slot.onTakeItem(player, from);
          if (old.getCount() == from.getCount()) {
            cir.setReturnValue(ItemStack.EMPTY);
          } else {
            cir.setReturnValue(old);
          }
        }
        cir.cancel();
        return;
      }
    }
  }

  @Inject(method = "onButtonClick", at = @At("HEAD"), cancellable = true)
  public void onOurButtonClick(PlayerEntity player, int id,
      CallbackInfoReturnable<Boolean> cir) {
    ItemStack enchantingMaterial = this.inventory.getStack(1);
    Item enchantingMaterialItem = enchantingMaterial.getItem();
    if (bloodLapisRequired[id] && !player.abilities.creativeMode
        && (!(enchantingMaterialItem instanceof EnchantingMaterial)
            || !((EnchantingMaterial) enchantingMaterialItem)
                .canOverenchant())) {
      cir.setReturnValue(false);
      cir.cancel();
      return;
    }
  }

  @Override
  public boolean isBloodLapisRequired(int level) {
    return bloodLapisRequired[level];
  }
}
