package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import flirora.suludom.item.tool.PerItemStackAttributes;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(ItemEntity.class)
public abstract class MixinItemEntity extends Entity {

  public MixinItemEntity(EntityType<?> type, World world) {
    super(type, world);
    // TODO Auto-generated constructor stub
  }

  @Shadow
  public ItemStack getStack() {
    return null;
  }

  @Inject(method = "damage", at = @At("HEAD"), cancellable = true)
  private void onDamage(DamageSource source, float amount,
      CallbackInfoReturnable<Boolean> cir) {
    ItemStack stack = ((ItemEntity) (Object) this).getStack();
    if (stack.getItem() instanceof PerItemStackAttributes
        && ((PerItemStackAttributes) stack.getItem()).isStackFireproof(stack)) {
      cir.setReturnValue(false);
      cir.cancel();
    }
  }

  @Inject(method = "isFireImmune", at = @At("HEAD"), cancellable = true)
  private void onIsFireImmune(CallbackInfoReturnable<Boolean> cir) {
    ItemStack stack = this.getStack();
    Item item = stack.getItem();
    if (item instanceof PerItemStackAttributes) {
      cir.setReturnValue(
          ((PerItemStackAttributes) item).isStackFireproof(stack));
    }
  }

}
