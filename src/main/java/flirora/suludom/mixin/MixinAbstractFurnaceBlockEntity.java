package flirora.suludom.mixin;

import blue.endless.jankson.annotation.Nullable;
import flirora.suludom.SuludomMod;
import flirora.suludom.block.EnchantableBlockEntity;
import flirora.suludom.enchanting.EnchantingUtils;
import flirora.suludom.enchanting.SuludomEnchantments;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeInputProvider;
import net.minecraft.recipe.RecipeUnlocker;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Random;

@Mixin(AbstractFurnaceBlockEntity.class)
public abstract class MixinAbstractFurnaceBlockEntity
    extends LockableContainerBlockEntity implements SidedInventory,
    RecipeUnlocker, RecipeInputProvider, Tickable, EnchantableBlockEntity {

  private Object2IntMap<Enchantment> enchantments = null;

  protected MixinAbstractFurnaceBlockEntity(
      BlockEntityType<?> blockEntityType) {
    super(blockEntityType);
  }

  @Override
  public Object2IntMap<Enchantment> getEnchantments() {
    return enchantments;
  }

  @Override
  public void setEnchantments(Object2IntMap<Enchantment> enchantments) {
    this.enchantments = enchantments;
  }

  private int getFortuneLevel() {
    return this.getLevelOf(Enchantments.FORTUNE);
  }

  private int getMaxYield() {
    return (9 + 2 * getFortuneLevel()) / 5;
  }

  private int getYield(Random r) {
    return (5 + r.nextInt(2 * getFortuneLevel() + 5)) / 5;
  }

  @Shadow protected DefaultedList<ItemStack> inventory;

  private boolean ourCanAcceptRecipeOutput(@Nullable Recipe<?> recipe) {
    if (!inventory.get(0).isEmpty() && recipe != null) {
      ItemStack recipeOutput = recipe.getOutput();
      int maxYield = SuludomMod.MULTIPLIABLE_BY_FORTUNE_SMELTING
          .contains(recipeOutput.getItem()) ? getMaxYield() : 1;
      if (recipeOutput.isEmpty()) {
        return false;
      } else {
        ItemStack outputStack = inventory.get(2);
        if (outputStack.isEmpty()) {
          return true;
        } else if (!outputStack.isItemEqualIgnoreDamage(recipeOutput)) {
          return false;
        } else if (outputStack.getCount() + maxYield <= this
            .getMaxCountPerStack()
            && outputStack.getCount() + maxYield <= outputStack.getMaxCount()) {
          return true;
        } else {
          return outputStack.getCount() + maxYield <= recipeOutput
              .getMaxCount();
        }
      }
    } else {
      return false;
    }
  }

  @Inject(
      method = "canAcceptRecipeOutput", at = @At("HEAD"), cancellable = true
  )
  protected void onCanAcceptRecipeOutput(@Nullable Recipe<?> recipe,
      CallbackInfoReturnable<Boolean> cir) {
    cir.setReturnValue(ourCanAcceptRecipeOutput(recipe));
    cir.cancel();
  }

  // Cook items faster with Efficiency
  @Inject(method = "getCookTime", at = @At("TAIL"), cancellable = true)
  protected void onGetCookTime(CallbackInfoReturnable<Integer> cir) {
    if (enchantments == null)
      return;
    int efficiencyLevel = this.getLevelOf(Enchantments.EFFICIENCY);
    cir.setReturnValue(cir.getReturnValueI() * 3 / (3 + efficiencyLevel));
  }

  // Efficiency shortens fuel time to match cook time,
  // but Frugal extends it
  @Inject(method = "getFuelTime", at = @At("TAIL"), cancellable = true)
  protected void onGetFuelTime(CallbackInfoReturnable<Integer> cir) {
    if (enchantments == null)
      return;
    int efficiencyLevel = this.getLevelOf(Enchantments.EFFICIENCY);
    int frugalLevel = getLevelOf(SuludomEnchantments.FRUGAL);
    cir.setReturnValue(
        cir.getReturnValueI() * (3 + frugalLevel) / (3 + efficiencyLevel));
  }

  @Inject(method = "toTag", at = @At("TAIL"))
  public void onToTag(CompoundTag tag,
      CallbackInfoReturnable<CompoundTag> cir) {
    CompoundTag theTag = cir.getReturnValue();
    theTag.put("Enchantments", EnchantingUtils.enchantmentsToTag(enchantments));
  }

  @Inject(method = "fromTag", at = @At("TAIL"))
  public void onFromTag(BlockState state, CompoundTag tag, CallbackInfo ci) {
    enchantments =
        EnchantingUtils.enchantmentsFromTag(tag.getList("Enchantments", 10));
  }

  @Inject(method = "craftRecipe", at = @At("TAIL"))
  private void craftRecipe(@Nullable Recipe<?> recipe, CallbackInfo ci) {
    ItemStack output = recipe.getOutput();
    if (SuludomMod.MULTIPLIABLE_BY_FORTUNE_SMELTING
        .contains(output.getItem())) {
      int bonus = getYield(world.getRandom()) - 1;
      inventory.get(2).increment(bonus);
    }
  }

}
