package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;

import flirora.suludom.block.ScrappingTableBlock;
import net.minecraft.block.BlastFurnaceBlock;
import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.LuckEnchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mixin(LuckEnchantment.class)
public abstract class MixinLuckEnchantment extends Enchantment {

  protected MixinLuckEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  public boolean isAcceptableItem(ItemStack stack) {
    if (type != EnchantmentTarget.DIGGER)
      return super.isAcceptableItem(stack); // Only inject on Fortune, not
                                            // Looting
    Item item = stack.getItem();
    if (stack.getCount() == 1 && item instanceof BlockItem) {
      Block block = ((BlockItem) item).getBlock();
      if (block instanceof BlastFurnaceBlock
          || block instanceof ScrappingTableBlock) {
        return true;
      }
    }
    return super.isAcceptableItem(stack);
  }

}
