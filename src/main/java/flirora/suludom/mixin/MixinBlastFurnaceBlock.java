package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;

import flirora.suludom.block.EnchantableBlock;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.BlastFurnaceBlock;

@Mixin(BlastFurnaceBlock.class)
public abstract class MixinBlastFurnaceBlock extends AbstractFurnaceBlock
    implements EnchantableBlock {

  protected MixinBlastFurnaceBlock(Settings settings) {
    super(settings);
  }

}
