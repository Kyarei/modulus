package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.item.tool.render.GenericArmorBipedFeatureRenderer;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.client.render.entity.PlayerEntityRenderer;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.render.entity.model.PlayerEntityModel;

@Mixin(PlayerEntityRenderer.class)
public abstract class MixinPlayerEntityRenderer extends
    LivingEntityRenderer<AbstractClientPlayerEntity, PlayerEntityModel<AbstractClientPlayerEntity>> {

  public MixinPlayerEntityRenderer(
      EntityRenderDispatcher entityRenderDispatcher,
      PlayerEntityModel<AbstractClientPlayerEntity> entityModel, float f) {
    super(entityRenderDispatcher, entityModel, f);
  }

  @Inject(
      method = "<init>(Lnet/minecraft/client/render/entity/EntityRenderDispatcher;Z)V",
      at = @At("TAIL")
  )
  public void onInit(EntityRenderDispatcher entityRenderDispatcher,
      boolean slim, CallbackInfo ci) {
    this.addFeature(new GenericArmorBipedFeatureRenderer<>(this,
        new BipedEntityModel<>(0.5F), new BipedEntityModel<>(1.0F)));
  }

}
