package flirora.suludom.mixin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.google.common.collect.Lists;

import flirora.suludom.block.EnchantableBlock;
import flirora.suludom.item.tool.GenericGear;
import flirora.suludom.item.tool.PerItemStackAttributes;
import flirora.suludom.material.enchantment.InfoEnchantmentExtended;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Util;
import net.minecraft.util.collection.WeightedPicker;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.registry.Registry;

@Mixin(EnchantmentHelper.class)
public abstract class MixinEnchantmentHelper {

  private static int ourGetEnchantability(Item item, ItemStack stack) {
    if (item instanceof PerItemStackAttributes) {
      PerItemStackAttributes theItem = (PerItemStackAttributes) item;
      return theItem.getStackEnchantability(stack);
    }
    if (EnchantableBlock.isOne(item)) {
      return EnchantableBlock.getPropertyOrDefault(item,
          (eb) -> eb.getEnchantability(), 0);
    }
    return item.getEnchantability();
  }

  @Inject(
      method = "calculateRequiredExperienceLevel", at = @At(
        "HEAD"
      ), cancellable = true
  )
  private static void onCalculateRequiredExperienceLevel(Random random, int num,
      int enchantmentPower, ItemStack stack,
      CallbackInfoReturnable<Integer> cir) {
    Item item = stack.getItem();
    int enchantability = ourGetEnchantability(item, stack);
    if (enchantability <= 0) {
      cir.setReturnValue(0);
    } else {
      // we perform the enchantment power check earlier
      int j = random.nextInt(8) + 1 + (enchantmentPower >> 2)
          + random.nextInt((enchantmentPower >> 1) + 1);
      if (num == 0) {
        cir.setReturnValue(Math.max(j / 3, 1));
      } else {
        cir.setReturnValue(
            num == 1 ? j * 2 / 3 + 1 : Math.max(j, enchantmentPower));
      }
    }
    cir.cancel();
  }

  private static List<EnchantmentLevelEntry> ourGetHighestApplicableEnchantmentsAtPower(
      int power, ItemStack stack, boolean hasTreasure, Random random) {
    List<EnchantmentLevelEntry> list = Lists.newArrayList();
    Item item = stack.getItem();
    boolean isBook = item == Items.BOOK;
    for (Enchantment enchantment : Registry.ENCHANTMENT) {
      if (!enchantment.isAcceptableItem(stack) && !isBook) {
//        if (!(item instanceof GenericGear) || !((GenericGear) item)
//            .acceptsEnchantmentOfTarget(enchantment.type)) {
//          continue; // Wrong kind of item
//        }
        continue;
      }
      if (enchantment.isTreasure() && !hasTreasure)
        continue;
      // Is the power too high to support this enchantment's highest level?
      int maximumLevel = enchantment.getMaxLevel();
      if (power > enchantment.getMaxPower(maximumLevel)) {
        if (maximumLevel == 1) {
          // This enchantment has no levels, so just put in this enchantment
          // as is
          list.add(new EnchantmentLevelEntry(enchantment, 1));
        } else {
          int diff = power - enchantment.getMaxPower(maximumLevel);
          int actualLevel = maximumLevel;
          while (diff > 0) {
            if (random.nextInt(13) == 0) {
              ++actualLevel;
              diff = diff * 12 / 13;
            } else {
              --diff;
            }
          }
          list.add(new EnchantmentLevelEntry(enchantment, actualLevel));
        }
      } else {
        // Nothing too fishy; go on with our usual business
        for (int level = maximumLevel; level > enchantment.getMinLevel()
            - 1; --level) {
          if (power >= enchantment.getMinPower(level)
              && power <= enchantment.getMaxPower(level)) {
            list.add(new EnchantmentLevelEntry(enchantment, level));
            break;
          }
        }
      }
    }
    return list;
  }

  @Redirect(
      method = "generateEnchantments(Ljava/util/Random;Lnet/minecraft/item/ItemStack;IZ)Ljava/util/List;",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/enchantment/EnchantmentHelper;getPossibleEntries(ILnet/minecraft/item/ItemStack;Z)Ljava/util/List;"
      )
  )
  private static List<EnchantmentLevelEntry> getPossibleEntriesProxy(int power,
      ItemStack stack, boolean hasTreasure, Random random, ItemStack stack2,
      int level, boolean hasTreasure2) {
    return ourGetHighestApplicableEnchantmentsAtPower(power, stack, hasTreasure,
        random);
  }

  @Redirect(
      method = "generateEnchantments(Ljava/util/Random;Lnet/minecraft/item/ItemStack;IZ)Ljava/util/List;",
      at = @At(
          value = "INVOKE",
          target = "Lnet/minecraft/item/Item;getEnchantability()I"
      )
  )
  private static int getEnchantabilityProxy(Item item, Random random,
      ItemStack stack, int level, boolean hasTreasure) {
    return ourGetEnchantability(item, stack);
  }

  private static String stringifyEnchantments(
      List<EnchantmentLevelEntry> enchantments) {
    return "[" + enchantments.stream()
        .map(
            (ench) -> ench.enchantment.getName(0).asString() + " " + ench.level)
        .collect(Collectors.joining(", ")) + "]";
  }

  private static String stringifyEnchantmentsWithWeights(
      List<InfoEnchantmentExtended> enchantments) {
    return "[" + enchantments
        .stream().map((ench) -> ench.enchantment.getName(0).asString() + " "
            + ench.level + " " + ench.getWeight())
        .collect(Collectors.joining(", ")) + "]";
  }

  private static void removeExtended(List<InfoEnchantmentExtended> infos,
      EnchantmentLevelEntry info) {
    Iterator<InfoEnchantmentExtended> iterator = infos.iterator();

    while (iterator.hasNext()) {
      if (!info.enchantment.canCombine(iterator.next().enchantment)) {
        iterator.remove();
      }
    }

  }

  private static List<EnchantmentLevelEntry> getEnchantmentsForGenericGear(
      Random random, ItemStack stack, int level, boolean hasTreasure,
      GenericGear item) {
    Object2IntMap<Enchantment> affinities =
        GenericGear.getCompositeMaterialFrom(stack).getEnchantmentAffinities()
            .getAffinities();
    List<EnchantmentLevelEntry> chosen = Lists.newArrayList();
    int i = item.getStackEnchantability(stack);
    if (i <= 0) {
      return chosen;
    } else {
      level += 1 + random.nextInt(i / 4 + 1) + random.nextInt(i / 4 + 1);
      float f = (random.nextFloat() + random.nextFloat() - 1.0F) * 0.15F;
      level =
          MathHelper.clamp(Math.round(level + level * f), 1, Integer.MAX_VALUE);
      List<EnchantmentLevelEntry> eligibleEnchantments =
          ourGetHighestApplicableEnchantmentsAtPower(level, stack, true,
              random);
      List<InfoEnchantmentExtended> eligibleEnchantmentsExtended =
          eligibleEnchantments.stream()
              .filter((e) -> !e.enchantment.isTreasure()
                  || affinities.containsKey(e.enchantment))
              .map((e) -> new InfoEnchantmentExtended(e,
                  affinities.getOrDefault(e.enchantment, 0)))
              .collect(Collectors.toCollection(ArrayList::new));
      System.out.println("Eligible enchantments: "
          + stringifyEnchantmentsWithWeights(eligibleEnchantmentsExtended));
      if (!eligibleEnchantmentsExtended.isEmpty()) {
        chosen.add(WeightedPicker
            .getRandom(random, eligibleEnchantmentsExtended).toInfo());

        while (random.nextInt(50) <= level) {
          removeExtended(eligibleEnchantmentsExtended, Util.getLast(chosen));
          if (eligibleEnchantmentsExtended.isEmpty()) {
            break;
          }

          chosen.add(WeightedPicker
              .getRandom(random, eligibleEnchantmentsExtended).toInfo());
          level /= 2;
        }
      }
      System.out
          .println("Chosen enchantments: " + stringifyEnchantments(chosen));

      return chosen;
    }
  }

//  @Inject(
//      method = "generateEnchantments(Ljava/util/Random;Lnet/minecraft/item/ItemStack;IZ)Ljava/util/List;",
//      at = @At("HEAD"), cancellable = true
//  )
//  private static void onGenerateEnchantments(Random random, ItemStack stack,
//      int level, boolean hasTreasure,
//      CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir) {
//    if (stack.getItem() instanceof GenericGear) {
//      GenericGear item = (GenericGear) stack.getItem();
//      cir.setReturnValue(getEnchantmentsForGenericGear(random, stack, level,
//          hasTreasure, item));
//      cir.cancel();
//    }
//  }

  // debug method
//  @Inject(method = "getHighestApplicableEnchantmentsAtPower", at = @At("TAIL"))
//  private static void onGetEnchantmentsDebug(int power, ItemStack stack,
//      boolean hasTreasure, CallbackInfoReturnable<List<InfoEnchantment>> ci) {
//    System.out.println("Eligible enchantments at power " + power + ": "
//        + stringifyEnchantments(ci.getReturnValue()));
//  }

}
