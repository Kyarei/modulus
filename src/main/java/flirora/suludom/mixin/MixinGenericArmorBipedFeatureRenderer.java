package flirora.suludom.mixin;

import flirora.suludom.item.tool.render.GenericArmorBipedFeatureRenderer;
import flirora.suludom.item.tool.render.GenericArmorFeatureRenderer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GenericArmorBipedFeatureRenderer.class)
public abstract class MixinGenericArmorBipedFeatureRenderer<T extends LivingEntity, M extends BipedEntityModel<T>, A extends BipedEntityModel<T>>
    extends GenericArmorFeatureRenderer<T, M, A> {

  public MixinGenericArmorBipedFeatureRenderer(
      FeatureRendererContext<T, M> context, A leggingsModel, A bodyModel) {
    super(context, leggingsModel, bodyModel);
  }

  private GenericArmorBipedFeatureRenderer<T, M, A> suludomRenderer;

  @Inject(method = "<init>", at = @At("TAIL"))
  protected void onInit(FeatureRendererContext<T, M> featureRendererContext,
      A bipedEntityModel, A bipedEntityModel2, CallbackInfo ci) {
    suludomRenderer =
        new GenericArmorBipedFeatureRenderer<>(featureRendererContext,
            bipedEntityModel, bipedEntityModel2);
  }

  @Override public void render(MatrixStack matrixStack,
      VertexConsumerProvider vertexConsumerProvider, int light, T livingEntity,
      float limbAngle, float limbDistance, float tickDelta, float age,
      float yaw, float pitch) {
    super.render(matrixStack, vertexConsumerProvider, light, livingEntity,
        limbAngle, limbDistance, tickDelta, age, yaw, pitch);
    suludomRenderer
        .render(matrixStack, vertexConsumerProvider, light, livingEntity,
            limbAngle, limbDistance, tickDelta, age, yaw, pitch);
  }

}
