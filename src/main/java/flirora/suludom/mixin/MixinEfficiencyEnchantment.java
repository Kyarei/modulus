package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.BlastFurnaceBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SmokerBlock;
import net.minecraft.enchantment.EfficiencyEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mixin(EfficiencyEnchantment.class)
public abstract class MixinEfficiencyEnchantment extends Enchantment {

  protected MixinEfficiencyEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  @Inject(method = "isAcceptableItem", at = @At("HEAD"), cancellable = true)
  public void onIsAcceptableItem(ItemStack stack,
      CallbackInfoReturnable<Boolean> cir) {
    Item item = stack.getItem();
    if (stack.getCount() == 1 && item instanceof BlockItem) {
      Block block = ((BlockItem) item).getBlock();
      if (block instanceof BlastFurnaceBlock || block instanceof SmokerBlock) {
        cir.setReturnValue(true);
        cir.cancel();
        return;
      }
    }
  }

}
