package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.SuludomMod;
import flirora.suludom.item.tool.GenericShovel;
import flirora.suludom.item.tool.GenericSword;
import flirora.suludom.item.tool.MobEquipmentUtil;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.Difficulty;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.World;

@Mixin(ZombieEntity.class)
public abstract class MixinZombieEntity extends HostileEntity {

  public MixinZombieEntity(EntityType<? extends HostileEntity> type,
      World world) {
    super(type, world);
  }

  private void ourInitEquipment(LocalDifficulty difficulty) {
    super.initEquipment(difficulty);
    if (this.random
        .nextFloat() < (this.world.getDifficulty() == Difficulty.HARD ? 0.05F
            : 0.01F)) {
      int i = this.random.nextInt(3);
      if (i == 0) {
        this.equipStack(EquipmentSlot.MAINHAND,
            MobEquipmentUtil.createGenericTool(
                (GenericSword) SuludomMod.GENERIC_SWORD,
                new Identifier("minecraft", "iron_ingot"), 2, 1));
      } else {
        this.equipStack(EquipmentSlot.MAINHAND,
            MobEquipmentUtil.createGenericTool(
                (GenericShovel) SuludomMod.GENERIC_SHOVEL,
                new Identifier("minecraft", "iron_ingot"), 1, 2));
      }
    }
  }

  @Inject(method = "initEquipment", at = @At("HEAD"), cancellable = true)
  protected void onInitEquipment(LocalDifficulty difficulty, CallbackInfo ci) {
    if (!SuludomMod.config.tweakLootTables)
      return;
    ourInitEquipment(difficulty);
    ci.cancel();
  }

}
