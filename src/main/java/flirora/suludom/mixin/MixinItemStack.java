package flirora.suludom.mixin;

import flirora.suludom.item.tool.PerItemStackAttributes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ItemStack.class)
public abstract class MixinItemStack {
  @Shadow public Item getItem() {
    return null;
  }

  @Shadow private boolean empty;

  @Shadow public CompoundTag getTag() {
    return null;
  }

  @Shadow private CompoundTag tag;

  @Shadow public boolean hasTag() {
    return false;
  }

  @Inject(method = "getMaxDamage", at = @At("HEAD"), cancellable = true)
  public void onGetMaxDamage(CallbackInfoReturnable<Integer> cir) {
    if (getItem() instanceof PerItemStackAttributes) {
      PerItemStackAttributes item = (PerItemStackAttributes) getItem();
      cir.setReturnValue(item.getStackMaxDamage((ItemStack) (Object) this));
      cir.cancel();
    }
  }

  // This method also needs to be injected because it calls getMaxDamage()
  // on the item instead of on the item stack. *sigh*
  @Inject(method = "isDamageable", at = @At("HEAD"), cancellable = true)
  public void onIsDamageable(CallbackInfoReturnable<Boolean> cir) {
    if (!this.empty && this.getItem() instanceof PerItemStackAttributes) {
      boolean result = false;
      PerItemStackAttributes item = (PerItemStackAttributes) this.getItem();
      if (!this.empty && item
          .getStackMaxDamage((ItemStack) (Object) this) > 0) {
        CompoundTag compoundTag = this.getTag();
        result = compoundTag == null || !compoundTag.getBoolean("Unbreakable");
      }
      cir.setReturnValue(result);
      cir.cancel();
    }
  }

  //  @Inject(method = "isEffectiveOn", at = @At("HEAD"), cancellable = true)
  //  public void onIsEffectiveOn(BlockState state,
  //      CallbackInfoReturnable<Boolean> cir) {
  //    if (getItem() instanceof PerItemStackAttributes) {
  //      PerItemStackAttributes item = (PerItemStackAttributes) getItem();
  //      cir.setReturnValue(
  //          item.isStackEffectiveOn((ItemStack) (Object) this, state, null));
  //      cir.cancel();
  //    }
  //  }

  @Inject(method = "getName", at = @At(value = "RETURN", ordinal = 1), cancellable = true)
  public void onGetName(CallbackInfoReturnable<Text> cir) {
    if (getItem() instanceof PerItemStackAttributes) {
      PerItemStackAttributes item = (PerItemStackAttributes) getItem();
      cir.setReturnValue(item.getStackName((ItemStack) (Object) this));
    }
  }
}
