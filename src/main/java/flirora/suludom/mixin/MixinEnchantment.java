package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import blue.endless.jankson.annotation.Nullable;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;

@Mixin(Enchantment.class)
public abstract class MixinEnchantment {
  @Nullable
  @Shadow
  public EnchantmentTarget type;

  /*
   * public boolean isAcceptableItem(ItemStack stack) { return
   * this.type.isAcceptableItem(stack.getItem()); }
   */
//  @Inject(method = "isAcceptableItem", at = @At("HEAD"), cancellable = true)
//  public void onIsAcceptableItem(ItemStack stack,
//      CallbackInfoReturnable<Boolean> cir) {
//    if (stack.getItem() instanceof GenericGear) {
//      GenericGear gear = (GenericGear) stack.getItem();
//      cir.setReturnValue(gear.acceptsEnchantmentOfTarget(type));
//      cir.cancel();
//    }
//  }
}
