package flirora.suludom.mixin;

import flirora.suludom.block.EnchantableBlockEntity;
import flirora.suludom.enchanting.EnchantingUtils;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(BlockItem.class)
public abstract class MixinBlockItem extends Item {
  public MixinBlockItem(Settings settings) {
    super(settings);
  }

  /*
   * We have to inject the BlockItem#place method instead of Block#onPlaced
   * method because some of Minecraft's block classes don't call super.onPlaced
   * in their onPlaced methods.
   */
  @Inject(method = "place(Lnet/minecraft/item/ItemPlacementContext;)Lnet/minecraft/util/ActionResult;", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/Block;onPlaced(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/item/ItemStack;)V"), locals = LocalCapture.CAPTURE_FAILHARD)
  public void onPlace(ItemPlacementContext context,
      CallbackInfoReturnable<ActionResult> cir,
      ItemPlacementContext itemPlacementContext, BlockState blockState,
      BlockPos blockPos, World world, PlayerEntity playerEntity,
      ItemStack itemStack, BlockState blockState2, Block block) {
    BlockEntity be = world.getBlockEntity(blockPos);
    if (!(be instanceof EnchantableBlockEntity))
      return;
    EnchantableBlockEntity ebe = (EnchantableBlockEntity) be;
    if (itemStack.getTag() == null)
      return;
    ListTag enchantmentTag = itemStack.getTag().getList("Enchantments", 10);
    Object2IntMap<Enchantment> enchantments =
        EnchantingUtils.enchantmentsFromTag(enchantmentTag);
    ebe.setEnchantments(enchantments);
  }
}
