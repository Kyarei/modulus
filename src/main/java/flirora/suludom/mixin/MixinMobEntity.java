package flirora.suludom.mixin;

import java.util.Random;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import flirora.suludom.SuludomMod;
import flirora.suludom.enchanting.EnchantingUtils;
import flirora.suludom.enchanting.SuludomEnchantments;
import flirora.suludom.item.tool.GenericArmor;
import flirora.suludom.item.tool.MobEquipmentUtil;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

@Mixin(MobEntity.class)
public abstract class MixinMobEntity {
  @Inject(
      method = "getPreferredEquipmentSlot", at = @At(value = "HEAD"), cancellable = true
  )
  private static void onGetPreferredEquipmentSlot(ItemStack stack,
      CallbackInfoReturnable<EquipmentSlot> cir) {
    Item item = stack.getItem();
    if (item instanceof GenericArmor) {
      cir.setReturnValue(((GenericArmor) item).getSlotType());
    }
  }

  @Redirect(
      method = "tryAttack",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/enchantment/EnchantmentHelper.getAttackDamage(Lnet/minecraft/item/ItemStack;Lnet/minecraft/entity/EntityGroup;)F"
      )
  )
  public float getAttackDamageProxy(ItemStack stack, EntityGroup group,
      Entity target) {
    return EnchantmentHelper.getAttackDamage(stack, group)
        + EnchantingUtils.getAdditionalAttackDamage(stack,
            (MobEntity) (Object) this, (LivingEntity) target);
  }

  @Redirect(
      method = "tryAttack",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/entity/Entity.damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"
      )
  )
  private boolean damageProxy(Entity target, DamageSource source,
      float amount) {
    if (target instanceof LivingEntity && EnchantmentHelper.getEquipmentLevel(
        SuludomEnchantments.CURSE_OF_MERCY, (MobEntity) (Object) this) > 0) {
      return target.damage(source,
          Math.min(amount, ((LivingEntity) target).getHealth() - 1));
    } else {
      return target.damage(source, amount);
    }
  }

  private int suludomEquipmentLevel;

  @Redirect(
      method = "initEquipment",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/entity/mob/MobEntity.getEquipmentForSlot(Lnet/minecraft/entity/EquipmentSlot;I)Lnet/minecraft/item/Item;"
      )
  )
  public Item getEquipmentForSlotProxy(EquipmentSlot equipmentSlot,
      int equipmentLevel) {
    if (!SuludomMod.config.tweakLootTables)
      return MobEntity.getEquipmentForSlot(equipmentSlot, equipmentLevel);
    suludomEquipmentLevel = equipmentLevel;
    return Items.BIRCH_SAPLING;
  }

  @Redirect(
      method = "initEquipment",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/entity/mob/MobEntity.equipStack(Lnet/minecraft/entity/EquipmentSlot;Lnet/minecraft/item/ItemStack;)V"
      )
  )
  public void equipStackProxy(MobEntity entity, EquipmentSlot equipmentSlot,
      ItemStack stack) {
    if (!SuludomMod.config.tweakLootTables) {
      entity.equipStack(equipmentSlot, stack);
      return;
    }
    ItemStack theStack = MobEquipmentUtil.getEquipmentForSlot(equipmentSlot,
        suludomEquipmentLevel);
    if (theStack != null)
      entity.equipStack(equipmentSlot, theStack);
  }

  @Redirect(
      method = "updateEnchantments",
      at = @At(
          value = "INVOKE",
          target = "net/minecraft/enchantment/EnchantmentHelper.enchant(Ljava/util/Random;Lnet/minecraft/item/ItemStack;IZ)Lnet/minecraft/item/ItemStack;"
      )
  )
  protected ItemStack enchantProxy(Random random, ItemStack target, int level,
      boolean hasTreasure) {
    if (random.nextInt(10) == 0)
      level = (int) (level * (2.5 + 0.5 * random.nextGaussian()));
    return EnchantmentHelper.enchant(random, target, level, hasTreasure);
  }
}
