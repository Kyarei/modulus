package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.enchantment.DamageEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;

@Mixin(DamageEnchantment.class)
public abstract class MixinDamageEnchantment extends Enchantment {

  protected MixinDamageEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  @Inject(method = "canAccept", at = @At("HEAD"), cancellable = true)
  public void onCanAccept(Enchantment other,
      CallbackInfoReturnable<Boolean> cir) {
    if ((Enchantment) (Object) this != Enchantments.SHARPNESS
        && other != Enchantments.SHARPNESS && this != other) {
      cir.setReturnValue(true);
      cir.cancel();
    }
  }

}
