package flirora.suludom.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import flirora.suludom.SuludomMod;
import flirora.suludom.item.tool.GenericSword;
import flirora.suludom.item.tool.MobEquipmentUtil;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.mob.AbstractSkeletonEntity;
import net.minecraft.entity.mob.WitherSkeletonEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.World;

@Mixin(WitherSkeletonEntity.class)
public abstract class MixinWitherSkeletonEntity extends AbstractSkeletonEntity {

  protected MixinWitherSkeletonEntity(
      EntityType<? extends AbstractSkeletonEntity> type, World world) {
    super(type, world);
  }

  private void ourInitEquipment(LocalDifficulty difficulty) {
    this.equipStack(EquipmentSlot.MAINHAND,
        MobEquipmentUtil.createGenericTool(
            (GenericSword) SuludomMod.GENERIC_SWORD,
            new Identifier("minecraft", "stone"), 2, 1));
  }

  @Inject(method = "initEquipment", at = @At("HEAD"), cancellable = true)
  protected void onInitEquipment(LocalDifficulty difficulty, CallbackInfo ci) {
    if (!SuludomMod.config.tweakLootTables)
      return;
    ourInitEquipment(difficulty);
    ci.cancel();
  }

}
