package flirora.suludom;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import flirora.suludom.block.*;
import flirora.suludom.block.gui.*;
import flirora.suludom.command.RecalculateCommand;
import flirora.suludom.enchanting.*;
import flirora.suludom.generation.GenerationUtils;
import flirora.suludom.item.BloodLapisItem;
import flirora.suludom.item.LoreItem;
import flirora.suludom.item.tool.*;
import flirora.suludom.loot.SuludomLootFunctionTypes;
import flirora.suludom.material.MaterialManager;
import flirora.suludom.material.MaterialType;
import flirora.suludom.material.armor.ArmorMaterialSerializer;
import flirora.suludom.material.augment.AugmentMaterialSerializer;
import flirora.suludom.material.rod.RodMaterialSerializer;
import flirora.suludom.material.tool.ToolMaterialSerializer;
import flirora.suludom.recipe.BloodSmeltingRecipe;
import flirora.suludom.recipe.BloodSmeltingRecipeSerializer;
import flirora.suludom.recipe.ToolTableRecipe;
import flirora.suludom.recipe.ToolTableRecipeSerializer;
import io.github.cottonmc.libcd.api.util.Gsons;
import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.JanksonConfigSerializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplierBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback.LootTableSetter;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.tag.TagRegistry;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.LootTable;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.tag.Tag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SuludomMod implements ModInitializer {
  public static SuludomConfig config;
  public static final String MOD_ID = "suludom";

  private static final MaterialManager MATERIAL_MANAGER = new MaterialManager();
  public static final Block TOOL_TABLE = new ToolTableBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 0).requiresTool()
          .resistance(30.0f).hardness(3.0f));
  public static final Block AUGMENT_TABLE = new AugmentTableBlock(
      FabricBlockSettings.of(Material.METAL).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 1).requiresTool()
          .resistance(30.0f).hardness(5.0f));
  public static final Block SCRAPPING_TABLE = new ScrappingTableBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 2).requiresTool()
          .resistance(50.0f).hardness(8.0f));
  public static final Block BLOOD_FURNACE = new BloodFurnaceBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 3).requiresTool()
          .resistance(70.0f).hardness(12.0f));
  public static final Block ORTHOGONAL_PYLON = new OrthogonalPylonBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 2).requiresTool()
          .resistance(5000.0f).hardness(5.0f).nonOpaque());
  public static final Block DIAGONAL_PYLON = new DiagonalPylonBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 3).requiresTool()
          .resistance(5000.0f).hardness(7.0f).nonOpaque());
  public static final Block BLOOD_LAPIS_ORE = new BloodLapisOreBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 3).requiresTool()
          .resistance(15.0f).hardness(3.0f));
  public static final Block BLOOD_LAPIS_BLOCK = new Block(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 3).requiresTool()
          .resistance(30.0f).hardness(3.0f));
  public static final Block NEBULITE_ORE = new Block(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 3).requiresTool()
          .resistance(3000.0f).hardness(100.0f));
  public static final Block HOMURA_ORE = new HomuraOreBlock(
      FabricBlockSettings.of(Material.STONE).breakByHand(false)
          .breakByTool(FabricToolTags.PICKAXES, 4).requiresTool()
          .resistance(15.0f).hardness(3.0f));
  public static BlockEntityType<ToolTableBlockEntity> TOOL_TABLE_BLOCK_ENTITY;
  public static BlockEntityType<AugmentTableBlockEntity>
      AUGMENT_TABLE_BLOCK_ENTITY;
  public static BlockEntityType<ScrappingTableBlockEntity>
      SCRAPPING_TABLE_BLOCK_ENTITY;
  public static BlockEntityType<BloodFurnaceBlockEntity>
      BLOOD_FURNACE_BLOCK_ENTITY;
  public static final Item GENERIC_PICKAXE =
      new GenericPickaxe(new Item.Settings().maxCount(1));
  public static final Item GENERIC_AXE =
      new GenericAxe(new Item.Settings().maxCount(1));
  public static final Item GENERIC_SHOVEL =
      new GenericShovel(new Item.Settings().maxCount(1));
  public static final Item GENERIC_HOE =
      new GenericHoe(new Item.Settings().maxCount(1));
  public static final Item GENERIC_SWORD =
      new GenericSword(new Item.Settings().maxCount(1));
  public static final Item GENERIC_HELMET =
      new GenericArmor(EquipmentSlot.HEAD, new Item.Settings().maxCount(1));
  public static final Item GENERIC_CHESTPLATE =
      new GenericArmor(EquipmentSlot.CHEST, new Item.Settings().maxCount(1));
  public static final Item GENERIC_LEGGINGS =
      new GenericArmor(EquipmentSlot.LEGS, new Item.Settings().maxCount(1));
  public static final Item GENERIC_BOOTS =
      new GenericArmor(EquipmentSlot.FEET, new Item.Settings().maxCount(1));
  public static final Item BLOOD_LAPIS =
      new BloodLapisItem(new Item.Settings().group(ItemGroup.MISC));
  public static final Item NEBULITE_INGOT =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item HOMURA_INGOT =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Item SHINONOME_INGOT =
      new LoreItem(new Item.Settings().group(ItemGroup.MISC),
          new TranslatableText("tooltip.lore.suludom.shinonome"));
  public static final Item DENSE_IRON_INGOT =
      new LoreItem(new Item.Settings().group(ItemGroup.MISC),
          new TranslatableText("tooltip.lore.suludom.dense_iron_ingot"));
  public static final Item CRYSTAL_MATRIX =
      new LoreItem(new Item.Settings().group(ItemGroup.MISC),
          new TranslatableText("tooltip.lore.suludom.crystal_matrix"));
  public static final Item PAPER_ROD =
      new Item(new Item.Settings().group(ItemGroup.MISC));
  public static final Tag<Item> MULTIPLIABLE_BY_FORTUNE_SMELTING = TagRegistry
      .item(new Identifier(MOD_ID, "multipliable_by_fortune_smelting"));
  // Used for the tool table crafting recipe
  public static final Tag<Item> TOOL_TABLE_EXTERIOR =
      TagRegistry.item(new Identifier(MOD_ID, "tool_table_exterior"));

  public static final Identifier MATERIAL_DATA_PACKET_ID =
      new Identifier(MOD_ID, "material_data_packet");
  public static final Identifier SCRAP_GEAR_PACKET_ID =
      new Identifier(MOD_ID, "scrap_gear_packet");

  public static MaterialManager getMaterialManager() {
    return MATERIAL_MANAGER;
  }

  private void registerCommands() {
    CommandRegistrationCallback.EVENT.register(new RecalculateCommand());
  }

  private static void registerBlock(Identifier id, Block block,
      Item.Settings settings) {
    Registry.register(Registry.BLOCK, id, block);
    Registry.register(Registry.ITEM, id, new BlockItem(block, settings));
  }

  private static boolean validatePosition(PlayerEntity player, BlockPos pos) {
    if (player.world.canSetBlock(pos) && player.getPos()
        .squaredDistanceTo(pos.getX(), pos.getY(), pos.getZ()) <= 16 * 16) {
      return true;
    } else {
      player.sendMessage(new LiteralText("I am a hacker!"), false);
      return false;
    }
  }

  @Override
  public void onInitialize() {
    // This code runs as soon as Minecraft is in a mod-load-ready state.
    // However, some things (like resources) may still be uninitialized.
    // Proceed with mild caution.'

    // Read configuration
    AutoConfig.register(SuludomConfig.class, JanksonConfigSerializer::new);
    config = AutoConfig.getConfigHolder(SuludomConfig.class).getConfig();

    // Register the material manager
    MATERIAL_MANAGER.registerMaterialType(MaterialType.TOOL_MATERIAL,
        new ToolMaterialSerializer());
    MATERIAL_MANAGER.registerMaterialType(MaterialType.ROD_MATERIAL,
        new RodMaterialSerializer());
    MATERIAL_MANAGER.registerMaterialType(MaterialType.ARMOR_MATERIAL,
        new ArmorMaterialSerializer());
    MATERIAL_MANAGER.registerMaterialType(MaterialType.AUGMENT_MATERIAL,
        new AugmentMaterialSerializer());
    ResourceManagerHelper helper =
        ResourceManagerHelper.get(ResourceType.SERVER_DATA);
    helper.registerReloadListener(MATERIAL_MANAGER);

    // Register blocks
    registerBlock(ToolTableBlock.ID, TOOL_TABLE,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    TOOL_TABLE_BLOCK_ENTITY = Registry
        .register(Registry.BLOCK_ENTITY_TYPE, ToolTableBlock.ID,
            BlockEntityType.Builder
                .create(ToolTableBlockEntity::new, TOOL_TABLE).build(null));
    registerBlock(AugmentTableBlock.ID, AUGMENT_TABLE,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    AUGMENT_TABLE_BLOCK_ENTITY = Registry
        .register(Registry.BLOCK_ENTITY_TYPE, AugmentTableBlock.ID,
            BlockEntityType.Builder
                .create(AugmentTableBlockEntity::new, AUGMENT_TABLE)
                .build(null));
    registerBlock(ScrappingTableBlock.ID, SCRAPPING_TABLE,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    SCRAPPING_TABLE_BLOCK_ENTITY = Registry
        .register(Registry.BLOCK_ENTITY_TYPE, ScrappingTableBlock.ID,
            BlockEntityType.Builder
                .create(ScrappingTableBlockEntity::new, SCRAPPING_TABLE)
                .build(null));
    registerBlock(BloodFurnaceBlock.ID, BLOOD_FURNACE,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    BLOOD_FURNACE_BLOCK_ENTITY = Registry
        .register(Registry.BLOCK_ENTITY_TYPE, BloodFurnaceBlock.ID,
            BlockEntityType.Builder
                .create(BloodFurnaceBlockEntity::new, BLOOD_FURNACE)
                .build(null));
    registerBlock(OrthogonalPylonBlock.ID, ORTHOGONAL_PYLON,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    registerBlock(DiagonalPylonBlock.ID, DIAGONAL_PYLON,
        new Item.Settings().group(ItemGroup.DECORATIONS));
    registerBlock(BloodLapisOreBlock.ID, BLOOD_LAPIS_ORE,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    registerBlock(new Identifier(MOD_ID, "blood_lapis_block"),
        BLOOD_LAPIS_BLOCK,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    registerBlock(new Identifier(MOD_ID, "nebulite_ore"), NEBULITE_ORE,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));
    registerBlock(HomuraOreBlock.ID, HOMURA_ORE,
        new Item.Settings().group(ItemGroup.BUILDING_BLOCKS));

    // Register tool types
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_pickaxe"),
        GENERIC_PICKAXE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_axe"),
        GENERIC_AXE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_shovel"),
        GENERIC_SHOVEL);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_hoe"),
        GENERIC_HOE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_sword"),
        GENERIC_SWORD);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_helmet"),
        GENERIC_HELMET);
    Registry
        .register(Registry.ITEM, new Identifier(MOD_ID, "generic_chestplate"),
            GENERIC_CHESTPLATE);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_leggings"),
        GENERIC_LEGGINGS);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "generic_boots"),
        GENERIC_BOOTS);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "blood_lapis"),
        BLOOD_LAPIS);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "nebulite_ingot"),
        NEBULITE_INGOT);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "homura_ingot"),
        HOMURA_INGOT);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "shinonome_ingot"),
        SHINONOME_INGOT);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "dense_iron_ingot"),
        DENSE_IRON_INGOT);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "crystal_matrix"),
        CRYSTAL_MATRIX);
    Registry.register(Registry.ITEM, new Identifier(MOD_ID, "paper_rod"),
        PAPER_ROD);

    // Register recipe types (namely, the tool table recipes)
    Registry.register(Registry.RECIPE_SERIALIZER, ToolTableRecipeSerializer.ID,
        ToolTableRecipeSerializer.INSTANCE);
    Registry.register(Registry.RECIPE_TYPE,
        new Identifier(MOD_ID, ToolTableRecipe.Type.ID),
        ToolTableRecipe.Type.INSTANCE);
    Registry
        .register(Registry.RECIPE_SERIALIZER, BloodSmeltingRecipeSerializer.ID,
            BloodSmeltingRecipeSerializer.INSTANCE);
    Registry.register(Registry.RECIPE_TYPE,
        new Identifier(MOD_ID, BloodSmeltingRecipe.Type.ID),
        BloodSmeltingRecipe.Type.INSTANCE);

    // Register enchantments
    Registry.register(Registry.ENCHANTMENT, FrugalEnchantment.ID,
        SuludomEnchantments.FRUGAL);
    Registry.register(Registry.ENCHANTMENT, RunicLoopbackEnchantment.ID,
        SuludomEnchantments.RUNIC_LOOPBACK);
    Registry
        .register(Registry.ENCHANTMENT, new Identifier(MOD_ID, "tidal_edge"),
            SuludomEnchantments.TIDAL_EDGE);
    Registry.register(Registry.ENCHANTMENT, new Identifier(MOD_ID, "endless"),
        SuludomEnchantments.ENDLESS);
    Registry.register(Registry.ENCHANTMENT, new Identifier(MOD_ID, "defuser"),
        SuludomEnchantments.DEFUSER);
    Registry
        .register(Registry.ENCHANTMENT, new Identifier(MOD_ID, "pork_chopper"),
            SuludomEnchantments.PORK_CHOPPER);
    Registry.register(Registry.ENCHANTMENT, CurseOfMercyEnchantment.ID,
        SuludomEnchantments.CURSE_OF_MERCY);
    Registry.register(Registry.ENCHANTMENT, CorruptedEdgeEnchantment.ID,
        SuludomEnchantments.CORRUPTED_EDGE);

    // Register GUIs
    SuludomScreenHandlerTypes.load();
    ContainerProviderRegistry.INSTANCE.registerFactory(ToolTableBlock.ID,
        (syncId, id, player, buf) -> new ToolTableController(syncId,
            player.inventory,
            ScreenHandlerContext.create(player.world, buf.readBlockPos())));
    ContainerProviderRegistry.INSTANCE.registerFactory(AugmentTableBlock.ID,
        (syncId, id, player, buf) -> new AugmentTableController(syncId,
            player.inventory,
            ScreenHandlerContext.create(player.world, buf.readBlockPos())));
    ContainerProviderRegistry.INSTANCE.registerFactory(ScrappingTableBlock.ID,
        (syncId, id, player, buf) -> new ScrappingTableController(syncId,
            player.inventory,
            ScreenHandlerContext.create(player.world, buf.readBlockPos())));
    ContainerProviderRegistry.INSTANCE.registerFactory(BloodFurnaceBlock.ID,
        (syncId, id, player, buf) -> new BloodFurnaceController(syncId,
            player.inventory,
            ScreenHandlerContext.create(player.world, buf.readBlockPos())));

    // Register packets
    ServerSidePacketRegistry.INSTANCE.register(SCRAP_GEAR_PACKET_ID,
        (packetContext, attachedData) -> {
          BlockPos pos = attachedData.readBlockPos();
          packetContext.getTaskQueue().execute(() -> {
            PlayerEntity player = packetContext.getPlayer();
            if (validatePosition(player, pos)) {
              BlockEntity be = player.world.getBlockEntity(pos);
              if (be instanceof ScrappingTableBlockEntity) {
                ((ScrappingTableBlockEntity) be).scrap();
              }
            }
          });
        });

    // Register commands
    registerCommands();

    // Register world gen
    GenerationUtils.registerFeatures();

    // Intercept loot tables
    new SuludomLootFunctionTypes();
    if (config.tweakLootTables) {
      LootTableLoadingCallback.EVENT.register(
          (ResourceManager resourceManager, LootManager manager, Identifier id,
              FabricLootSupplierBuilder supplier, LootTableSetter setter) -> {
            JsonObject json = Gsons.LOOT_TABLE.toJsonTree(manager.getTable(id))
                .getAsJsonObject();
            transformLootTable(json);
            LootTable changed =
                Gsons.LOOT_TABLE.fromJson(json, LootTable.class);
            setter.set(changed);
          });
    }
  }

  private static final ImmutableMap<String, ImmutablePair<String, Function<String, CompoundTag>>> FORBIDDEN_GEAR_TYPES;

  private static CompoundTag makeToolTag(String materialId, int nToolMaterial,
      int nRodMaterial) {
    CompoundTag tag = new CompoundTag();
    ListTag toolMaterialTag = new ListTag();
    ListTag rodMaterialTag = new ListTag();
    StringTag material = StringTag.of(materialId);
    StringTag stick = StringTag.of("minecraft:stick");
    for (int i = 0; i < nToolMaterial; ++i)
      toolMaterialTag.add(material);
    for (int i = 0; i < nRodMaterial; ++i)
      rodMaterialTag.add(stick);
    tag.put("ToolMaterials", toolMaterialTag);
    tag.put("RodMaterials", rodMaterialTag);
    return tag;
  }

  private static CompoundTag makeArmorTag(String materialId,
      int nArmorMaterial) {
    CompoundTag tag = new CompoundTag();
    ListTag armorMaterialTag = new ListTag();
    StringTag material = StringTag.of(materialId);
    for (int i = 0; i < nArmorMaterial; ++i)
      armorMaterialTag.add(material);
    tag.put("ArmorMaterials", armorMaterialTag);
    return tag;
  }

  static {
    Map<String, ImmutablePair<String, Function<String, CompoundTag>>> temp =
        new HashMap<>();
    temp.put("pickaxe", ImmutablePair.of("suludom:generic_pickaxe",
        (id) -> makeToolTag(id, 3, 2)));
    temp.put("shovel", ImmutablePair.of("suludom:generic_shovel",
        (id) -> makeToolTag(id, 1, 2)));
    temp.put("axe",
        ImmutablePair.of("suludom:generic_axe", (id) -> makeToolTag(id, 3, 2)));
    temp.put("sword", ImmutablePair.of("suludom:generic_sword",
        (id) -> makeToolTag(id, 2, 1)));
    temp.put("hoe",
        ImmutablePair.of("suludom:generic_hoe", (id) -> makeToolTag(id, 2, 2)));
    temp.put("helmet", ImmutablePair.of("suludom:generic_helmet",
        (id) -> makeArmorTag(id, 5)));
    temp.put("chestplate", ImmutablePair.of("suludom:generic_chestplate",
        (id) -> makeArmorTag(id, 8)));
    temp.put("leggings", ImmutablePair.of("suludom:generic_leggings",
        (id) -> makeArmorTag(id, 7)));
    temp.put("boots",
        ImmutablePair.of("suludom:generic_boots", (id) -> makeArmorTag(id, 4)));
    FORBIDDEN_GEAR_TYPES = ImmutableMap.copyOf(temp);
  }

  private static final ImmutableMap<String, String> FORBIDDEN_MATERIALS =
      ImmutableMap.of("stone", "minecraft:stone", "iron",
          "minecraft:iron_ingot", "golden", "minecraft:gold_ingot", "diamond",
          "minecraft:diamond", "chain", "minecraft:iron_ingot");
  private static final Pattern FORBIDDEN_ITEM_REGEX =
      Pattern.compile("(\\w+)_(\\w+)");

  private void transformLootTable(JsonObject json) {
    if (json.get("pools") == null)
      return;// Some loot tables don't have this field
    JsonArray pools = json.get("pools").getAsJsonArray();
    for (JsonElement pool : pools) {
      JsonArray entries =
          pool.getAsJsonObject().get("entries").getAsJsonArray();
      for (JsonElement e : entries) {
        JsonObject entry = e.getAsJsonObject();
        // Only touch items
        if (!entry.get("type").getAsString().equals("minecraft:item"))
          continue;
        // Should we touch this?
        Identifier id = Identifier.tryParse(entry.get("name").getAsString());
        if (id == null || !id.getNamespace().equals("minecraft"))
          continue;
        Matcher m = FORBIDDEN_ITEM_REGEX.matcher(id.getPath());
        if (!m.matches())
          continue;
        String materialId = FORBIDDEN_MATERIALS.get(m.group(1));
        // We did it. Java 10 has `var`. C++ has `auto`. But alas, we are using
        // neither of those. Thanks to Eclipse for helpfully filling this out.
        ImmutablePair<String, Function<String, CompoundTag>> gearInfo =
            FORBIDDEN_GEAR_TYPES.get(m.group(2));
        if (materialId == null || gearInfo == null)
          continue;
        // Touch that baby
        CompoundTag result = gearInfo.getRight().apply(materialId);
        CompoundTag outerResult = new CompoundTag();
        outerResult.put("RecipeResult", result);
        String snbt = outerResult.toString();
        entry.remove("name");
        entry.addProperty("name", gearInfo.getLeft());
        JsonElement fns = entry.get("functions");
        if (fns == null) {
          fns = new JsonArray();
          entry.add("functions", fns);
        }
        JsonArray functions = fns.getAsJsonArray();
        JsonObject setNbtFunction = new JsonObject();
        setNbtFunction.addProperty("function", "minecraft:set_nbt");
        setNbtFunction.addProperty("tag", snbt);
        JsonObject recalculateFunction = new JsonObject();
        recalculateFunction.addProperty("function",
            "suludom:recalculate_composite");
        JsonArray newFunctions = new JsonArray();
        newFunctions.add(setNbtFunction);
        newFunctions.add(recalculateFunction);
        newFunctions.addAll(functions);
        entry.remove("functions");
        entry.add("functions", newFunctions);
      }
    }
  }
}
