package flirora.suludom.enchanting;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;

public abstract class SuludomDamageEnchantment extends Enchantment
    implements CustomDamageEnchantment {
  private final int minimumPowerAtLvl1;
  private final int minimumPowerIncrementPerLevel;
  private final int powerRange;
  private final int maxLevel;
  private final float damageBonusBase;
  private final float damageBonusPerLevel;

  public SuludomDamageEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes, int minimumPowerAtLvl1,
      int minimumPowerIncrementPerLevel, int powerRange, int maxLevel,
      float damageBonusBase, float damageBonusPerLevel) {
    super(weight, type, slotTypes);
    this.minimumPowerAtLvl1 = minimumPowerAtLvl1;
    this.minimumPowerIncrementPerLevel = minimumPowerIncrementPerLevel;
    this.powerRange = powerRange;
    this.maxLevel = maxLevel;
    this.damageBonusBase = damageBonusBase;
    this.damageBonusPerLevel = damageBonusPerLevel;
  }

  public abstract boolean isEligible(LivingEntity entity);

  @Override
  public int getMaxLevel() {
    return maxLevel;
  }

  @Override
  public int getMinPower(int level) {
    return minimumPowerAtLvl1 + minimumPowerIncrementPerLevel * (level - 1);
  }

  @Override
  public int getMaxPower(int level) {
    return getMinPower(level) + powerRange;
  }

  @Override
  public float getAttackDamage(int level, LivingEntity attacker,
      LivingEntity target) {
    if (isEligible(target)) {
      return damageBonusBase + damageBonusPerLevel * (level - 1);
    }
    return 0.0f;
  }

  @Override
  public boolean canAccept(Enchantment other) {
    return other != Enchantments.SHARPNESS && this != other;
  }

}
