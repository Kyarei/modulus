package flirora.suludom.enchanting;

public interface EnchantingTableContainerTraits {
  boolean isBloodLapisRequired(int level);
}
