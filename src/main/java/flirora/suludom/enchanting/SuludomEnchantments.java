package flirora.suludom.enchanting;

import com.google.common.collect.ImmutableSet;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;

import java.util.Set;

public class SuludomEnchantments {
  public static final Enchantment FRUGAL =
      new FrugalEnchantment(Enchantment.Rarity.UNCOMMON,
          EnchantmentTarget.TRIDENT /* best gum */, new EquipmentSlot[] {});
  public static final Enchantment RUNIC_LOOPBACK =
      new RunicLoopbackEnchantment(Enchantment.Rarity.VERY_RARE,
          EnchantmentTarget.TRIDENT, new EquipmentSlot[] {});

  private static final Set<EntityType<?>> WATER_SUSCEPTIBLE_MOBS = ImmutableSet
      .of(EntityType.BLAZE, EntityType.MAGMA_CUBE, EntityType.ENDERMAN);
  public static final Enchantment TIDAL_EDGE = new SuludomDamageEnchantment(
      Enchantment.Rarity.RARE, EnchantmentTarget.WEAPON,
      new EquipmentSlot[] { EquipmentSlot.MAINHAND }, 15, 8, 20, 5, 2.5f,
      2.5f) {
    @Override public boolean isEligible(LivingEntity entity) {
      return WATER_SUSCEPTIBLE_MOBS.contains(entity.getType());
    }
  };

  private static final Set<EntityType<?>> ENDER_MOBS =
      ImmutableSet.of(EntityType.ENDERMAN, EntityType.ENDER_DRAGON,
          EntityType.ENDERMITE, EntityType.SHULKER);
  public static final Enchantment ENDLESS = new SuludomDamageEnchantment(
      Enchantment.Rarity.RARE, EnchantmentTarget.WEAPON,
      new EquipmentSlot[] { EquipmentSlot.MAINHAND }, 22, 10, 20, 5, 2.5f,
      2.5f) {
    @Override public boolean isEligible(LivingEntity entity) {
      return ENDER_MOBS.contains(entity.getType());
    }
  };

  private static final Set<EntityType<?>> EXPLOSIVE_MOBS =
      ImmutableSet.of(EntityType.CREEPER, EntityType.GHAST, EntityType.WITHER);
  public static final Enchantment DEFUSER = new SuludomDamageEnchantment(
      Enchantment.Rarity.RARE, EnchantmentTarget.WEAPON,
      new EquipmentSlot[] { EquipmentSlot.MAINHAND }, 10, 8, 20, 5, 2.5f,
      2.5f) {
    @Override public boolean isEligible(LivingEntity entity) {
      return EXPLOSIVE_MOBS.contains(entity.getType());
    }
  };

  private static final Set<EntityType<?>> PIGGY_MOBS = ImmutableSet
      .of(EntityType.PIG, EntityType.PIGLIN, EntityType.ZOMBIFIED_PIGLIN,
          EntityType.HOGLIN, EntityType.ZOGLIN);
  public static final Enchantment PORK_CHOPPER =
      new SuludomDamageEnchantment(Enchantment.Rarity.RARE,
          EnchantmentTarget.WEAPON,
          new EquipmentSlot[] { EquipmentSlot.MAINHAND }, 10, 8, 20, 5, 2.5f,
          2.5f) {
        @Override public boolean isEligible(LivingEntity entity) {
          return PIGGY_MOBS.contains(entity.getType());
        }
      };

  public static final Enchantment CURSE_OF_MERCY = new CurseOfMercyEnchantment(
      Enchantment.Rarity.VERY_RARE, EnchantmentTarget.WEAPON,
      new EquipmentSlot[] { EquipmentSlot.MAINHAND }, 15, 8, 20, 5, 1.5f,
      0.75f);
  public static final Enchantment CORRUPTED_EDGE = new CorruptedEdgeEnchantment(
      Enchantment.Rarity.RARE, EnchantmentTarget.WEAPON,
      new EquipmentSlot[] { EquipmentSlot.MAINHAND });
}
