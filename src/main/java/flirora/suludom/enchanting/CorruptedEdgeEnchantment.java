package flirora.suludom.enchanting;

import java.util.List;

import com.google.common.collect.ImmutableList;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.util.Identifier;

public class CorruptedEdgeEnchantment extends Enchantment {
  public static final Identifier ID =
      new Identifier("suludom", "corrupted_edge");

  public CorruptedEdgeEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  @Override
  public int getMaxLevel() {
    return 4;
  }

  @Override
  public int getMinPower(int level) {
    return 10 + 30 * (level - 1);
  }

  @Override
  public int getMaxPower(int level) {
    return getMinPower(level) + 40;
  }

  @Override
  public float getAttackDamage(int level, EntityGroup group) {
    return -2.0f;
  }

  private List<StatusEffect> eligibleEffects = ImmutableList.of(
      StatusEffects.BLINDNESS, StatusEffects.HUNGER, StatusEffects.LEVITATION,
      StatusEffects.MINING_FATIGUE, StatusEffects.NAUSEA, StatusEffects.POISON,
      StatusEffects.SLOWNESS, StatusEffects.WEAKNESS);

  @Override
  public void onTargetDamaged(LivingEntity user, Entity target, int level) {
    super.onTargetDamaged(user, target, level);
    if (target instanceof LivingEntity) {
      LivingEntity livingEntity = (LivingEntity) target;
      livingEntity.addStatusEffect(new StatusEffectInstance(
          eligibleEffects.get(user.getRandom().nextInt(eligibleEffects.size())),
          100 + 20 * Math.max(0, level - 3), Math.min(3, level) - 1));
    }

  }

}
