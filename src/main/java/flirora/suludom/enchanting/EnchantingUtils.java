package flirora.suludom.enchanting;

import java.util.List;

import flirora.suludom.item.EnchantingMaterial;
import it.unimi.dsi.fastutil.objects.Object2IntLinkedOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EnchantingUtils {
  public static boolean isOverenchanted(List<EnchantmentLevelEntry> enchantments) {
    for (EnchantmentLevelEntry enchantment : enchantments) {
      if (enchantment.level > enchantment.enchantment.getMaxLevel())
        return true;
    }
    return false;
  }

  public static Object2IntMap<Enchantment> enchantmentsFromTag(
      ListTag enchantmentTag) {
    Object2IntMap<Enchantment> enchantments =
        new Object2IntLinkedOpenHashMap<>();
    for (Tag e : enchantmentTag) {
      if (!(e instanceof CompoundTag))
        continue;
      CompoundTag info = (CompoundTag) e;
      enchantments.put(
          Registry.ENCHANTMENT.get(Identifier.tryParse(info.getString("id"))),
          info.getInt("lvl"));
      ;
    }
    return enchantments;
  }

  public static ListTag enchantmentsToTag(
      Object2IntMap<Enchantment> enchantments) {
    ListTag enchantmentTag = new ListTag();
    if (enchantments == null)
      return enchantmentTag;
    for (Object2IntMap.Entry<Enchantment> entry : enchantments
        .object2IntEntrySet()) {
      CompoundTag info = new CompoundTag();
      info.putString("id",
          Registry.ENCHANTMENT.getId(entry.getKey()).toString());
      info.putInt("lvl", entry.getIntValue());
      enchantmentTag.add(info);
    }
    return enchantmentTag;
  }

  public static float getAdditionalAttackDamage(ItemStack stack,
      LivingEntity attacker, LivingEntity target) {
    float amount = 0;
    Object2IntMap<Enchantment> enchantments =
        enchantmentsFromTag(stack.getEnchantments());
    for (Object2IntMap.Entry<Enchantment> entry : enchantments
        .object2IntEntrySet()) {
      Enchantment enchantment = entry.getKey();
      if (enchantment instanceof CustomDamageEnchantment) {
        amount += ((CustomDamageEnchantment) enchantment)
            .getAttackDamage(entry.getIntValue(), attacker, target);
      }
    }
    return amount;
  }

  public static Slot createLapisOrBloodLapisSlot(Inventory inventory) {
    return new Slot(inventory, 1, 35, 47) {
      public boolean canInsert(ItemStack stack) {
        return stack.getItem() == Items.LAPIS_LAZULI
            || stack.getItem() instanceof EnchantingMaterial;
      }
    };
  }
}
