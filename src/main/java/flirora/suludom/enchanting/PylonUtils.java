package flirora.suludom.enchanting;

import flirora.suludom.SuludomMod;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction.Axis;
import net.minecraft.world.World;

public class PylonUtils {
  public static boolean hasOrthogonalPylonAt(World world, BlockPos blockPos,
      int offX, int offZ) {
    /*
     * Need column of 4 quartz pillars oriented vertically, and an orthogonal
     * pylon block on top. Bottom block must have no opaque blocks around it.
     */
    for (int offY = 0; offY < 4; ++offY) {
      BlockState state = world.getBlockState(blockPos.add(offX, offY, offZ));
      if (state.getBlock() != Blocks.QUARTZ_PILLAR)
        return false;
      if (state.get(Properties.AXIS) != Axis.Y)
        return false;
    }
    for (int offX2 = -1; offX2 < 2; ++offX2) {
      for (int offZ2 = -1; offZ2 < 2; ++offZ2) {
        if (offX2 == 0 && offZ2 == 0)
          continue;
        if (world.getBlockState(blockPos.add(offX + offX2, 0, offZ + offZ2))
            .getMaterial().blocksMovement()) {
          return false;
        }
      }
    }
    return world.getBlockState(blockPos.add(offX, 4, offZ))
        .getBlock() == SuludomMod.ORTHOGONAL_PYLON;
  }

  public static boolean hasDiagonalPylonAt(World world, BlockPos blockPos,
      int offX, int offZ) {
    /*
     * Need column of 4 quartz pillars oriented vertically, and an orthogonal
     * pylon block on top. Bottom block must have no opaque blocks around it.
     */
    for (int offY = 0; offY < 4; ++offY) {
      BlockState state = world.getBlockState(blockPos.add(offX, offY, offZ));
      if (state.getBlock() != Blocks.PURPUR_PILLAR)
        return false;
      if (state.get(Properties.AXIS) != Axis.Y)
        return false;
    }
    for (int offX2 = -1; offX2 < 2; ++offX2) {
      for (int offZ2 = -1; offZ2 < 2; ++offZ2) {
        if (offX2 == 0 && offZ2 == 0)
          continue;
        if (world.getBlockState(blockPos.add(offX + offX2, 0, offZ + offZ2))
            .getMaterial().blocksMovement()) {
          return false;
        }
      }
    }
    return world.getBlockState(blockPos.add(offX, 4, offZ))
        .getBlock() == SuludomMod.DIAGONAL_PYLON;
  }
}
