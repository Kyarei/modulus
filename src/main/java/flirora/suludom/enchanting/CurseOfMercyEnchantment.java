package flirora.suludom.enchanting;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;

public class CurseOfMercyEnchantment extends SuludomDamageEnchantment {
  public static Identifier ID = new Identifier("suludom", "curse_of_mercy");

  public CurseOfMercyEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes, int minimumPowerAtLvl1,
      int minimumPowerIncrementPerLevel, int powerRange, int maxLevel,
      float damageBonusBase, float damageBonusPerLevel) {
    super(weight, type, slotTypes, minimumPowerAtLvl1,
        minimumPowerIncrementPerLevel, powerRange, maxLevel, damageBonusBase,
        damageBonusPerLevel);
  }

  @Override
  public boolean isEligible(LivingEntity entity) {
    return true;
  }

  public boolean isCursed() {
    return true;
  }

  @Override
  public boolean canAccept(Enchantment other) {
    return super.canAccept(other) && other != Enchantments.FIRE_ASPECT
        && other != Enchantments.LOOTING;
  }

}
