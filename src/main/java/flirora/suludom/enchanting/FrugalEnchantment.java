package flirora.suludom.enchanting;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;

public class FrugalEnchantment extends Enchantment {
  public static final Identifier ID = new Identifier("suludom", "frugal");

  public FrugalEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  @Override
  public int getMinPower(int level) {
    return 5 * (level - 1) + 8;
  }

  @Override
  public int getMaxLevel() {
    return 3;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    Item item = stack.getItem();
    return item == Items.BLAST_FURNACE || item == Items.SMOKER;
  }

}
