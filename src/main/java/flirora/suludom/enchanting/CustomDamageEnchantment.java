package flirora.suludom.enchanting;

import net.minecraft.entity.LivingEntity;

public interface CustomDamageEnchantment {
  float getAttackDamage(int level, LivingEntity attacker, LivingEntity target);
}
