package flirora.suludom.enchanting;

import flirora.suludom.SuludomMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class RunicLoopbackEnchantment extends Enchantment {
  public static final Identifier ID =
      new Identifier("suludom", "runic_loopback");

  public RunicLoopbackEnchantment(Rarity weight, EnchantmentTarget type,
      EquipmentSlot[] slotTypes) {
    super(weight, type, slotTypes);
  }

  @Override
  public int getMinPower(int level) {
    return 45 + (level - 1) * 15;
  }

  @Override
  public int getMaxLevel() {
    return 5;
  }

  @Override
  public boolean isAcceptableItem(ItemStack stack) {
    Item item = stack.getItem();
    return (item instanceof BlockItem)
        && ((BlockItem) item).getBlock() == SuludomMod.SCRAPPING_TABLE;
  }

}
