package flirora.suludom;

import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;
import me.sargunvohra.mcmods.autoconfig1u.annotation.ConfigEntry;

@Config(name = "suludom") public class SuludomConfig implements ConfigData {

  public SuludomConfig() {
    // TODO Auto-generated constructor stub
  }

  @ConfigEntry.Gui.Tooltip public boolean disableVanillaTools = true;

  @ConfigEntry.Gui.Tooltip public boolean tweakLootTables = true;

}
