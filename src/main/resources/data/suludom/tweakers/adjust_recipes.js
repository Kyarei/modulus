log.info("Tweaking recipes...");

var RecipeTweaker = libcd.require("libcd.recipe.RecipeTweaker");
var TweakerUtils = libcd.require("libcd.util.TweakerUtils");

var forbiddenToolMaterials = ["stone", "iron", "golden", "diamond"];
var forbiddenArmorMaterials = ["iron", "golden", "diamond"];
var toolTypes = ["sword", "pickaxe", "axe", "shovel", "hoe"];
var armorTypes = ["helmet", "chestplate", "leggings", "boots"];

for (var i = 0; i < forbiddenToolMaterials.length; ++i) {
  for (var j = 0; j < toolTypes.length; ++j) {
    RecipeTweaker.removeRecipesFor(
      "minecraft:" + forbiddenToolMaterials[i] + "_" + toolTypes[j]);
  }
}

for (var i = 0; i < forbiddenArmorMaterials.length; ++i) {
  for (var j = 0; j < armorTypes.length; ++j) {
    RecipeTweaker.removeRecipesFor(
      "minecraft:" + forbiddenArmorMaterials[i] + "_" + armorTypes[j]);
  }
}

